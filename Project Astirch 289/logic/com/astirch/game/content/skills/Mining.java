package com.astirch.game.content.skills;

import com.astirch.Config;
import com.astirch.engine.event.CycleEvent;
import com.astirch.engine.event.CycleEventContainer;
import com.astirch.engine.event.CycleEventHandler;
import com.astirch.engine.util.Misc;
import com.astirch.game.model.players.*;

/**
* @Author Sanity
*/

public class Mining {
	
	Client c;
	
	private final int VALID_PICK[] = {1265,1267,1269,1273,1271,1275};
	private final int[] PICK_REQS = {1,1,6,6,21,31,41,61};
	private final int[] RANDOM_GEMS = {1623,1621,1619,1617,1631};
	private int oreType;
	private int exp;
	@SuppressWarnings("unused")
	private int levelReq;
	private int pickType;
	private int emoteTimer = 4;
	private boolean processRunning = false;
	
	public Mining(Client c) {
		this.c = c;
	}
	
	public void startMining(int oreType, int levelReq, int exp) {
		c.turnPlayerTo(c.objectX, c.objectY);
		if (processRunning)
			return;
		if (goodPick() > 0) {
			if (c.playerLevel[c.playerMining] >= levelReq) {
				this.oreType = oreType;
				this.exp = exp;
				this.levelReq = levelReq;
				this.pickType = goodPick();
				c.miningTimer = getMiningTimer(oreType)  - getPickAxeTimer();
				if (c.miningTimer < 1) {
					c.miningTimer = 1 + Misc.random(5);
				}
				c.sendMessage("You swing your pick at the rock.");
				mineOre();
				c.startAnimation(getPickEmote());
			} else {
				resetMining();
				c.sendMessage("You need a mining level of " + levelReq + " to mine this rock.");
				c.startAnimation(65535);
			}		
		} else {
			resetMining();
			c.sendMessage("You need a pickaxe to mine this rock.");
			c.startAnimation(65535);
			c.getPA().resetVariables();
		}
	}
	private boolean spaceInInventory(){
		if (c.getItems().freeSlots() > 0){
			return true;
		}
		return false;
	}
	private void mineOre() {
			if (processRunning) {
				return;
			}
			c.isSkilling = true;
			processRunning = true;
			CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
				@Override
				public void execute(CycleEventContainer container) {
					if (c.isSkilling) {
						if (!spaceInInventory()){
							c.sendMessage("Not enough space in your inventory");
							container.stop();
							return;
						}
						if (c.miningTimer > 0) {
							c.miningTimer--;
						}
						pickType = goodPick();
						if (emoteTimer > 0){
							emoteTimer--;
						}
						if (emoteTimer == 0) {
							c.startAnimation(getPickEmote());
							emoteTimer = 4;
						}
						if (c.miningTimer == 0 && c.getItems().addItem(oreType,1)) {
							processRunning = true;
							c.sendMessage("You manage to mine some ore.");
							c.getPA().addSkillXP(exp * Config.SKILLING_EXPERIENCE, c.playerMining);
							c.getPA().refreshSkill(c.playerMining);
							c.miningTimer = getMiningTimer(oreType)  - getPickAxeTimer();
							if (c.miningTimer <= 0) {
								c.miningTimer = 1 + Misc.random(5);
							}
							if (Misc.random(25) == 0) {
								c.getItems().addItem(RANDOM_GEMS[(int)(RANDOM_GEMS.length * Math.random())], 1);
								c.sendMessage("You find a gem!");
							}					
						}
					} else {
						container.stop();
					}
				}
				@Override
				public void stop() {
					resetMining();
				}
			}, 1);
	}
	
	private void resetMining() {
		this.oreType = -1;
		this.exp = -1;
		this.levelReq = -1;
		this.pickType = -1;
		c.isSkilling = false;
		c.startAnimation(65535);
		this.processRunning = false;
	}
	
	private int goodPick() {
		for (int j = VALID_PICK.length - 1; j >= 0; j--) {
			if (c.playerEquipment[c.playerWeapon] == VALID_PICK[j]) {
				if (c.playerLevel[c.playerMining] >= PICK_REQS[j])
					return VALID_PICK[j];
			}		
		}
		for (int i = 0; i < c.playerItems.length; i++) {
			for (int j = VALID_PICK.length - 1; j >= 0; j--) {
				if (c.playerItems[i] == VALID_PICK[j] + 1) {
					if (c.playerLevel[c.playerMining] >= PICK_REQS[j])
						return VALID_PICK[j];
				}
			}		
		}
		return - 1;
	}
	
	private int getMiningTimer(int ore) {
		int time = 0;
		switch (ore){
		case 438: //Tin
		case 436: //Copper
			return time += 5 + Misc.random(5);
		case 440: //iron
			return time += 7.5 + Misc.random((int) 7.5);
		case 453: //coal
			return time += 10 + Misc.random(10);
		case 444:
			return time += 10 + Misc.random(10);
		case 447: //mithril
			return time += 12.5 + Misc.random((int) 12.5);
		case 449: //adamant
			return time += 17.5 + Misc.random((int) 17.5);
		case 451: //rune
			return time += 25 + Misc.random(25);
		}
		return time;
	}

	private int getPickAxeTimer() {
		switch(pickType) {
		case 1265:
			return 0;
		case 1267:
			return 1;
		case 1269:
			return 3;
		case 1273:
			return 5;
		case 1271:
			return 7;
		case 1275:
			return 10;
		}
		return 0;
	}
	private int getPickEmote() {
		switch(pickType) {
		case 1265: //bronze
			return 625;
		case 1267: //iron
			return 626;
		case 1269: //steel
			return 627;
		case 1273: //mithril
			return 629;
		case 1271: //adamant
			return 628;
		case 1275: //rune
			return 624;
		}
		return 625;
	}
	
}