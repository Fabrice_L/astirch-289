package com.astirch.engine.world;

import com.astirch.engine.event.CycleEvent;
import com.astirch.engine.event.CycleEventContainer;
import com.astirch.engine.event.CycleEventHandler;
import com.astirch.game.model.players.Client;

public class StairsHandler {
	
	Client client;
	public boolean usingStairs = false;

	public StairsHandler(Client client) {
		this.client = client;
	}
	
	public void useStairs(final int x, final int y, final int height, String way) {
		client.sendMessage("You walk " + (way == "up" ? "up" : "down") + " the stairs.");
		usingStairs = true;
		CycleEventHandler.getSingleton().addEvent(client, new CycleEvent() {
			@Override
			public void execute(CycleEventContainer container) {
				client.teleportToX = x;
				client.teleportToY = y;
				client.heightLevel = height;
				if (client.absX == x && client.absY == y && client.heightLevel == height)
				container.stop();
			}
			@Override
			public void stop() {
				usingStairs = false;
			}
		}, 1);
	}
	
	public void useLadder(final int x, final int y, final int height, String way) {
		client.sendMessage("You climb " + (way == "up" ? "up" : "down") + " the ladder.");
		client.startAnimation(828);
		usingStairs = true;
		CycleEventHandler.getSingleton().addEvent(client, new CycleEvent() {
			@Override
			public void execute(CycleEventContainer container) {
				client.teleportToX = x;
				client.teleportToY = y;
				client.heightLevel = height;
				if (client.absX == x && client.absY == y && client.heightLevel == height)
				container.stop();
			}
			@Override
			public void stop() {
				usingStairs = false;
			}
		}, 1);
	}
}
