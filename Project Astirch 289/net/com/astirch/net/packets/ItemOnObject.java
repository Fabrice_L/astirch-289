package com.astirch.net.packets;

/**
 * @author Ryan / Lmctruck30
 */

import com.astirch.game.model.items.UseItem;
import com.astirch.game.model.players.Client;
import com.astirch.game.model.players.PacketType;

public class ItemOnObject implements PacketType {

	@SuppressWarnings("unused")
	@Override
	public void processPacket(Client c, int packetType, int packetSize) {
		c.faceUpdate(0);
		c.npcIndex = 0;
		c.playerIndex = 0;
		if (c.followId > 0 || c.followId2 > 0)
			c.getPA().resetFollow();
		
		int a = c.getInStream().readUnsignedWord();
		int objectId = c.getInStream().readSignedWordBigEndian();
		int objectY = c.getInStream().readSignedWordBigEndianA();
		int b = c.getInStream().readUnsignedWord();
		int objectX = c.getInStream().readSignedWordBigEndianA();
		int itemId = c.getInStream().readUnsignedWord();
		UseItem.ItemonObject(c, objectId, objectX, objectY, itemId);
		
	}

}
