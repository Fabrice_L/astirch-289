package com.astirch.net.packets;

import com.astirch.Config;
import com.astirch.Server;
import com.astirch.engine.Connection;
import com.astirch.engine.util.Misc;
import com.astirch.game.model.players.Client;
import com.astirch.game.model.players.PacketType;
import com.astirch.game.model.players.PlayerHandler;

/**
 * Commands
 **/
public class Commands implements PacketType {

	
	@Override
	public void processPacket(Client c, int packetType, int packetSize) {
	String playerCommand = c.getInStream().readString();
	if(Config.SERVER_DEBUG)
		Misc.println(c.playerName+" playerCommand: "+playerCommand);
		if(c.playerRights >= 0) {
			if (playerCommand.equalsIgnoreCase("players")) {
				c.sendMessage("There are currently "+PlayerHandler.getPlayerCount() + " players online.");
			}
			
			if (playerCommand.startsWith("ban") && playerCommand.charAt(3) == ' ' && c.playerRights >= 1) { // use as ::ban name
				try {	
					String playerToBan = playerCommand.substring(4);
					Connection.addNameToBanList(playerToBan);
					Connection.addNameToFile(playerToBan);
					for(int i = 0; i < Config.MAX_PLAYERS; i++) {
						if(PlayerHandler.players[i] != null) {
							if(PlayerHandler.players[i].playerName.equalsIgnoreCase(playerToBan)) {
								PlayerHandler.players[i].disconnected = true;
							} 
						}
					}
				} catch(Exception e) {
					c.sendMessage("Player Must Be Offline.");
				}
			}
			if(playerCommand.startsWith("jail") && c.playerRights >= 2) {
				try {
					String playerToBan = playerCommand.substring(5);
					for(int i = 0; i < Config.MAX_PLAYERS; i++) {
						if(PlayerHandler.players[i] != null) {
							if(PlayerHandler.players[i].playerName.equalsIgnoreCase(playerToBan)) {
						Client c2 = (Client)PlayerHandler.players[i];
					    c2.teleportToX = 3102;
                        c2.teleportToY = 9516;
								c2.sendMessage("You have been jailed by "+c.playerName+"");
								c.sendMessage("Successfully Jailed "+c2.playerName+".");
							} 
						}
					}
				} catch(Exception e) {
					c.sendMessage("Player Must Be Offline.");
				}
			}		
			if(playerCommand.startsWith("unjail") && c.playerRights >= 1) {
				try {
					String playerToBan = playerCommand.substring(7);
					for(int i = 0; i < Config.MAX_PLAYERS; i++) {
						if(PlayerHandler.players[i] != null) {
							if(PlayerHandler.players[i].playerName.equalsIgnoreCase(playerToBan)) {
						Client c2 = (Client)PlayerHandler.players[i];
					    c2.teleportToX = 2613;
                        c2.teleportToY = 3088;
					
								c2.sendMessage("You have been unjailed by "+c.playerName+"");
								c.sendMessage("Successfully unjailed "+c2.playerName+".");
							} 
						}
					}
				} catch(Exception e) {
					c.sendMessage("Player Must Be Offline.");
				}
			}
							if (playerCommand.startsWith("kick") && c.playerRights >= 1) { // use as ::kick name
				try {	
					String playerToKick = playerCommand.substring(5);
					for(int i = 0; i < Config.MAX_PLAYERS; i++) {
						if(PlayerHandler.players[i] != null) {
							if(PlayerHandler.players[i].playerName.equalsIgnoreCase(playerToKick)) {
								PlayerHandler.players[i].disconnected = true;
							} 
						}
					}
				} catch(Exception e) {
					c.sendMessage("@red@Use as ::kick name");
				}
			}
			if (playerCommand.startsWith("mute") && c.playerRights >= 1) {
				try {	
					if(c.playerRights == 0 || c.playerRights == 5){
		return;
		}
					String playerToBan = playerCommand.substring(5);
					Connection.addNameToMuteList(playerToBan);
					for(int i = 0; i < Config.MAX_PLAYERS; i++) {
						if(PlayerHandler.players[i] != null) {
							if(PlayerHandler.players[i].playerName.equalsIgnoreCase(playerToBan)) {
								Client c2 = (Client)PlayerHandler.players[i];
								c2.sendMessage("You have been muted by: " + c.playerName);
								break;
							} 
						}
					}
				} catch(Exception e) {
					c.sendMessage("Player Must Be Offline.");
				}			
			}

		if (playerCommand.startsWith("yell")) {
				String rank = "";
				String Message = playerCommand.substring(4).toLowerCase();
				if (c.playerRights >= 1) {
					rank = "[Mod] "+ Misc.optimizeText(c.playerName) +": ";
				}
				if (c.playerRights >= 2) {
					rank = "[Admin] "+ Misc.optimizeText(c.playerName) +": ";
				}
				if (c.playerRights == 0) {
					rank = ""+ Misc.optimizeText(c.playerName) +": ";
				}  	   				
				c.yell("@cha@" +rank+Misc.optimizeText(Message));
		}
			if (playerCommand.startsWith("object") && (c.playerRights >= 1)) {
				String[] args = playerCommand.split(" ");				
				c.getPA().object(Integer.parseInt(args[1]), c.absX, c.absY, 0, 10);
			}

			if (playerCommand.startsWith("tele") && (c.playerRights >= 1)) {
				String[] arg = playerCommand.split(" ");
				if (arg.length > 3)
					c.getPA().movePlayer(Integer.parseInt(arg[1]),Integer.parseInt(arg[2]),Integer.parseInt(arg[3]));
				else if (arg.length == 3)
					c.getPA().movePlayer(Integer.parseInt(arg[1]),Integer.parseInt(arg[2]),c.heightLevel);
			}
			if (playerCommand.startsWith("headicon")) {
				String[] args = playerCommand.split(" ");
				c.headIcon = Integer.parseInt(args[1]);
				c.getPA().requestUpdates();
			}
			if (playerCommand.startsWith("item") /*&& (c.playerRights >= 2)*/) {
				if (c.inWild())
					return;
				try {
				String[] args = playerCommand.split(" ");
				if (args.length == 3) {
					int newItemID = Integer.parseInt(args[1]);
					int newItemAmount = Integer.parseInt(args[2]);
					if ((newItemID <= 4088) && (newItemID >= 0)) {
						c.getItems().addItem(newItemID, newItemAmount);
						System.out.println("Spawned: " + newItemID + " by: " + c.playerName);
					} else {
						/* If there is no such ID */
						c.sendMessage("Maximum itemid is 4088");
					}
				} else {
					/* If input is incorrect */
					c.sendMessage("Wrong input");
				}
				} catch (Exception e) {
				
				}
			}
			

			if (playerCommand.startsWith("find") && c.playerRights == 3) {
				int id = Integer.parseInt(playerCommand.substring(5));
				for (int i = 1000; i < id; i++) {
					c.getPA().sendFrame126(""+i, i);
				}
			}
			
			if (playerCommand.startsWith("xteleto") && c.playerRights >= 1) {
				if (c.inWild())
				return;
				String name = playerCommand.substring(8);
				for (int i = 0; i < Config.MAX_PLAYERS; i++) {
					if (PlayerHandler.players[i] != null) {
						if (PlayerHandler.players[i].playerName.equalsIgnoreCase(name)) {
							c.getPA().movePlayer(PlayerHandler.players[i].getX(), PlayerHandler.players[i].getY(), PlayerHandler.players[i].heightLevel);
						}
					}
				}			
			}
			if (playerCommand.startsWith("mute") && c.playerRights >= 1) {
				try {	
					String playerToBan = playerCommand.substring(5);
					Connection.addNameToMuteList(playerToBan);
					for(int i = 0; i < Config.MAX_PLAYERS; i++) {
						if(PlayerHandler.players[i] != null) {
							if(PlayerHandler.players[i].playerName.equalsIgnoreCase(playerToBan)) {
								Client c2 = (Client)PlayerHandler.players[i];
								c2.sendMessage("You have been muted by: " + c.playerName);
								c.sendMessage("You have muted someone.");
								break;
							} 
						}
					}
				} catch(Exception e) {
					c.sendMessage("Player Must Be Offline.");
				}			
			}
			if (playerCommand.startsWith("xteletome") && c.playerRights >= 1) {
			try {	
				String playerToTele = playerCommand.substring(10);
				for(int i = 0; i < Config.MAX_PLAYERS; i++) {
					if(PlayerHandler.players[i] != null) {
						if(PlayerHandler.players[i].properName.equalsIgnoreCase(playerToTele)) {
							Client c2 = (Client)PlayerHandler.players[i];
                            if (c.inWild() && (c.playerRights != 3)) {
                                c.sendMessage("You cannot move players when you are in the Wilderness.");
                                return;
                            }
                            if (c2.inWild() && (c.playerRights != 3)) {
                                c.sendMessage("You cannot move players when they are in the Wilderness.");
                                return;
                            }
							c2.sendMessage("You have been teleported to " + c.properName);
							c2.getPA().movePlayer(c.getX(), c.getY(), c.heightLevel);
							break;
						} 
					}
				}
			} catch(Exception e) {
				c.sendMessage("Player Must Be Offline.");
			}			
		}
		if(c.playerRights >= 3) {
			
			if (playerCommand.equalsIgnoreCase("mypos")) {
				c.sendMessage("X: "+c.absX);
				c.sendMessage("Y: "+c.absY);
			}
			
			if (playerCommand.startsWith("anim")) {
				String[] args = playerCommand.split(" ");
				c.startAnimation(Integer.parseInt(args[1]));
				c.getPA().requestUpdates();
			}
			
			if (playerCommand.startsWith("gfx")) {
				String[] args = playerCommand.split(" ");
				c.gfx0(Integer.parseInt(args[1]));
			}

			if (playerCommand.startsWith("update")) {
				String[] args = playerCommand.split(" ");
				int a = Integer.parseInt(args[1]);
				PlayerHandler.updateSeconds = a;
				PlayerHandler.updateAnnounced = false;
				PlayerHandler.updateRunning = true;
				PlayerHandler.updateStartTime = System.currentTimeMillis();
			}

			if (playerCommand.startsWith("interface")) {
				try {	
					String[] args = playerCommand.split(" ");
					int a = Integer.parseInt(args[1]);
					c.getPA().showInterface(a);
				} catch(Exception e) {
					c.sendMessage("::interface ####"); 
				}
			}
			if (playerCommand.startsWith("cookingif")){
	        	c.getPA().sendFrame126("What would you like to make?", 8879);
	         	c.getPA().sendFrame246(8884, 250, 839); // middle
	     		c.getPA().sendFrame246(8883, 250, 841); // left picture
	     		c.getPA().sendFrame246(8885, 250, 52); // right pic
	     		c.getPA().sendFrame126("Shortbow", 8889);
	     		c.getPA().sendFrame126("Longbow", 8893);
	     		c.getPA().sendFrame126("Arrow Shafts", 8897);
				c.getPA().sendFrame164(8938);
			}
			if(playerCommand.startsWith("pnpc")) {
				int npc = Integer.parseInt(playerCommand.substring(5));
				if(npc < 9999){
				c.npcId2 = npc;
				c.isNpc = true;
				c.updateRequired = true;
				c.appearanceUpdateRequired = true;
				}
				}
				if(playerCommand.startsWith("unpc")) {
				c.isNpc = false;
				c.updateRequired = true;
				c.appearanceUpdateRequired = true;
				}

			
			if(playerCommand.startsWith("npc")) {
				try {
					int newNPC = Integer.parseInt(playerCommand.substring(4));
					if(newNPC > 0) {
						Server.npcHandler.spawnNpc(c, newNPC, c.absX, c.absY, 0, 0, 120, 7, 70, 70, false, false);
						c.sendMessage("You spawn a Npc.");
					} else {
						c.sendMessage("No such NPC.");
					}
				} catch(Exception e) {
					
				}			
			}
						if (playerCommand.equals("xteleall")) {
				for (int j = 0; j < PlayerHandler.players.length; j++) {
					if (PlayerHandler.players[j] != null) {
						Client c2 = (Client)PlayerHandler.players[j];
						c2.teleportToX = c.absX;
						c2.teleportToY = c.absY;
						c2.heightLevel = c.heightLevel;
						c2.sendMessage("Mass teleport to: " + c.playerName + "");
					}
				}
			}
			
			
			if (playerCommand.startsWith("ipban")) { // use as ::ipban name
				try {
					String playerToBan = playerCommand.substring(6);
					for(int i = 0; i < Config.MAX_PLAYERS; i++) {
						if(PlayerHandler.players[i] != null) {
							if(PlayerHandler.players[i].playerName.equalsIgnoreCase(playerToBan)) {
								Connection.addIpToBanList(PlayerHandler.players[i].connectedFrom);
								Connection.addIpToFile(PlayerHandler.players[i].connectedFrom);
								c.sendMessage("You have IP banned the user: "+PlayerHandler.players[i].playerName+" with the host: "+PlayerHandler.players[i].connectedFrom);
								PlayerHandler.players[i].disconnected = true;
							} 
						}
					}
				} catch(Exception e) {
					c.sendMessage("Player Must Be Offline.");
				}
			}
			

			
			if (playerCommand.startsWith("unban")) {
				try {	
					String playerToBan = playerCommand.substring(6);
					Connection.removeNameFromBanList(playerToBan);
					c.sendMessage(playerToBan + " has been unbanned.");
				} catch(Exception e) {
					c.sendMessage("Player Must Be Offline.");
				}
			}
			if (playerCommand.startsWith("ipmute")) {
				try {	
					String playerToBan = playerCommand.substring(7);
					for(int i = 0; i < Config.MAX_PLAYERS; i++) {
						if(PlayerHandler.players[i] != null) {
							if(PlayerHandler.players[i].playerName.equalsIgnoreCase(playerToBan)) {
								Connection.addIpToMuteList(PlayerHandler.players[i].connectedFrom);
								c.sendMessage("You have IP Muted the user: "+PlayerHandler.players[i].playerName);
								Client c2 = (Client)PlayerHandler.players[i];
								c2.sendMessage("You have been muted by: " + c.playerName);
								break;
							} 
						}
					}
				} catch(Exception e) {
					c.sendMessage("Player Must Be Offline.");
				}			
			}
			if (playerCommand.startsWith("unipmute")) {
				try {	
					String playerToBan = playerCommand.substring(9);
					for(int i = 0; i < Config.MAX_PLAYERS; i++) {
						if(PlayerHandler.players[i] != null) {
							if(PlayerHandler.players[i].playerName.equalsIgnoreCase(playerToBan)) {
								Connection.unIPMuteUser(PlayerHandler.players[i].connectedFrom);
								c.sendMessage("You have Un Ip-Muted the user: "+PlayerHandler.players[i].playerName);
								break;
							} 
						}
					}
				} catch(Exception e) {
					c.sendMessage("Player Must Be Offline.");
				}			
			}
			if (playerCommand.startsWith("unmute") ) {
				try {	
					String playerToBan = playerCommand.substring(7);
					Connection.unMuteUser(playerToBan);
				} catch(Exception e) {
					c.sendMessage("Player Must Be Offline.");
				}			
			}
		}
	}
}
}		
		
		
		
		
		
		

