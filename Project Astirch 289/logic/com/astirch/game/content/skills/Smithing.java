package com.astirch.game.content.skills;

import com.astirch.Config;
import com.astirch.engine.event.CycleEvent;
import com.astirch.engine.event.CycleEventContainer;
import com.astirch.engine.event.CycleEventHandler;
import com.astirch.engine.util.Misc;
import com.astirch.game.model.players.Client;
/**
 * Smithing.java
 *
 * @author Sanity
 *
 **/
 
public class Smithing {
	
	private Client c;
	private final int[] SMELT_BARS = {2349,2351,2355,2353,2357,2359,2361,2363};
	private final int[] SMELT_FRAME = {2405,2406,2407,2409,2410,2411,2412,2413};
	private final int[] BAR_REQS = {1,15,20,30,40,50,70,85};
	private final int[] ORE_1 = {438,440,-1,440,444,447,449,451};
	private final int[] ORE_2 = {436,-1,-1,-1,-1,-1,-1,-1};
	private final int[] SMELT_EXP = {6,13,-1,18,23,30,38,50};
	public int item;
	public int xp;
	public int remove;
	public int removeamount;
	public int maketimes;
	private int exp;
	private int oreId;
	private int oreId2;
	private int barId;
	private boolean processRunning = false;
	
	public Smithing(Client c) {
		this.c = c;
	}
	
	public void sendSmelting() {
		for (int j = 0; j < SMELT_FRAME.length; j++) {
			c.getPA().sendFrame246(SMELT_FRAME[j], 150, SMELT_BARS[j]);
		}
		c.getPA().sendFrame164(2400);
		c.smeltInterface = true;	
	}
	
	public void startSmelting(int barType) {
		if (processRunning) {
			return;
		}
		c.turnPlayerTo(c.objectX, c.objectY);
		if (canSmelt(barType)) {
			//c.sendMessage("We canSmelt");
			if (hasOres(barType)) {
				this.exp = getExp(barType);
				this.oreId = getOre(barType);
				this.oreId2 = getOre2(barType);
				this.barId = barType;
				smelt(barType);		
			} else {
				c.sendMessage("You do not have the required ores to smelt this.");
				c.getPA().removeAllWindows();
				resetSmelting();
			}
		} else {
			c.sendMessage("You must have a higher smithing level to smith this.");
			c.getPA().removeAllWindows();
			resetSmelting();
		}
	}
	public void smelt(final int barType) {
		if (processRunning) {
			return;
		}
		c.isSkilling = true;
		processRunning = true;
		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
			@Override
			public void execute(CycleEventContainer container) {
				if (c.isSkilling) {
					if (c.smeltTimer > 0) {
						c.smeltTimer--;
					}
					int ironRefining = Misc.random(3);
					if (c.smeltTimer == 0) {
						if (c.smeltAmount > 0) {
							c.getPA().closeAllWindows();
							if (hasOres(barType)) {
								c.getItems().deleteItem(oreId, c.getItems().getItemSlot(oreId), 1);
								c.smeltAmount--;
								c.startAnimation(899);
								c.smeltTimer = 2;
								if (barType == 2351 && ironRefining == 0) {
									c.sendMessage("You fail to refine the ores.");
									return;
								}
								if (oreId2 > 0)
									c.getItems().deleteItem(oreId2, c.getItems().getItemSlot(oreId2), 1);
								c.sendMessage("You succesfully smelt a bar.");
								c.getItems().addItem(barId,1);
								c.getPA().addSkillXP(exp * Config.SKILLING_EXPERIENCE, c.playerSmithing);
							} else {
								c.sendMessage("You do not have the required ores to smelt this.");
								c.getPA().removeAllWindows();
								container.stop();
							}
						} else {
							container.stop();
						}
					}
				} else {
					container.stop();
				}
			}
			@Override
			public void stop() {
				resetSmelting();
			}
		}, 1);
	}
	
	private void resetSmelting() {
		c.smeltType = 0;
		c.smeltAmount = 0;
		c.isSkilling = false;
		processRunning = false;
	}

	public int getExp(int barType) {
		for (int j = 0; j < SMELT_BARS.length; j++) {
			if (barType == SMELT_BARS[j]) {
				return SMELT_EXP[j];
			}
		}
		return 0;	
	}
	
	public int getOre(int barType) {
		for (int j = 0; j < SMELT_BARS.length; j++) {
			if (barType == SMELT_BARS[j]) {
				//c.sendMessage("" + ORE_1[j]);
				return ORE_1[j];
			}
		}
		return 0;	
	}
	
	public int getOre2(int barType) {
		for (int j = 0; j < SMELT_BARS.length; j++) {
			if (barType == SMELT_BARS[j]) {
				//c.sendMessage("" + ORE_2[j]);
				return ORE_2[j];
			}
		}
		return 0;	
	}
	
	public boolean canSmelt(int barType) {
		for (int j = 0; j < SMELT_BARS.length; j++) {
			if (barType == SMELT_BARS[j]) {
				//c.sendMessage("" + c.playerLevel + " bar: " + BAR_REQS[j]);
				return c.playerLevel[c.playerSmithing] >= BAR_REQS[j];
			}
		}
		return false;
	}
	
	public boolean hasOres(int barType) {
		if (getOre2(barType) > 0)
			return c.getItems().playerHasItem(getOre(barType)) && c.getItems().playerHasItem(getOre2(barType));
		else
			return c.getItems().playerHasItem(getOre(barType));
	}
	
	public void readInput(int level, int type, Client c, int amounttomake) {
		/*{item, lvl, exp,  amountremoved, itemremoved}*/
		int[][] smithingItems = {
				//bronze
				{1351, 1, 13, 1, 2349}, {1205, 1, 13, 1, 2349}, {1422, 2, 13, 1, 2349}, {1139, 3, 13, 1, 2349}, {819, 4, 13, 1, 2349},
				{1277, 4, 13, 1, 2349}, {3095, 13, 13, 2, 2349}, {39, 5, 13, 1, 2349}, {1321, 5, 13, 2, 2349}, {1291, 6, 13, 2, 2349},
				{864, 7, 13, 1, 2349}, {1155, 7, 13, 2, 2349}, {1173, 8, 13, 2, 2349}, {1337, 9, 13, 3, 2349}, {1375, 10, 13, 3, 2349},
				{1103, 11, 13, 3, 2349}, {1189, 12, 13, 3, 2349}, {1307, 14, 13, 3, 2349}, {1075, 16, 13, 3, 2349}, {1087, 16, 13, 3, 2349}, 
				{1117, 18, 13, 5, 2349},
				
				//iron
				{1203, 15, 25, 1, 2351}, {1349, 16, 25, 1, 2351}, {1420, 17, 25, 1, 2351}, {1137, 18, 25, 1, 2351}, {820, 19, 25, 1, 2351},
				{1279, 19, 25, 1, 2351}, {40, 20, 25, 1, 2351}, {1323, 20, 25, 2, 2351}, {1293, 21, 25, 2, 2351}, {863, 22, 25, 1, 2351},
				{1153, 22, 25, 2, 2351}, {1175, 23, 25, 2, 2351}, {1335, 24, 25, 3, 2351}, {1363, 25, 25, 3, 2351}, {1101, 26, 25, 3, 2351},
				{1191, 27, 25, 3, 2351}, {3096, 28, 25, 2, 2351}, {1309, 29, 25, 3, 2351}, {1081, 31, 25, 3, 2351}, {1067, 31, 25, 3, 2351},
				{1115, 33, 25, 5, 2351},
				
				//steel
				{1207, 30, (int) 37.5, 1, 2353}, {1353, 31, (int) 37.5, 1, 2353}, {1424, 32, (int) 37.5, 1, 2353}, {1141, 33, (int) 37.5, 1, 2353},
				{821, 34, (int) 37.5, 1, 2353}, {1281, 34, (int) 37.5, 1, 2353}, {41, 35, (int) 37.5, 1, 2353}, {1325, 35, (int) 37.5, 2, 2353},
				{1295, 36, (int) 37.5, 2, 2353}, {865, 37, (int) 37.5, 1, 2353}, {1157, 37, (int) 37.5, 2, 2353}, {1177, 38, (int) 37.5, 2, 2353},
				{1339, 39, (int) 37.5, 3, 2353}, {1365, 40, (int) 37.5, 3, 2353}, {1105, 41, (int) 37.5, 3, 2353}, {1193, 42, (int) 37.5, 3, 2353},
				{3097, 43, (int) 37.5, 2, 2353}, {1311, 44, (int) 37.5, 3, 2353}, {1083, 46, (int) 37.5, 3, 2353}, {1069, 46, (int) 37.5, 3, 2353},
				{1119, 48, (int) 37.5, 5, 2353},
				
				//Mithril
				{1209, 50, 50, 1, 2359}, {1355, 51, 50, 1, 2359}, {1428, 52, 50, 1, 2359}, {1143, 53, 50, 1, 2359}, {822, 54, 50, 1, 2359},
				{1285, 54, 50, 1, 2359}, {42, 55, 50, 1, 2359}, {1329, 55, 50, 2, 2359}, {1299, 56, 50, 2, 2359}, {866, 57, 50, 1, 2359},
				{1159, 57, 50, 2, 2359}, {1181, 58, 50, 2, 2359}, {1343, 59, 50, 3, 2359}, {1369, 60, 50, 3, 2359}, {1109, 61, 50, 3, 2359},
				{1197, 62, 50, 3, 2359}, {3099, 63, 50, 2, 2359}, {1315, 64, 50, 3, 2359}, {1085, 66, 50, 3, 2359}, {1071, 66, 50, 3, 2359},
				{1121, 68, 50, 5, 2359},
				
				//adamant
				{1211, 70, (int) 62.5, 1, 2361}, {1357, 71, (int) 62.5, 1, 2361}, {1430, 72, (int) 62.5, 1, 2361}, {1145, 73, (int) 62.5, 1, 2361},
				{823, 74, (int) 62.5, 1, 2361}, {1287, 74, (int) 62.5, 1, 2361}, {43, 75, (int) 62.5, 1, 2361}, {1331, 75, (int) 62.5, 2, 2361},
				{1301, 76, (int) 62.5, 2, 2361}, {867, 77, (int) 62.5, 1, 2361}, {1161, 77, (int) 62.5, 2, 2361}, {1183, 78, (int) 62.5, 2, 2361},
				{1345, 79, (int) 62.5, 3, 2361}, {1371, 80, (int) 62.5, 3, 2361}, {1111, 81, (int) 62.5, 3, 2361}, {1199, 82, (int) 62.5, 3, 2361},
				{3100, 83, (int) 62.5, 2, 2361}, {1317, 84, (int) 62.5, 3, 2361}, {1091, 86, (int) 62.5, 3, 2361}, {1073, 86, (int) 62.5, 3, 2361},
				{1123, 88, (int) 62.5, 5, 2361},
				
				//rune
				{1213, 85, 75, 1, 2363}, {1359, 86, 75, 1, 2363}, {1432, 87, 75, 1, 2363}, {1147, 88, 75, 1, 2363}, {824, 89, 75, 1, 2363}, 
				{1289, 89, 75, 1, 2363}, {44, 90, 75, 1, 2363}, {1333, 90, 75, 2, 2363}, {1303, 91, 75, 2, 2363}, {868, 92, 75, 1, 2363},
				{1163, 92, 75, 2, 2363}, {1185, 93, 75, 2, 2363}, {1347, 94, 75, 3, 2363}, {1373, 95, 75, 3, 2363}, {1113, 96, 75, 3, 2363},
				{1201, 97, 75, 3, 2363}, {3101, 98, 75, 2, 2363}, {1319, 99, 75, 3, 2363}, {1093, 99, 75, 3, 2363}, {1097, 99, 75, 3, 2363},
				{1127, 99, 75, 5, 2363}};
		
		
		if (!c.getItems().playerHasItem(2347)) {
			c.sendMessage("You need a hammer to smith items.");
			resetSmithing();
			return;
		}
		for (int j = 0; j < smithingItems.length; j++) {
			if (type == smithingItems[j][0]) {
				if (level >= smithingItems[j][1]) {
					removeamount = smithingItems[j][3];
					xp = smithingItems[j][2] * removeamount;
					item = smithingItems[j][0];
					remove = smithingItems[j][4];
					maketimes = amounttomake;
					doaction(c);
				} else {
					c.sendMessage("You need a smithing level of " + smithingItems[j][1] + " to smith this.");
					resetSmithing();
				}
			}
		}		
	}	

	private void resetSmithing() {
		removeamount = -1;
		xp = -1;
		item = -1;
		remove = -1;
		maketimes = -1;
		processRunning = false;
		c.isSkilling = false;
	}

	public void doaction(final Client c) {
		c.getPA().removeAllWindows();
		if (processRunning){
			return;
		}
		c.turnPlayerTo(c.objectX, c.objectY);
		if (!c.getItems().playerHasItem(remove, removeamount)) {
			c.sendMessage("You need more bars to smith this item.");
			resetSmithing();
			return;
		}
		c.startAnimation(898);
		processRunning = true;
		c.isSkilling = true;
		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
			@Override
			public void execute(CycleEventContainer container) {
				if (c.isSkilling) {
					if (!c.getItems().playerHasItem(2347)) {
						c.sendMessage("You need a hammer to smith items.");
						container.stop();
						return;
					}
					if (maketimes > 0) {
						maketimes--;
						if (c.getItems().playerHasItem(remove, removeamount)){
							c.startAnimation(898);
							c.getItems().deleteItem(remove, removeamount);
							c.getItems().addItem(item, getAmountToAdd());
							c.getPA().addSkillXP(xp * Config.SKILLING_EXPERIENCE, c.playerSmithing);
						} else {
							c.sendMessage("You need more bars to continue smithing.");
							container.stop();
							return;
						}
					} else {
						container.stop();
						return;
					}
				} else {
					container.stop();
					return;
				}
			}
			@Override
			public void stop() {
				resetSmithing();
			}
		}, 2);	
	}
	
	private int getAmountToAdd() {
		switch(item){
		case 863: case 864: case 865: case 866: case 867: case 868:
			return 5;
		case 39: case 40: case 41: case 42: case 43: case 44:
			return 15;
		}
		return 1;
	}
}