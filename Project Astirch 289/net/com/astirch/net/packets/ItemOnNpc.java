package com.astirch.net.packets;

import com.astirch.game.model.items.UseItem;
import com.astirch.game.model.npcs.NPCHandler;
import com.astirch.game.model.players.Client;
import com.astirch.game.model.players.PacketType;


public class ItemOnNpc implements PacketType {

	@Override
	public void processPacket(Client c, int packetType, int packetSize) {
		c.faceUpdate(0);
		c.npcIndex = 0;
		c.playerIndex = 0;
		if (c.followId > 0 || c.followId2 > 0)
			c.getPA().resetFollow();
		int itemId = c.getInStream().readSignedWordA();
		int i = c.getInStream().readSignedWordA();
		int slot = c.getInStream().readSignedWordBigEndian();
		int npcId = NPCHandler.npcs[i].npcType;
		
		UseItem.ItemonNpc(c, itemId, npcId, slot);
	}
}
