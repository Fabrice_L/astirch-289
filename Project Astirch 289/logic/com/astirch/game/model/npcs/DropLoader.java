package com.astirch.game.model.npcs;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.astirch.Server;
import com.astirch.engine.util.Misc;
import com.astirch.game.model.players.Client;
import com.astirch.game.model.players.PlayerHandler;

/**
 * Handles npc drops loaded via xml.
 * @author Fabrice
 */
public class DropLoader {

	public static HashMap<Integer, List<Drop>> npcDrops = new HashMap<Integer, List<Drop>>();
	private final static Logger logger = Logger.getLogger(DropLoader.class.getName());
	Random random = new Random();
	
	public void dropItems(NPC npc) {

		Client p = (Client)PlayerHandler.players[npc.killedBy];
		List<Drop> drops = DropLoader.npcDrops.get(npc.npcType); 
		final double roll = Double.valueOf(random.nextDouble());

		if (drops != null) {
	
			List<Integer> regularDropped = new ArrayList<Integer>();
			List<Integer> regularAmountDropped = new ArrayList<Integer>();
			List<Integer> rareDropped = new ArrayList<Integer>();
			List<Integer> rareAmountDropped = new ArrayList<Integer>();
			for (Drop drop : drops) {
				if (drop.getDropChance() >= 1) {
					Server.itemHandler.createGroundItem(p,drop.getItemID() , npc.getX(), npc.getY(), (drop.getAmount() > 1 ? 1+Misc.random(drop.getAmount()) : drop.getAmount()), p.getId());
				}
				if (roll <drop.getDropChance() && drop.getDropChance() < 1 && drop.getDropChance() >= 0.15) {
					regularDropped.add(drop.getItemID());
					regularAmountDropped.add(drop.getAmount());
				}
				if (roll <drop.getDropChance() && drop.getDropChance() <= 0.15) {
					rareDropped.add(drop.getItemID());
					rareAmountDropped.add(drop.getAmount());
				}
			}
			if (regularDropped.size() > 0) {
				int randomRegular = random.nextInt(regularDropped.size());
				final int regularItem = regularDropped.get(randomRegular);
				final int regularAmount = regularAmountDropped.get(randomRegular);
				Server.itemHandler.createGroundItem(p, regularItem , npc.getX(), npc.getY(), (regularAmount > 1 ? 1+Misc.random(regularAmount) : regularAmount), p.getId());
			}
			if (rareDropped.size() > 0) {
				int randomRare = random.nextInt(rareDropped.size());
				final int rareItem = rareDropped.get(randomRare);
				final int rareAmount = rareAmountDropped.get(randomRare);
				Server.itemHandler.createGroundItem(p, rareItem , npc.getX(), npc.getY(), (rareAmount > 1 ? 1+Misc.random(rareAmount) : rareAmount), p.getId());
			}
		}
	}
	public static void loadDrops() {
		try {
			File dropFile = new File("./config/npcs/npcdrops.xml");	
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbFactory.newDocumentBuilder();
			Document doc = db.parse(dropFile);

			doc.getDocumentElement().normalize();			 
			NodeList nodeList = doc.getElementsByTagName("npc");

			for (int i = 0; i < nodeList.getLength(); i++) {
				Node node = nodeList.item(i);
				/** NPC **/
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					List<Drop> drops = new ArrayList<Drop>();
					Element element = (Element) node;
					int npcId = Integer.parseInt(element.getAttribute("npcid"));
					NodeList dropsNode = node.getChildNodes();
					/** DROPS **/
					for (int j = 0; j < dropsNode.getLength(); j++) {
						Node dropNode = dropsNode.item(j);
						if (dropNode.getNodeType() == Node.ELEMENT_NODE && dropNode.getNodeName().equals("drop")) {
							Element drop = (Element) dropNode;
							int itemId = Integer.parseInt(drop.getChildNodes().item(1).getTextContent());
							int itemAmount = Integer.parseInt(drop.getChildNodes().item(3).getTextContent());
							double itemPercent = Double.parseDouble(drop.getChildNodes().item(5).getTextContent());
							drops.add(new Drop(itemId, itemAmount, itemPercent));
						}
					}
					npcDrops.put(npcId, drops);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.severe("Unable to load drops!");
		}
	}
}