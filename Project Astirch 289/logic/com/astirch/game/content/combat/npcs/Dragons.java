package com.astirch.game.content.combat.npcs;

import com.astirch.engine.util.Misc;
import com.astirch.game.model.npcs.NPC;
import com.astirch.game.model.players.Client;
import com.astirch.game.model.players.PlayerHandler;

public class Dragons {
	
	
	public static void dragonAttack(NPC npc) {
							//green				Blue			Red				Black			KBD
		int dragons[][] = {{941, 50, 7, 0}, {55, 50, 7, 0}, {53, 50, 7, 0}, {54, 55, 10, 3}, {50, 64, 25, 10}};
		int damage = 0;
		Client client = (Client) PlayerHandler.players[npc.oldIndex];
		int anti = client.getPA().antiFire();
		for (int i = 0; i < dragons.length; i++) {
			if (npc.npcType == dragons[i][0])
			if (anti == 0) {
				damage = Misc.random(dragons[i][1]);
				client.sendMessage("You are badly burnt by the dragon fire!");
			} else if (anti == 1) {
				damage = Misc.random(dragons[i][2]);
			} else if (anti == 2) {
				damage = Misc.random(dragons[i][3]);
			} 
		}
		if (client.playerLevel[3] - damage < 0) {
			damage = client.playerLevel[3];	
		}
		client.logoutDelay = System.currentTimeMillis(); // logout delay
		//c.setHitDiff(damage);
		client.handleHitMask(damage);
		client.playerLevel[3] -= damage;
		if (client.playerLevel[3] <= 0){
			client.playerLevel[3] = 0;
		}
		client.getPA().refreshSkill(3);
		client.updateRequired = true;
	}

}
