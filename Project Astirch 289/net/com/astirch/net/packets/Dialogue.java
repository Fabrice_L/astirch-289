package com.astirch.net.packets;

import com.astirch.game.model.players.Client;
import com.astirch.game.model.players.PacketType;


/**
 * Dialogue
 **/
public class Dialogue implements PacketType {

	@Override
	public void processPacket(Client c, int packetType, int packetSize) {
		
		if(c.nextChat > 0) {
			c.getDH().sendDialogues(c.nextChat, c.talkingNpc);
		} else {
			c.getPA().removeAllWindows();
		}
		
	}

}
