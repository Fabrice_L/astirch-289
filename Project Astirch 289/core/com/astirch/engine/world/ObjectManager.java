package com.astirch.engine.world;

import java.util.ArrayList;

import com.astirch.engine.util.Misc;
import com.astirch.game.model.objects.Object;
import com.astirch.game.model.players.Client;
import com.astirch.game.model.players.PlayerHandler;

/**
 * @author Sanity
 */

public class ObjectManager {

	public ArrayList<Object> objects = new ArrayList<Object>();
	private ArrayList<Object> toRemove = new ArrayList<Object>();
	public void process() {
		for (Object o : objects) {
			if (o.tick > 0)
				o.tick--;
			else {
				updateObject(o);
				toRemove.add(o);
			}		
		}
		for (Object o : toRemove) {
			if (isObelisk(o.newId)) {
				int index = getObeliskIndex(o.newId);
				if (activated[index]) {
					activated[index] = false;
					teleportObelisk(index);
				}
			}
			objects.remove(o);	
		}
		toRemove.clear();
	}
	
	public void removeObject(int x, int y) {
		for (int j = 0; j < PlayerHandler.players.length; j++) {
			if (PlayerHandler.players[j] != null) {
				Client c = (Client)PlayerHandler.players[j];
				c.getPA().object(-1, x, y, 0, 10);	
			}	
		}	
	}
	
	public void updateObject(Object o) {
		for (int j = 0; j < PlayerHandler.players.length; j++) {
			if (PlayerHandler.players[j] != null) {
				Client c = (Client)PlayerHandler.players[j];
				c.getPA().object(o.newId, o.objectX, o.objectY, o.face, o.type);			
			}	
		}	
	}
	
	public void placeObject(Object o) {
		for (int j = 0; j < PlayerHandler.players.length; j++) {
			if (PlayerHandler.players[j] != null) {
				Client c = (Client)PlayerHandler.players[j];
				if (c.distanceToPoint(o.objectX, o.objectY) <= 60)
					c.getPA().object(o.objectId, o.objectX, o.objectY, o.face, o.type);
			}	
		}
	}
	
	public Object getObject(int x, int y, int height) {
		for (Object o : objects) {
			if (o.objectX == x && o.objectY == y && o.height == height)
				return o;
		}	
		return null;
	}
	
	public void loadObjects(Client c) {
		if (c == null)
			return;
		for (Object o : objects) {
			if (loadForPlayer(o,c))
				c.getPA().object(o.objectId, o.objectX, o.objectY, o.face, o.type);
		}
		loadCustomSpawns(c);
	}
	
	public void removeObjects(Client c) {
		removeObject(2606, 3876);
		removeObject(2600, 3876);
		removeObject(2613, 3873);
		removeObject(2616, 3874);
		removeObject(2616, 3872);
		removeObject(2620, 3894);
	}
	public void loadCustomSpawns(Client c) {
		//Dragons lair
		c.getPA().checkObjectSpawn(1764, 2270, 4710, 0, 10);
		c.getPA().checkObjectSpawn(2218, 3096, 3504, 0, 10);

		//Wilderness signs
		c.getPA().checkObjectSpawn(3742, 2583, 3878, 3, 10);
		c.getPA().checkObjectSpawn(3742, 2583, 3881, 3, 10);
		
		//stalls for thieving
		c.getPA().checkObjectSpawn(2562, 2655, 3309, 1, 10);
		c.getPA().checkObjectSpawn(2565, 2667, 3301, 3, 10);
		
		//Furnace for smithing
		c.getPA().checkObjectSpawn(3044, 2615, 3872, 2, 10);
		
		//fire for cooking
		c.getPA().checkObjectSpawn(4265, 2618, 3850, 0, 10);
		
		//mining
		c.getPA().checkObjectSpawn(2544, 2615, 3894, 0, 10); //stairs to falador mine
		c.getPA().checkObjectSpawn(2107, 2506, 3885, 0, 10); // runite ore
		c.getPA().checkObjectSpawn(2107, 2508, 3887, 0, 10); // runite ore
		
		//falador min
		c.getPA().checkObjectSpawn(541, 3038, 9788, 0, 10); // stalagmite
		c.getPA().checkObjectSpawn(541, 3039, 9788, 0, 10); // stalagmite
		c.getPA().checkObjectSpawn(541, 3040, 9788, 0, 10); // stalagmite
		c.getPA().checkObjectSpawn(541, 3041, 9788, 0, 10); // stalagmite
		c.getPA().checkObjectSpawn(541, 3042, 9788, 0, 10); // stalagmite
		c.getPA().checkObjectSpawn(541, 3043, 9788, 0, 10); // stalagmite 
		c.getPA().checkObjectSpawn(541, 3044, 9788, 0, 10); // stalagmite 
		c.getPA().checkObjectSpawn(541, 3045, 9788, 0, 10); // stalagmite 
		c.getPA().checkObjectSpawn(541, 3046, 9788, 0, 10); // stalagmite 
		c.getPA().checkObjectSpawn(541, 3047, 9788, 0, 10); // stalagmite
		
		//Prayer Recharge
		c.getPA().checkObjectSpawn(409, 2603, 3869, 2, 10);
		
		//cave entrance
		c.getPA().checkObjectSpawn(2852, 2580, 3875, 2, 10);
		
		//Bank booths
		c.getPA().checkObjectSpawn(2213, 2608, 3870, 1, 10);
		c.getPA().checkObjectSpawn(2213, 2608, 3869, 1, 10);
		c.getPA().checkObjectSpawn(2213, 2608, 3868, 1, 10);
		c.getPA().checkObjectSpawn(2213, 2608, 3867, 1, 10);
		c.getPA().checkObjectSpawn(2213, 2608, 3866, 1, 10);
		c.getPA().checkObjectSpawn(2213, 2608, 3865, 1, 10);
		
		//Runecrafting altar - Edgeville
		
		c.getPA().checkObjectSpawn(7103, 3079, 3486, 1, 10); // pick good rc item
		c.getPA().checkObjectSpawn(2483, 2591, 9883, 1, 10);
		
		// Runecrafting altar teles
		c.getPA().checkObjectSpawn(2465, 2655, 4836, 1, 10); // earth
		c.getPA().checkObjectSpawn(2465, 2582, 4833, 1, 10);// fire
		c.getPA().checkObjectSpawn(2465, 2713, 4832, 1, 10); // water
		c.getPA().checkObjectSpawn(2465, 2269, 4839, 1, 10); // chaos
		c.getPA().checkObjectSpawn(2465, 2144, 4829, 1, 10); // cosmic
		c.getPA().checkObjectSpawn(2465, 2202, 4833, 1, 10); // death
		c.getPA().checkObjectSpawn(2465, 2467, 4828, 1, 10); // law
		c.getPA().checkObjectSpawn(2465, 2783, 4837, 1, 10); //body
		c.getPA().checkObjectSpawn(2465, 2397, 4837, 1, 10); // mind
		c.getPA().checkObjectSpawn(2465, 2587, 9885, 1, 10); // blood
		
	}
	
	public final int IN_USE_ID = 14825;
	public boolean isObelisk(int id) {
		for (int j = 0; j < obeliskIds.length; j++) {
			if (obeliskIds[j] == id)
				return true;
		}
		return false;
	}
	public int[] obeliskIds = {14829,14830,14827,14828,14826,14831};
	public int[][] obeliskCoords = {{3154,3618},{3225,3665},{3033,3730},{3104,3792},{2978,3864},{3305,3914}};
	public boolean[] activated = {false,false,false,false,false,false};
	
	public void startObelisk(int obeliskId) {
		int index = getObeliskIndex(obeliskId);
		if (index >= 0) {
			if (!activated[index]) {
				activated[index] = true;
				addObject(new Object(14825, obeliskCoords[index][0], obeliskCoords[index][1], 0, -1, 10, obeliskId,16));
				addObject(new Object(14825, obeliskCoords[index][0] + 4, obeliskCoords[index][1], 0, -1, 10, obeliskId,16));
				addObject(new Object(14825, obeliskCoords[index][0], obeliskCoords[index][1] + 4, 0, -1, 10, obeliskId,16));
				addObject(new Object(14825, obeliskCoords[index][0] + 4, obeliskCoords[index][1] + 4, 0, -1, 10, obeliskId,16));
			}
		}	
	}
	
	public int getObeliskIndex(int id) {
		for (int j = 0; j < obeliskIds.length; j++) {
			if (obeliskIds[j] == id)
				return j;
		}
		return -1;
	}
	
	public void teleportObelisk(int port) {
		int random = Misc.random(5);
		while (random == port) {
			random = Misc.random(5);
		}
		for (int j = 0; j < PlayerHandler.players.length; j++) {
			if (PlayerHandler.players[j] != null) {
				Client c = (Client)PlayerHandler.players[j];
				int xOffset = c.absX - obeliskCoords[port][0];
				int yOffset = c.absY - obeliskCoords[port][1];
				if (c.goodDistance(c.getX(), c.getY(), obeliskCoords[port][0] + 2, obeliskCoords[port][1] + 2, 1)) {
					c.getPA().startTeleport2(obeliskCoords[random][0] + xOffset, obeliskCoords[random][1] + yOffset, 0);
				}
			}		
		}
	}
	
	public boolean loadForPlayer(Object o, Client c) {
		if (o == null || c == null)
			return false;
		return c.distanceToPoint(o.objectX, o.objectY) <= 60 && c.heightLevel == o.height;
	}
	
	public void addObject(Object o) {
		if (getObject(o.objectX, o.objectY, o.height) == null) {
			objects.add(o);
			placeObject(o);
		}	
	}
	
	public boolean objectExists(final int x, final int y) {
		for (Object o : objects) {
			if (o.objectX == x && o.objectY == y) {
				return true;
			}
		}
		return false;
	}




}