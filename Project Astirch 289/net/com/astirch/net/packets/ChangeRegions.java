package com.astirch.net.packets;

import com.astirch.Server;
import com.astirch.game.model.players.Client;
import com.astirch.game.model.players.PacketType;

/**
 * Change Regions
 */
public class ChangeRegions implements PacketType {

	@Override
	public void processPacket(Client c, int packetType, int packetSize) {
		//Server.objectHandler.updateObjects(c);
		c.faceUpdate(0);
		c.npcIndex = 0;
		c.playerIndex = 0;
		if (c.followId > 0 || c.followId2 > 0)
			c.getPA().resetFollow();
		c.productionAmount = 0;
		c.isSkilling = false;
		Server.objectManager.removeObjects(c);
		Server.itemHandler.reloadItems(c);
		Server.objectManager.loadObjects(c);
		
		c.saveFile = true;
		c.headIcon = c.headIcon;
		/*if(c.skullTimer > 0) {
			c.isSkulled = true;	
			c.headIcon = c.headIcon;
			c.getPA().requestUpdates();
		}*/

	}
		
}
