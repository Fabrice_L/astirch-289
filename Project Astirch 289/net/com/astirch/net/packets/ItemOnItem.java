package com.astirch.net.packets;

/**
 * @author Ryan / Lmctruck30
 */

import com.astirch.game.model.items.UseItem;
import com.astirch.game.model.players.Client;
import com.astirch.game.model.players.PacketType;

public class ItemOnItem implements PacketType {

	@Override
	public void processPacket(Client c, int packetType, int packetSize) {
		if (c.getCrafting().isGemCutting)
			return;
		if (c.getHerblore().isBusy)
			return;
		int usedWithSlot = c.getInStream().readUnsignedWord();
		int itemUsedSlot = c.getInStream().readUnsignedWordA();
		int useWith = c.playerItems[usedWithSlot] - 1;
		int itemUsed = c.playerItems[itemUsedSlot] - 1;
		UseItem.ItemonItem(c, itemUsed, useWith);
	}

}
