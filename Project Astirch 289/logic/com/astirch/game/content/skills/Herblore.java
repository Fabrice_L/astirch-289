package com.astirch.game.content.skills;

import com.astirch.Config;
import com.astirch.engine.event.CycleEvent;
import com.astirch.engine.event.CycleEventContainer;
import com.astirch.engine.event.CycleEventHandler;
import com.astirch.game.model.players.Client;


/**
* Herblore.java
*@author Fabrice
**/

public class Herblore {
		
		
	private Client c;
	public boolean isBusy = false;
	private int[][] information = {{199,249,1,3},{201,251,5,4},{203,253,11,5},{205,255,20,6},{207,257,25,8},
									{3049,2998,30,8},{209,259,40,9},{211,261,48,10},{213,263,54,11},{3051,3000,59,12},
									{215,265,65,13},{2485,2481,67,13},{217,267,70,14},{219,269,75,15}};
	
	private int[][] information2 = {{249,227,91,1,(int)12.5}, {91,221,2428,1,(int)12.5}, //attack potion
									{251,227,93,5,(int)18.75}, {93,235,2446,5,(int)18.75},//antipoison
									{253,227,95,12,25}, {95,225,113,12,25}, //strength potion
									{255,227,97,22,32}, {97,223,2430,22,32}, //restore potion
									};
									//{identifiedHerbId,3dosepot,4dosepot,levelreq,exp}
	public Herblore(Client c) {
		this.c = c;	
	}
	
	public void handleHerbClick(int herbId) {
		for (int j = 0; j < information.length; j++){
			if (information[j][0] == herbId)
				idHerb(j);
		}	
	}
	
	public void handlePotMaking(int item1, int item2) {
		for (int j = 0; j < information2.length; j++){
			if (item1 == information2[j][0] && item2 == information2[j][1])
				makePot(item1, item2);
			else if (item2 == information2[j][0] && item1 == information2[j][1])
				makePot(item2, item1);
		}
	}
	
	public boolean isUnidHerb(int clickId) {
		for (int j = 0; j < information.length; j++)
			if (information[j][0] == clickId)
				return true;	
		return false;
	}
	
	public boolean isIdedHerb(int item) {
		for (int j = 0; j < information2.length; j++)
			if (information2[j][0] == item)
				return true;
		return false;
	}
	
	private void idHerb(final int slot) {
		isBusy = true;
		c.sendMessage("You take a good look at the herb...");
		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
			@Override
			public void execute(CycleEventContainer container) {
				container.stop();
			}
			@Override
			public void stop() {
				if (c.getItems().playerHasItem(information[slot][0])) {
					if (c.playerLevel[c.playerHerblore] >= information[slot][2]) {
							c.getItems().deleteItem(information[slot][0],c.getItems().getItemSlot(information[slot][0]),1);
							c.getItems().addItem(information[slot][1],1);
							c.getPA().addSkillXP(information[slot][3] * Config.SKILLING_EXPERIENCE,c.playerHerblore);
							c.sendMessage("and identify it as a " + c.getItems().getItemName(information[slot][1]) + ".");
					} else {
						c.sendMessage("You need a herblore level of " + information[slot][2] + " to identify this herb.");
					}		
				}
				isBusy = false;
			}
		}, 1);
	}
	
	private void makePot(final int herbId, final int itemUsed) {		
		isBusy = true;
		c.startAnimation(363);
		c.sendMessage("You put the ingredients together...");
		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
			@Override
			public void execute(CycleEventContainer container) {
				container.stop();
			}
			@Override
			public void stop() {
				if (c.getItems().playerHasItem(itemUsed) && c.getItems().playerHasItem(herbId)) {
					int slot = getSlot(herbId);
					if (c.playerLevel[c.playerHerblore] >= information2[slot][3]) {
						c.getItems().deleteItem(herbId,c.getItems().getItemSlot(herbId),1);
						c.getItems().deleteItem(itemUsed,c.getItems().getItemSlot(itemUsed),1);
						c.getItems().addItem(information2[slot][2],1);
						c.sendMessage("and become a " + c.getItems().getItemName(information2[slot][2]) + ".");
						c.getPA().addSkillXP(information2[slot][4] * Config.SKILLING_EXPERIENCE,c.playerHerblore);
					} else {
						c.sendMessage("You need a herblore level of " + information2[slot][3] + " to make this pot.");
					}				
				}
				isBusy = false;
			}
		}, 2);
	}
	
	private int getSlot(int herb) {
		for (int j = 0; j < information2.length; j++)
			if (information2[j][0] == herb)
				return j;
		return -1;	
	}

	
}