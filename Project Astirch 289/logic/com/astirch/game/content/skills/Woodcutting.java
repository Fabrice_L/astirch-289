

package com.astirch.game.content.skills;

import com.astirch.Config;
import com.astirch.engine.event.CycleEvent;
import com.astirch.engine.event.CycleEventContainer;
import com.astirch.engine.event.CycleEventHandler;
import com.astirch.engine.util.Misc;
import com.astirch.game.model.objects.Object;
import com.astirch.game.model.players.*;


public class Woodcutting {
	
	Client c;
	
	private final int VALID_AXE[] = {1349,1351,1353,1355,1357,1359};
	private final int[] AXE_REQS = {1,1,6,6,21,31,41,61};
	private int logType;
	private int exp;
	@SuppressWarnings("unused")
	private int levelReq;
	private int axeType;
	private int emoteTimer = 4;
	private boolean processRunning = false;
	
	public Woodcutting(Client c) {
		this.c = c;
	}
	
	public void startCutting(int logType, int levelReq, int exp) {
		c.turnPlayerTo(c.objectX, c.objectY);
		if (processRunning)
			return;
		if (goodAxe() > 0) {
			if (c.playerLevel[c.playerWoodcutting] >= levelReq) {
				this.logType = logType;
				this.exp = exp;
				this.levelReq = levelReq;
				this.axeType = goodAxe();
				c.wcTimer = getCuttingTimer(logType)  - getAxeTimer();
				if (c.wcTimer < 1) {
					c.wcTimer = 1 + Misc.random(5);
				}
				c.sendMessage("You swing your axe at the tree.");
				cutTree();
				c.startAnimation(getAxeEmote());
			} else {
				resetCutting(c);
				c.sendMessage("You need a woodcutting level of " + levelReq + " to cut this tree.");
				c.startAnimation(65535);
			}		
		} else {
			resetCutting(c);
			c.sendMessage("You need an axe to cut this tree.");
			c.startAnimation(65535);
			c.getPA().resetVariables();
		}
	}
	private boolean spaceInInventory(){
		if (c.getItems().freeSlots() > 0){
			return true;
		}
		return false;
	}
	private void cutTree() {
			if (processRunning) {
				return;
			}
			c.isSkilling = true;
			processRunning = true;
			CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
				@Override
				public void execute(CycleEventContainer container) {
					if (c.isSkilling) {
						if (!spaceInInventory()){
							c.sendMessage("Not enough space in your inventory");
							container.stop();
							return;
						}
						if (goodAxe() <= 0) {
							c.sendMessage("You need an axe to cut this tree.");
							container.stop();
							return;
						}
						if (c.wcTimer > 0) {
							c.wcTimer--;
						}
						axeType = goodAxe();
						if (emoteTimer > 0){
							emoteTimer--;
						}
						if (emoteTimer == 0) {
							c.startAnimation(getAxeEmote());
							emoteTimer = 4;
						}
						if (c.wcTimer == 0 && c.getItems().addItem(logType,1)) {
							processRunning = true;
							c.sendMessage("You get some logs.");
							c.getPA().addSkillXP(exp * Config.SKILLING_EXPERIENCE, c.playerWoodcutting);
							c.getPA().refreshSkill(c.playerWoodcutting);
							c.wcTimer = getCuttingTimer(logType)  - getAxeTimer();
							if (c.wcTimer <= 0) {
								c.wcTimer = 1 + Misc.random(5);
							}
							int stumpChance = Misc.random(getStumpChance(logType));
							if (stumpChance == 0) {
								createStump(c.objectId, c.objectX, c.objectY);
							}
						}
					} else {
						container.stop();
					}
				}
				@Override
				public void stop() {
					resetCutting(c);
				}
			}, 1);
	}

	private void resetCutting(Client c) {
		this.logType = -1;
		this.exp = -1;
		this.levelReq = -1;
		this.axeType = -1;
		c.isSkilling = false;
		c.startAnimation(65535);
		this.processRunning = false;
	}
	
	private int goodAxe() {
		for (int j = VALID_AXE.length - 1; j >= 0; j--) {
			if (c.playerEquipment[c.playerWeapon] == VALID_AXE[j]) {
				if (c.playerLevel[c.playerWoodcutting] >= AXE_REQS[j])
					return VALID_AXE[j];
			}		
		}
		for (int i = 0; i < c.playerItems.length; i++) {
			for (int j = VALID_AXE.length - 1; j >= 0; j--) {
				if (c.playerItems[i] == VALID_AXE[j] + 1) {
					if (c.playerLevel[c.playerWoodcutting] >= AXE_REQS[j])
						return VALID_AXE[j];
				}
			}		
		}
		return - 1;
	}
	
	//same as mining timer
	private int getCuttingTimer(int log) {
		int time = 0;
		switch (log){
		case 1511: //log
			return time += 5 + Misc.random(5);
		case 1521: //oak log
			return time += 7.5 + Misc.random((int) 7.5);
		case 1519: //willow log
			return time += 10 + Misc.random(10);
		case 1517: //maple logs
			return time += 12.5 + Misc.random((int) 12.5);
		case 1515: //yew logs
			return time += 17.5 + Misc.random((int) 17.5);
		case 1513: //magic logs
			return time += 25 + Misc.random(25);
		}
		return time;
	}

	private int getAxeTimer() {
		switch(axeType) {
		case 1351:
			return 0;
		case 1349:
			return 1;
		case 1353:
			return 3;
		case 1355:
			return 5;
		case 1357:
			return 7;
		case 1359:
			return 10;
		}
		return 0;
	}
	private int getAxeEmote() {
		switch(axeType) {
		case 1351: //bronze
			return 879;
		case 1349: //iron
			return 877;
		case 1353: //steel
			return 875;
		case 1355: //mithril
			return 871;
		case 1357: //adamant
			return 869;
		case 1359: //rune
			return 867;
		}
		return 879;
	}
	
	private int getStumpChance(int logType) {
		switch (logType){
		case 1511: //log
			return 1;
		case 1521: //oak log
			return 3;
		case 1519: //willow log
			return 8;
		case 1517: //maple logs
			return 14;
		case 1515: //yew logs
			return 20;
		case 1513: //magic logs
			return 28;
		}
		return 1;
	}
	
	private void createStump(final int objectId, final int x, final int y){
		new Object(1342, x, y, 0, 0, 10, objectId, getRespawnRate(objectId));	
		for (int j = 0; j < PlayerHandler.players.length; j++) {
			if (PlayerHandler.players[j] != null) {
				final Client c2 = (Client)PlayerHandler.players[j]; 
				if (c2.objectId == objectId && c2.objectX == x && c2.objectY == y) {
					c2.getWoodcutting().resetCutting(c2);
				}
			}
		}
	}

	private int getRespawnRate(int objectId) {
		switch(objectId) {
		case 1315:
		case 1316: //evergreen + trees
		case 1318:
		case 1319:
		case 1276:
		case 1277:
		case 1278:
		case 1280:
			return 7;
		case 1281: //oak
			return (int) 13.6;
		case 1308: //willow
			return (int) 13.6;
		case 1307: //maple tree
			return (int) 58.3;
		case 1309: //yew tree
			return 100;
		case 1306: //magic tree
			return 120;
		}
		return 0;
	}

	
}

