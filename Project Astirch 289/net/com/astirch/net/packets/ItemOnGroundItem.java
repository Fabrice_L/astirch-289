package com.astirch.net.packets;

import com.astirch.engine.util.Misc;
import com.astirch.game.model.players.Client;
import com.astirch.game.model.players.PacketType;

public class ItemOnGroundItem implements PacketType {

	@SuppressWarnings("unused")
	@Override
	public void processPacket(Client c, int packetType, int packetSize) {
		c.faceUpdate(0);
		c.npcIndex = 0;
		c.playerIndex = 0;
		if (c.followId > 0 || c.followId2 > 0)
			c.getPA().resetFollow();
		int a1 = c.getInStream().readSignedWordBigEndian();
		int itemUsed = c.getInStream().readSignedWordA();
		int groundItem = c.getInStream().readUnsignedWord();
		int gItemY = c.getInStream().readSignedWordA();
		int itemUsedSlot = c.getInStream().readSignedWordBigEndianA();
		int gItemX = c.getInStream().readUnsignedWord();
		
		switch(itemUsed) {
		
		default:
			if(c.playerRights == 3)
				Misc.println("ItemUsed "+itemUsed+" on Ground Item "+groundItem);
			break;
		}
	}

}
