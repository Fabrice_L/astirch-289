package com.astirch.game.model.items;

import com.astirch.Config;
import com.astirch.engine.clipping.region.ObjectDef;
import com.astirch.engine.util.Misc;
import com.astirch.game.content.skills.Crafting;
import com.astirch.game.model.players.Client;

public class UseItem {

	public static void ItemonObject(Client c, int objectID, int objectX,
			int objectY, int itemId) {
		c.objectX = objectX;
		c.objectY = objectY;
		if (!c.getItems().playerHasItem(itemId, 1))
			return;
		if (ObjectDef.getObjectDef(objectID).name.equalsIgnoreCase("furnace")
				&& itemId == 2357) {
			Crafting.jewelryInterface(c);
		}
		switch (objectID) {
		case 2783:
			c.getSmithingInt().showSmithInterface(itemId);
			break;
		case 409:
			if (c.getPrayer().isBone(itemId))
				c.getPrayer().bonesOnAltar(itemId);
			break;
		case 2728:
		case 4265:
			c.getCooking().itemOnObject(itemId, objectID);
			break;
		case 7103: // runecrafting
			if (itemId == 1438) { // air
				c.sendMessage("You use the air talisman to teleport to the altar");
				c.teleportToX = 2845;
				c.teleportToY = 4832;
			} else if (itemId == 1440) { // earth
				c.sendMessage("You use the earth talisman to teleport to the altar");
				c.teleportToX = 2660;
				c.teleportToY = 4839;
			} else if (itemId == 1442) { // fire
				c.sendMessage("You use the fire talisman to teleport to the altar");
				c.teleportToX = 2584;
				c.teleportToY = 4836;
			} else if (itemId == 1444) { // water
				c.sendMessage("You use the water talisman to teleport to the altar");
				c.teleportToX = 2713;
				c.teleportToY = 4836;
			} else if (itemId == 1446) { // body 
				c.sendMessage("You use the body talisman to teleport to the altar");
				c.teleportToX = 2791;
				c.teleportToY = 4839;
			} else if (itemId == 1448) { // mind 
				c.sendMessage("You use the mind talisman to teleport to the altar");
				c.teleportToX = 2403;
				c.teleportToY = 4843;
			} else if (itemId == 1450) { // blood
				c.sendMessage("You use the blood talisman to teleport to the altar");
				c.teleportToX = 2588;
				c.teleportToY = 9880;
			} else if (itemId == 1452) { // chaos
				c.sendMessage("You use the chaos talisman to teleport to the altar");
				c.teleportToX = 2269;
				c.teleportToY = 4843;
			} else if (itemId == 1454) { // cosmic
				c.sendMessage("You use the cosmic talisman to teleport to the altar");
				c.teleportToX = 2138;
				c.teleportToY = 4831;
			} else if (itemId == 1456) { // death
				c.sendMessage("You use the death talisman to teleport to the altar");
				c.teleportToX = 2207;
				c.teleportToY = 4836;
			} else if (itemId == 1458) { // law
				c.sendMessage("You use the law talisman to teleport to the altar");
				c.teleportToX = 2467;
				c.teleportToY = 4834;
			} else {

				c.sendMessage("You need a talisman to teleport!");
			}
			break;
		default:
			if (c.playerRights == 3)
				Misc.println("Player At Object id: " + objectID
						+ " with Item id: " + itemId);
			break;
		}

	}

	public static void ItemonItem(Client c, int itemUsed, int useWith) {
		c.getHerblore().handlePotMaking(itemUsed, useWith);
		if (itemUsed == 1759 || useWith == 1759) {
			Crafting.stringAmulet(c, itemUsed, useWith);
		}
		if (itemUsed == 946 || useWith == 946)
			c.getFletching().handleLog(itemUsed, useWith);
		if (itemUsed == 53 || useWith == 53 || itemUsed == 52 || useWith == 52)
			c.getFletching().makeArrows(itemUsed, useWith);
		if (itemUsed == 1733 || useWith == 1733)
			c.getCrafting().handleLeather(itemUsed, useWith);
		if (itemUsed == 1755 || useWith == 1755)
			c.getCrafting().handleChisel(itemUsed, useWith);
		if (itemUsed == 590 || useWith == 590)
			c.getFiremaking().checkLog(itemUsed, useWith);
		if (itemUsed == 9142 && useWith == 9190 || itemUsed == 9190
				&& useWith == 9142) {
			if (c.playerLevel[c.playerFletching] >= 58) {
				int boltsMade = c.getItems().getItemAmount(itemUsed) > c
						.getItems().getItemAmount(useWith) ? c.getItems()
						.getItemAmount(useWith) : c.getItems().getItemAmount(
						itemUsed);
				c.getItems().deleteItem(useWith,
						c.getItems().getItemSlot(useWith), boltsMade);
				c.getItems().deleteItem(itemUsed,
						c.getItems().getItemSlot(itemUsed), boltsMade);
				c.getItems().addItem(9241, boltsMade);
				c.getPA().addSkillXP(
						boltsMade * 6 * Config.SKILLING_EXPERIENCE,
						c.playerFletching);
			} else {
				c.sendMessage("You need a fletching level of 58 to fletch this item.");
			}
		}
		if (itemUsed == 9143 && useWith == 9191 || itemUsed == 9191
				&& useWith == 9143) {
			if (c.playerLevel[c.playerFletching] >= 63) {
				int boltsMade = c.getItems().getItemAmount(itemUsed) > c
						.getItems().getItemAmount(useWith) ? c.getItems()
						.getItemAmount(useWith) : c.getItems().getItemAmount(
						itemUsed);
				c.getItems().deleteItem(useWith,
						c.getItems().getItemSlot(useWith), boltsMade);
				c.getItems().deleteItem(itemUsed,
						c.getItems().getItemSlot(itemUsed), boltsMade);
				c.getItems().addItem(9242, boltsMade);
				c.getPA().addSkillXP(
						boltsMade * 7 * Config.SKILLING_EXPERIENCE,
						c.playerFletching);
			} else {
				c.sendMessage("You need a fletching level of 63 to fletch this item.");
			}
		}
		if (itemUsed == 9143 && useWith == 9192 || itemUsed == 9192
				&& useWith == 9143) {
			if (c.playerLevel[c.playerFletching] >= 65) {
				int boltsMade = c.getItems().getItemAmount(itemUsed) > c
						.getItems().getItemAmount(useWith) ? c.getItems()
						.getItemAmount(useWith) : c.getItems().getItemAmount(
						itemUsed);
				c.getItems().deleteItem(useWith,
						c.getItems().getItemSlot(useWith), boltsMade);
				c.getItems().deleteItem(itemUsed,
						c.getItems().getItemSlot(itemUsed), boltsMade);
				c.getItems().addItem(9243, boltsMade);
				c.getPA().addSkillXP(
						boltsMade * 7 * Config.SKILLING_EXPERIENCE,
						c.playerFletching);
			} else {
				c.sendMessage("You need a fletching level of 65 to fletch this item.");
			}
		}
		if (itemUsed == 9144 && useWith == 9193 || itemUsed == 9193
				&& useWith == 9144) {
			if (c.playerLevel[c.playerFletching] >= 71) {
				int boltsMade = c.getItems().getItemAmount(itemUsed) > c
						.getItems().getItemAmount(useWith) ? c.getItems()
						.getItemAmount(useWith) : c.getItems().getItemAmount(
						itemUsed);
				c.getItems().deleteItem(useWith,
						c.getItems().getItemSlot(useWith), boltsMade);
				c.getItems().deleteItem(itemUsed,
						c.getItems().getItemSlot(itemUsed), boltsMade);
				c.getItems().addItem(9244, boltsMade);
				c.getPA().addSkillXP(
						boltsMade * 10 * Config.SKILLING_EXPERIENCE,
						c.playerFletching);
			} else {
				c.sendMessage("You need a fletching level of 71 to fletch this item.");
			}
		}
		if (itemUsed == 9144 && useWith == 9194 || itemUsed == 9194
				&& useWith == 9144) {
			if (c.playerLevel[c.playerFletching] >= 58) {
				int boltsMade = c.getItems().getItemAmount(itemUsed) > c
						.getItems().getItemAmount(useWith) ? c.getItems()
						.getItemAmount(useWith) : c.getItems().getItemAmount(
						itemUsed);
				c.getItems().deleteItem(useWith,
						c.getItems().getItemSlot(useWith), boltsMade);
				c.getItems().deleteItem(itemUsed,
						c.getItems().getItemSlot(itemUsed), boltsMade);
				c.getItems().addItem(9245, boltsMade);
				c.getPA().addSkillXP(
						boltsMade * 13 * Config.SKILLING_EXPERIENCE,
						c.playerFletching);
			} else {
				c.sendMessage("You need a fletching level of 58 to fletch this item.");
			}
		}
		if (itemUsed == 1601 && useWith == 1755 || itemUsed == 1755
				&& useWith == 1601) {
			if (c.playerLevel[c.playerFletching] >= 63) {
				c.getItems()
						.deleteItem(1601, c.getItems().getItemSlot(1601), 1);
				c.getItems().addItem(9192, 15);
				c.getPA().addSkillXP(8 * Config.SKILLING_EXPERIENCE,
						c.playerFletching);
			} else {
				c.sendMessage("You need a fletching level of 63 to fletch this item.");
			}
		}
		if (itemUsed == 1607 && useWith == 1755 || itemUsed == 1755
				&& useWith == 1607) {
			if (c.playerLevel[c.playerFletching] >= 65) {
				c.getItems()
						.deleteItem(1607, c.getItems().getItemSlot(1607), 1);
				c.getItems().addItem(9189, 15);
				c.getPA().addSkillXP(8 * Config.SKILLING_EXPERIENCE,
						c.playerFletching);
			} else {
				c.sendMessage("You need a fletching level of 65 to fletch this item.");
			}
		}
		if (itemUsed == 1605 && useWith == 1755 || itemUsed == 1755
				&& useWith == 1605) {
			if (c.playerLevel[c.playerFletching] >= 71) {
				c.getItems()
						.deleteItem(1605, c.getItems().getItemSlot(1605), 1);
				c.getItems().addItem(9190, 15);
				c.getPA().addSkillXP(8 * Config.SKILLING_EXPERIENCE,
						c.playerFletching);
			} else {
				c.sendMessage("You need a fletching level of 71 to fletch this item.");
			}
		}
		if (itemUsed == 1603 && useWith == 1755 || itemUsed == 1755
				&& useWith == 1603) {
			if (c.playerLevel[c.playerFletching] >= 73) {
				c.getItems()
						.deleteItem(1603, c.getItems().getItemSlot(1603), 1);
				c.getItems().addItem(9191, 15);
				c.getPA().addSkillXP(8 * Config.SKILLING_EXPERIENCE,
						c.playerFletching);
			} else {
				c.sendMessage("You need a fletching level of 73 to fletch this item.");
			}
		}
		if (itemUsed == 1615 && useWith == 1755 || itemUsed == 1755
				&& useWith == 1615) {
			if (c.playerLevel[c.playerFletching] >= 73) {
				c.getItems()
						.deleteItem(1615, c.getItems().getItemSlot(1615), 1);
				c.getItems().addItem(9193, 15);
				c.getPA().addSkillXP(8 * Config.SKILLING_EXPERIENCE,
						c.playerFletching);
			} else {
				c.sendMessage("You need a fletching level of 73 to fletch this item.");
			}
		}
		if (itemUsed == 2368 && useWith == 2366 || itemUsed == 2366
				&& useWith == 2368) {
			c.getItems().deleteItem(2368, c.getItems().getItemSlot(2368), 1);
			c.getItems().deleteItem(2366, c.getItems().getItemSlot(2366), 1);
			c.getItems().addItem(1187, 1);
		}

		switch (itemUsed) {
		default:
			if (c.playerRights == 3)
				Misc.println("Player used Item id: " + itemUsed
						+ " with Item id: " + useWith);
			break;
		}
	}

	public static void ItemonNpc(Client c, int itemId, int npcId, int slot) {
		switch (itemId) {

		default:
			if (c.playerRights == 3)
				Misc.println("Player used Item id: " + itemId
						+ " with Npc id: " + npcId + " With Slot : " + slot);
			break;
		}

	}

}
