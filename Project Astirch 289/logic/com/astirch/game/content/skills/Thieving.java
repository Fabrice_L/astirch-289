package com.astirch.game.content.skills;

import java.util.Random;

import com.astirch.Config;
import com.astirch.game.model.items.Item;
import com.astirch.game.model.objects.Object;
import com.astirch.game.model.players.Client;

public class Thieving {

	private final Client c;
	private final int EMOTE = 832;
	

	public Thieving(final Client c) {
		this.c = c;
	}

	public void stealFromStall(final int xp, final int level, final int i, final int x, final int y) {	
		if (System.currentTimeMillis() - c.lastThieve < 2500) {
			return;
		}
		int id = getItemId(i);
		c.turnPlayerTo(c.objectX, c.objectY);
		if (c.playerLevel[c.playerThieving] >= level) {
			if (c.getItems().addItem(id,1)) {
				c.startAnimation(EMOTE);
				c.getPA().addSkillXP(xp * Config.SKILLING_EXPERIENCE, c.playerThieving);
				c.lastThieve = System.currentTimeMillis();
				c.sendMessage("You steal a " + Item.getItemName(id) + ".");
				new Object(634, x, y, 0, getObjectFace(x, y), 10, i, getRespawnRate(i));
			}
		} else {
			c.sendMessage("Your thieving level must be " + level + " or higher to theive from this stall");
		}
	}

	private int getRespawnRate(int i) {
		switch(i){
		case 2561:
			return 3;
		case 2560:
			return 5;
		case 2563:
			return 15;
		case 2565:
			return 30;
		case 2564:
			return 80;
		case 2562:
			return 180;
		}
		return -1;
	}

	private int getItemId(int i) {
		Random random = new Random();
		int [] bakersStall = {2309, 1891, 2327, 1901};
		int [] gemStall = {1623, 1621, 1619, 1617};
		switch(i){
		case 2561:
			return bakersStall[random.nextInt(bakersStall.length)];
		case 2560:
			return 950;
		case 2563:
			return 958;
		case 2565:
			return 442;
		case 2564:
			return 2007;
		case 2562:
			return gemStall[random.nextInt(gemStall.length)];
		}
		return -1;
	}
	
	private int getObjectFace(int objectX, int objectY) {
		if (objectX == 2667) {
			return 3;
		}
		if (objectX == 2655 || objectX == 2656) {
			return 1;
		}
		if (objectY == 3314) {
			return 2;
		}
		return 0;
	}

}
