package com.astirch.game.content.skills;

import com.astirch.Config;
import com.astirch.game.model.players.Client;

public class Runecrafting {

	Client c;
	private final int essenceID = 1436;
	private int essenceAmount;
	public int talismen[] = {1438, 1440, 1442, 1444, 1446, 1448, 1450, 1452, 1454, 1456, 1458};
	int multiTalismen = 0;
	
	public Runecrafting(final Client c) {
		this.c = c;
	}
	public void talismenCheck() {
		for(int y = 0; y < talismen.length; y++) {
				if(c.getItems().playerHasItem(talismen[y])) {
					multiTalismen++;
				}
		}
		if(multiTalismen < 2) {
			multiTalismen = 0;
		if (c.getItems().playerHasItem(1438)) { // air 
		c.sendMessage("You use the air talisman to teleport to the altar");
		c.teleportToX = 2845;
		c.teleportToY = 4832;
	} else if (c.getItems().playerHasItem(1440)) { // earth
		c.sendMessage("You use the earth talisman to teleport to the altar");
		c.teleportToX = 2660;
		c.teleportToY = 4839;
	} else if (c.getItems().playerHasItem(1442)) { // fire
		c.sendMessage("You use the fire talisman to teleport to the altar");
		c.teleportToX = 2584;
		c.teleportToY = 4836;
	} else if (c.getItems().playerHasItem(1444)) { // water
		c.sendMessage("You use the water talisman to teleport to the altar");
		c.teleportToX = 2713;
		c.teleportToY = 4836;
	} else if (c.getItems().playerHasItem(1446)) { // body 
		c.sendMessage("You use the body talisman to teleport to the altar");
		c.teleportToX = 2791;
		c.teleportToY = 4839;
	} else if (c.getItems().playerHasItem(1448)) { // mind 
		c.sendMessage("You use the mind talisman to teleport to the altar");
		c.teleportToX = 2403;
		c.teleportToY = 4843;
	} else if(c.getItems().playerHasItem(1450)) { // blood
		c.sendMessage("You use the blood talisman to teleport to the altar");
		c.teleportToX = 2588;
		c.teleportToY = 9880;
	} else if (c.getItems().playerHasItem(1452)) { // chaos
		c.sendMessage("You use the chaos talisman to teleport to the altar");
		c.teleportToX = 2269;
		c.teleportToY = 4843;
	} else if (c.getItems().playerHasItem(1454)) { // cosmic
		c.sendMessage("You use the cosmic talisman to teleport to the altar");
		c.teleportToX = 2138;
		c.teleportToY = 4831;
	} else if (c.getItems().playerHasItem(1456)) { // death
		c.sendMessage("You use the death talisman to teleport to the altar");
		c.teleportToX = 2207;
		c.teleportToY = 4836;
	} else if (c.getItems().playerHasItem(1458)) { // law
		c.sendMessage("You use the law talisman to teleport to the altar");
		c.teleportToX = 2467;
		c.teleportToY = 4834;
	} } else {
		c.sendMessage("You can only carry one talismen to use the teleport!");
		multiTalismen = 0;
	}
	}
	public void craftRune(final int runeReceive, final int reqLvl, final int expPer) {
		essenceAmount = c.getItems().getItemAmount(essenceID);
		if((c.playerLevel[c.playerRunecrafting] >= reqLvl)) {
		if(essenceAmount > 0) {
			c.startAnimation(791);
			c.getItems().deleteItem(essenceID, essenceAmount);
			c.getItems().addItem(runeReceive, essenceAmount);
			
			c.getPA().addSkillXP((expPer * essenceAmount) * Config.SKILLING_EXPERIENCE, c.playerRunecrafting);
		} else {
			c.sendMessage("You need essence to make runes!");
		}
	} else {
		c.sendMessage("Your runecrafting level is not high enough to make these runes!");
	}
	}
}
