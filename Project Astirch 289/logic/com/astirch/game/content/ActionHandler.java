package com.astirch.game.content;

import com.astirch.Config;
import com.astirch.Server;
import com.astirch.engine.event.CycleEvent;
import com.astirch.engine.event.CycleEventContainer;
import com.astirch.engine.event.CycleEventHandler;
import com.astirch.engine.util.Misc;
import com.astirch.game.content.minigames.DragonLair;
import com.astirch.game.model.objects.Object;
import com.astirch.game.model.players.Client;
import com.astirch.game.model.players.PlayerHandler;

public class ActionHandler {

	private Client c;

	public ActionHandler(Client Client) {
		this.c = Client;
	}

	public void firstClickObject(int objectType, int obX, int obY) {
		c.clickObjectType = 0;
		// c.sendMessage("Object type: " + objectType);
		/*
		 * if(c.actionTimer > 0) { return; } c.actionTimer = 4;
		 */
		if (c.getGnomeAgility().agilityObstacle(c, c.objectId)) {
			c.getGnomeAgility().agilityCourse(c, c.objectId);
		}
		if (c.getBarbarianAgility().agilityObstacle(c, c.objectId)) {
			c.getBarbarianAgility().agilityCourse(c, c.objectId);
		}
		switch (objectType) {
		// fissure to dragons
		case 2218:
			if (c.getItems().playerHasItem(4047)) {
				c.sendMessage("You swing your rope down the fissure and start descending.");
				c.startAnimation(828);
				c.getItems().deleteItem(4047, 1);
				CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
					@Override
					public void execute(CycleEventContainer container) {
						container.stop();
					}

					@Override
					public void stop() {
						c.teleportToX = 2270;
						c.teleportToY = 4711;
					}
				}, 1);
			} else {
				c.sendMessage("You need a climbing rope to descend the fissure.");
			}
			break;

		case 1764:
			c.startAnimation(828);
			c.sendMessage("You climb up the rope.");
			CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
				@Override
				public void execute(CycleEventContainer container) {
					container.stop();
				}

				@Override
				public void stop() {
					c.teleportToX = 3097;
					c.teleportToY = 3503;
				}
			}, 1);
			break;

		// stairs
		case 1747:
			if (obX == 2532 && obY == 3545)
				c.getStairsHandler().useLadder(2532, 3546, 1, "up");
			break;
			
		case 3205:
			if (obX == 2532 && obY == 3545)
				c.getStairsHandler().useLadder(2532, 3546, 0, "down");
			break;
			
		case 1755:
			if (obX == 2547 && obY == 9951)
				c.getStairsHandler().useLadder(2548, 3551, 0, "up");
			break;
			
		case 1759:
			if (obX == 2547 && obY == 3551)
				c.getStairsHandler().useLadder(2548, 9951, 0, "down");
			break;
			
		case 2544:
			if (obX == 2615 && obY == 3894)
				c.getStairsHandler().useLadder(3058, 9776, 0, "down");
			break;

		case 1734:
			if (obX == 3059 && obY == 9776)
				c.getStairsHandler().useStairs(2614, 3894, 0, "up");
			break;

		// end of stairs
		case 3044:
			c.getSmithing().sendSmelting();
			break;
		case 2852:
			c.sendMessage("@red@Remember, alot of npcs that were inhere, have been scattered");
			c.sendMessage("@red@all-over the teleport areas!!!");
			c.teleportToX = 2808;
			c.teleportToY = 10002;
			c.heightLevel = 0;
			break;
		case 4500:
			if (obX == 2809 && obY == 10001) {
				c.teleportToX = 2584;
				c.teleportToY = 3876;
				c.heightLevel = 0;
			}
			break;
		// woodcutting
		case 1315:
		case 1316: // evergreen + trees
		case 1318:
		case 1319:
		case 1276:
		case 1277:
		case 1278:
		case 1280:
			c.getWoodcutting().startCutting(1511, 1, 25);
			break;
		case 1281: // oak
			c.getWoodcutting().startCutting(1521, 15, (int) 37.5);
			break;
		case 1308: // willow
			c.getWoodcutting().startCutting(1519, 30, (int) 67.5);
			break;
		case 1307: // maple tree
			c.getWoodcutting().startCutting(1517, 45, 100);
			break;
		case 1309: // yew tree
			c.getWoodcutting().startCutting(1515, 60, 175);
			break;
		case 1306: // magic tree
			c.getWoodcutting().startCutting(1513, 75, 250);
			break;
		// mining
		case 2090:// copper
		case 2091:
			c.mining[0] = 436;
			c.mining[1] = 1;
			c.mining[2] = 18;
			c.getMining().startMining(c.mining[0], c.mining[1], c.mining[2]);
			break;

		case 2094:// tin
			c.mining[0] = 438;
			c.mining[1] = 1;
			c.mining[2] = 18;
			c.getMining().startMining(c.mining[0], c.mining[1], c.mining[2]);
			break;

		case 145856:
		case 2092:
		case 2093: // iron
			c.mining[0] = 440;
			c.mining[1] = 15;
			c.mining[2] = 35;
			c.getMining().startMining(c.mining[0], c.mining[1], c.mining[2]);
			break;

		case 14850:
		case 14851:
		case 14852:
		case 2096:
		case 2097: // coal
			c.mining[0] = 453;
			c.mining[1] = 30;
			c.mining[2] = 50;
			c.getMining().startMining(c.mining[0], c.mining[1], c.mining[2]);
			break;

		case 2098:
		case 2099:
			c.mining[0] = 444;
			c.mining[1] = 40;
			c.mining[2] = 65;
			c.getMining().startMining(c.mining[0], c.mining[1], c.mining[2]);
			break;

		case 2102:
		case 2103:
		case 14853:
		case 14854:
		case 14855: // mith ore
			c.mining[0] = 447;
			c.mining[1] = 55;
			c.mining[2] = 80;
			c.getMining().startMining(c.mining[0], c.mining[1], c.mining[2]);
			break;

		case 2105:
		case 14862: // addy ore
			c.mining[0] = 449;
			c.mining[1] = 70;
			c.mining[2] = 95;
			c.getMining().startMining(c.mining[0], c.mining[1], c.mining[2]);
			break;

		case 2107: // rune ore
			c.mining[0] = 451;
			c.mining[1] = 85;
			c.mining[2] = 125;
			c.getMining().startMining(c.mining[0], c.mining[1], c.mining[2]);
			break;

		/* Shops */
		case 6839:
			c.getShops().openShop(9);
			c.sendMessage("As you look down in the chest, you see a small monkey with a money-pouch");
			c.sendMessage("next to him. I think there is where I should put the coins.");
			break;

		/* Bank */
		case 2213:
			// c.sendMessage("DEBUG");
			// c.getPA().openUpBank();
			// system.out.println("banker");
			c.getDH().sendDialogues(1000, 494);
			break;

		case 2557:
			if (c.getItems().playerHasItem(1523, 1) && c.absX == 3190
					&& c.absY == 3957) {
				c.getPA().movePlayer(3190, 3958, 0);
			} else if (c.getItems().playerHasItem(1523, 1) && c.absX == 3190
					&& c.absY == 3958) {
				c.getPA().movePlayer(3190, 3957, 0);
			}
			break;

		case 1816:
			c.getPA().startTeleport2(2271, 4680, 0);
			break;
		case 1817:
			if (c.objectX == 2271 && c.objectY == 4680)
				DragonLair.enterKBDLair(c);
			if (c.objectX == 2717 && c.objectY == 9801)
				c.getPA().movePlayer(2271, 4680, 0);
			break;
		case 1814:
			// ardy lever
			c.getPA().startTeleport(3153, 3923, 0, "modern");
			break;

		case 2882:
		case 2883:
			if (c.objectX == 3268) {
				if (c.absX < c.objectX) {
					c.getPA().walkTo(1, 0);
				} else {
					c.getPA().walkTo(-1, 0);
				}
			}
			break;

		case 1568:
			c.getPA().movePlayer(2400, 9507, 0);
			break;

		case 272:
			c.getPA().movePlayer(c.absX, c.absY, 1);
			break;

		case 273:
			c.getPA().movePlayer(c.absX, c.absY, 0);
			break;

		case 245:
			c.getPA().movePlayer(c.absX, c.absY + 2, 2);
			break;

		case 246:
			c.getPA().movePlayer(c.absX, c.absY - 2, 1);
			break;

		case 6552:
			if (c.playerMagicBook == 0) {
				c.playerMagicBook = 1;
				c.setSidebarInterface(6, 12855);
				c.autocasting = false;
				c.sendMessage("An ancient wisdomin fills your mind.");
				c.getPA().resetAutocast();
			} else {
				c.setSidebarInterface(6, 1151); // modern
				c.playerMagicBook = 0;
				c.autocasting = false;
				c.sendMessage("You feel a drain on your memory.");
				c.autocastId = -1;
				c.getPA().resetAutocast();
			}
			break;

		case 410:
			if (c.playerMagicBook == 0) {
				c.playerMagicBook = 2;
				c.setSidebarInterface(6, 29999);
				c.sendMessage("An Lunar wisdomin fills your mind.");
				c.getPA().resetAutocast();
			} else {
				c.setSidebarInterface(6, 1151); // modern
				c.playerMagicBook = 0;
				c.sendMessage("You feel a drain on your memory.");
				c.autocastId = -1;
				c.getPA().resetAutocast();
			}
			break;

		case 8959:
			if (c.getX() == 2490 && (c.getY() == 10146 || c.getY() == 10148)) {
				if (c.getPA().checkForPlayer(2490,
						c.getY() == 10146 ? 10148 : 10146)) {
					new Object(6951, c.objectX, c.objectY, c.heightLevel, 1,
							10, 8959, 15);
				}
			}
			break;

		case 10177:
			c.getPA().movePlayer(1890, 4407, 0);
			break;
		case 10230:
			c.getPA().movePlayer(2900, 4449, 0);
			break;
		case 10229:
			c.getPA().movePlayer(1912, 4367, 0);
			break;

		case 2623:
			if (c.absX >= c.objectX)
				c.getPA().walkTo(-1, 0);
			else
				c.getPA().walkTo(-1, 0);
			break;
		// pc boat
		case 14315:
			c.getPA().movePlayer(2661, 2639, 0);
			break;
		case 14314:
			c.getPA().movePlayer(2657, 2639, 0);
			break;

		case 1596:
		case 1597:
			if (c.getY() >= c.objectY)
				c.getPA().walkTo(0, -1);
			else
				c.getPA().walkTo(0, 1);
			break;

		case 14235:
		case 14233:
			if (c.objectX == 2670)
				if (c.absX <= 2670)
					c.absX = 2671;
				else
					c.absX = 2670;
			if (c.objectX == 2643)
				if (c.absX >= 2643)
					c.absX = 2642;
				else
					c.absX = 2643;
			if (c.absX <= 2585)
				c.absY += 1;
			else
				c.absY -= 1;
			c.getPA().movePlayer(c.absX, c.absY, 0);
			break;

		case 14829:
		case 14830:
		case 14827:
		case 14828:
		case 14826:
		case 14831:
			// Server.objectHandler.startObelisk(objectType);
			Server.objectManager.startObelisk(objectType);
			break;

		case 9369:
			if (c.getY() > 5175)
				c.getPA().movePlayer(2399, 5175, 0);
			else
				c.getPA().movePlayer(2399, 5177, 0);
			break;

		// barrows
		// Chest
		case 10284:
			if (c.barrowsKillCount < 5) {
				c.sendMessage("You haven't killed all the brothers");
			}
			if (c.barrowsKillCount == 5
					&& c.barrowsNpcs[c.randomCoffin][1] == 1) {
				c.sendMessage("I have already summoned this npc.");
			}
			if (c.barrowsNpcs[c.randomCoffin][1] == 0
					&& c.barrowsKillCount >= 5) {
				Server.npcHandler.spawnNpc(c, c.barrowsNpcs[c.randomCoffin][0],
						3551, 9694 - 1, 0, 0, 120, 30, 200, 200, true, true);
				c.barrowsNpcs[c.randomCoffin][1] = 1;
			}
			if ((c.barrowsKillCount > 5 || c.barrowsNpcs[c.randomCoffin][1] == 2)
					&& c.getItems().freeSlots() >= 2) {
				c.getPA().resetBarrows();
				c.getItems().addItem(c.getPA().randomRunes(),
						Misc.random(150) + 100);
				if (Misc.random(2) == 1)
					c.getItems().addItem(c.getPA().randomBarrows(), 1);
				c.getPA().startTeleport(3564, 3288, 0, "modern");
			} else if (c.barrowsKillCount > 5 && c.getItems().freeSlots() <= 1) {
				c.sendMessage("You need at least 2 inventory slot opened.");
			}
			break;
		// doors
		case 6749:
			if (obX == 3562 && obY == 9678) {
				c.getPA().object(3562, 9678, 6749, -3, 0);
				c.getPA().object(3562, 9677, 6730, -1, 0);
			} else if (obX == 3558 && obY == 9677) {
				c.getPA().object(3558, 9677, 6749, -1, 0);
				c.getPA().object(3558, 9678, 6730, -3, 0);
			}
			break;
		case 6730:
			if (obX == 3558 && obY == 9677) {
				c.getPA().object(3562, 9678, 6749, -3, 0);
				c.getPA().object(3562, 9677, 6730, -1, 0);
			} else if (obX == 3558 && obY == 9678) {
				c.getPA().object(3558, 9677, 6749, -1, 0);
				c.getPA().object(3558, 9678, 6730, -3, 0);
			}
			break;
		case 6727:
			if (obX == 3551 && obY == 9684) {
				c.sendMessage("You cant open this door..");
			}
			break;
		case 6746:
			if (obX == 3552 && obY == 9684) {
				c.sendMessage("You cant open this door..");
			}
			break;
		case 6748:
			if (obX == 3545 && obY == 9678) {
				c.getPA().object(3545, 9678, 6748, -3, 0);
				c.getPA().object(3545, 9677, 6729, -1, 0);
			} else if (obX == 3541 && obY == 9677) {
				c.getPA().object(3541, 9677, 6748, -1, 0);
				c.getPA().object(3541, 9678, 6729, -3, 0);
			}
			break;
		case 6729:
			if (obX == 3545 && obY == 9677) {
				c.getPA().object(3545, 9678, 6748, -3, 0);
				c.getPA().object(3545, 9677, 6729, -1, 0);
			} else if (obX == 3541 && obY == 9678) {
				c.getPA().object(3541, 9677, 6748, -1, 0);
				c.getPA().object(3541, 9678, 6729, -3, 0);
			}
			break;
		case 6726:
			if (obX == 3534 && obY == 9684) {
				c.getPA().object(3534, 9684, 6726, -4, 0);
				c.getPA().object(3535, 9684, 6745, -2, 0);
			} else if (obX == 3535 && obY == 9688) {
				c.getPA().object(3535, 9688, 6726, -2, 0);
				c.getPA().object(3534, 9688, 6745, -4, 0);
			}
			break;
		case 6745:
			if (obX == 3535 && obY == 9684) {
				c.getPA().object(3534, 9684, 6726, -4, 0);
				c.getPA().object(3535, 9684, 6745, -2, 0);
			} else if (obX == 3534 && obY == 9688) {
				c.getPA().object(3535, 9688, 6726, -2, 0);
				c.getPA().object(3534, 9688, 6745, -4, 0);
			}
			break;
		case 6743:
			if (obX == 3545 && obY == 9695) {
				c.getPA().object(3545, 9694, 6724, -1, 0);
				c.getPA().object(3545, 9695, 6743, -3, 0);
			} else if (obX == 3541 && obY == 9694) {
				c.getPA().object(3541, 9694, 6724, -1, 0);
				c.getPA().object(3541, 9695, 6743, -3, 0);
			}
			break;
		case 6724:
			if (obX == 3545 && obY == 9694) {
				c.getPA().object(3545, 9694, 6724, -1, 0);
				c.getPA().object(3545, 9695, 6743, -3, 0);
			} else if (obX == 3541 && obY == 9695) {
				c.getPA().object(3541, 9694, 6724, -1, 0);
				c.getPA().object(3541, 9695, 6743, -3, 0);
			}
			break;

		// DOORS
		case 1516:
		case 1519:
			if (c.objectY == 9698) {
				if (c.absY >= c.objectY)
					c.getPA().walkTo(0, -1);
				else
					c.getPA().walkTo(0, 1);
				break;
			}
		case 1530:
		case 1531:
		case 1533:
		case 1534:
		case 11712:
		case 11711:
		case 11707:
		case 4696:
		case 4247:
		case 11708:
		case 6725:
		case 3198:
		case 3197:
		case 1512:
			Server.objectHandler.doorHandling(objectType, c.objectX, c.objectY,
					0);
			break;

		case 9319:
			if (c.heightLevel == 0)
				c.getPA().movePlayer(c.absX, c.absY, 1);
			else if (c.heightLevel == 1)
				c.getPA().movePlayer(c.absX, c.absY, 2);
			break;

		case 9320:
			if (c.heightLevel == 1)
				c.getPA().movePlayer(c.absX, c.absY, 0);
			else if (c.heightLevel == 2)
				c.getPA().movePlayer(c.absX, c.absY, 1);
			break;

		case 4496:
		case 4494:
			if (c.heightLevel == 2) {
				c.getPA().movePlayer(c.absX - 5, c.absY, 1);
			} else if (c.heightLevel == 1) {
				c.getPA().movePlayer(c.absX + 5, c.absY, 0);
			}
			break;

		case 4493:
			if (c.heightLevel == 0) {
				c.getPA().movePlayer(c.absX - 5, c.absY, 1);
			} else if (c.heightLevel == 1) {
				c.getPA().movePlayer(c.absX + 5, c.absY, 2);
			}
			break;

		case 4495:
			if (c.heightLevel == 1) {
				c.getPA().movePlayer(c.absX + 5, c.absY, 2);
			}
			break;

		case 5126:
			if (c.absY == 3554)
				c.getPA().walkTo(0, 1);
			else
				c.getPA().walkTo(0, -1);
			break;
		case 1558:
			if (c.absX == 3041 && c.absY == 10308) {
				c.getPA().movePlayer(3040, 10308, 0);
			} else if (c.absX == 3040 && c.absY == 10308) {
				c.getPA().movePlayer(3041, 10308, 0);
			} else if (c.absX == 3040 && c.absY == 10307) {
				c.getPA().movePlayer(3041, 10307, 0);
			} else if (c.absX == 3041 && c.absY == 10307) {
				c.getPA().movePlayer(3040, 10307, 0);
			} else if (c.absX == 3044 && c.absY == 10341) {
				c.getPA().movePlayer(3045, 10341, 0);
			} else if (c.absX == 3045 && c.absY == 10341) {
				c.getPA().movePlayer(3044, 10341, 0);
			} else if (c.absX == 3044 && c.absY == 10342) {
				c.getPA().movePlayer(3045, 10342, 0);
			} else if (c.absX == 3045 && c.absY == 10342) {
				c.getPA().movePlayer(3044, 10343, 0);
			}
			break;
		case 1557:
			if (c.absX == 3023 && c.absY == 10312) {
				c.getPA().movePlayer(3022, 10312, 0);
			} else if (c.absX == 3022 && c.absY == 10312) {
				c.getPA().movePlayer(3023, 10312, 0);
			} else if (c.absX == 3023 && c.absY == 10311) {
				c.getPA().movePlayer(3022, 10311, 0);
			} else if (c.absX == 3022 && c.absY == 10311) {
				c.getPA().movePlayer(3023, 10311, 0);
			}
			break;
		case 3203: // dueling forfeit
			if (c.duelCount > 0) {
				c.sendMessage("You may not forfeit yet.");
				break;
			}
			Client o = (Client) PlayerHandler.players[c.duelingWith];
			if (o == null) {
				c.getTradeAndDuel().resetDuel();
				c.getPA().movePlayer(
						Config.DUELING_RESPAWN_X
								+ (Misc.random(Config.RANDOM_DUELING_RESPAWN)),
						Config.DUELING_RESPAWN_Y
								+ (Misc.random(Config.RANDOM_DUELING_RESPAWN)),
						0);
				break;
			}
			if (c.duelRule[0]) {
				c.sendMessage("Forfeiting the duel has been disabled!");
				break;
			}
			{
				o.getPA().movePlayer(
						Config.DUELING_RESPAWN_X
								+ (Misc.random(Config.RANDOM_DUELING_RESPAWN)),
						Config.DUELING_RESPAWN_Y
								+ (Misc.random(Config.RANDOM_DUELING_RESPAWN)),
						0);
				c.getPA().movePlayer(
						Config.DUELING_RESPAWN_X
								+ (Misc.random(Config.RANDOM_DUELING_RESPAWN)),
						Config.DUELING_RESPAWN_Y
								+ (Misc.random(Config.RANDOM_DUELING_RESPAWN)),
						0);
				o.duelStatus = 6;
				o.getTradeAndDuel().duelVictory();
				c.getTradeAndDuel().resetDuel();
				c.getTradeAndDuel().resetDuelItems();
				o.sendMessage("The other player has forfeited the duel!");
				c.sendMessage("You forfeit the duel!");
				break;
			}

		case 409:
			if (c.playerLevel[5] < c.getPA().getLevelForXP(c.playerXP[5])) {
				c.startAnimation(645);
				c.playerLevel[5] = c.getPA().getLevelForXP(c.playerXP[5]);
				c.sendMessage("You recharge your prayer points.");
				c.getPA().refreshSkill(5);
			} else {
				c.sendMessage("You already have full prayer points.");
			}
			break;
		case 2873:
			if (!c.getItems().ownsCape()) {
				c.startAnimation(645);
				c.sendMessage("Saradomin blesses you with a cape.");
				c.getItems().addItem(2412, 1);
			}
			break;
		case 2875:
			if (!c.getItems().ownsCape()) {
				c.startAnimation(645);
				c.sendMessage("Guthix blesses you with a cape.");
				c.getItems().addItem(2413, 1);
			}
			break;
		case 2874:
			if (!c.getItems().ownsCape()) {
				c.startAnimation(645);
				c.sendMessage("Zamorak blesses you with a cape.");
				c.getItems().addItem(2414, 1);
			}
			break;
			//rc tele
			
		case 2465:
			c.teleportToX = 3088;
			c.teleportToY = 3492;
			break;
		// rc altars
		case 7103:		
			c.getRunecrafting().talismenCheck();
			break;
		case 2478:
			c.getRunecrafting().craftRune(556, 1, 5);
			break;
		case 2481:
			c.getRunecrafting().craftRune(557, 1, (int) 5.5);
			break;
		case 2482:
			c.getRunecrafting().craftRune(554, 14, 7);
			break;
		case 2480:
			c.getRunecrafting().craftRune(555, 5, 6);
			break;
		case 2487:
			c.getRunecrafting().craftRune(562, 35, (int) 8.5);
			break;
		case 2488:
			c.getRunecrafting().craftRune(560, 65, 10);
			break;
		case 2485:
			c.getRunecrafting().craftRune(563, 54, (int) 9.5);
			break;
		case 2479:
			c.getRunecrafting().craftRune(559, 20, (int) 7.5);
			break;
		case 2486:
			c.getRunecrafting().craftRune(558, 1, (int) 5.5);
			break;
		case 2484:
			c.getRunecrafting().craftRune(564, 27, 8);
			break;
		case 2483:
			c.getRunecrafting().craftRune(565, 77, (int) 10.5);
			break;
		
		
		

		default:
			System.out.println("objectClick1: ID = " + objectType + " X = "
					+ obX + " Y = " + obY);
			break;

		}
	}

	public void secondClickObject(int objectType, int obX, int obY) {
		c.clickObjectType = 0;
		switch (objectType) {
		case 2561:
			c.getThieving().stealFromStall(25, 1, objectType, obX, obY);
			break;
		case 2560:
			c.getThieving()
					.stealFromStall((int) 37.5, 20, objectType, obX, obY);
			break;
		case 2563:
			c.getThieving()
					.stealFromStall((int) 67.5, 25, objectType, obX, obY);
			break;
		case 2565:
			c.getThieving().stealFromStall(100, 50, objectType, obX, obY);
			break;
		case 2564:
			c.getThieving().stealFromStall(175, 75, objectType, obX, obY);
			break;
		case 2562:
			c.getThieving().stealFromStall(250, 90, objectType, obX, obY);
			break;
		case 11666:
		case 3044:
			c.getSmithing().sendSmelting();
			break;

		case 2213:
		case 14367:
			c.getPA().openUpBank();
			break;
		case 2558:
			if (System.currentTimeMillis() - c.lastLockPick < 3000
					|| c.freezeTimer > 0)
				break;
			if (c.getItems().playerHasItem(1523, 1)) {
				c.lastLockPick = System.currentTimeMillis();
				if (Misc.random(10) <= 3) {
					c.sendMessage("You fail to pick the lock.");
					break;
				}
				if (c.objectX == 3044 && c.objectY == 3956) {
					if (c.absX == 3045) {
						c.getPA().walkTo2(-1, 0);
					} else if (c.absX == 3044) {
						c.getPA().walkTo2(1, 0);
					}

				} else if (c.objectX == 3038 && c.objectY == 3956) {
					if (c.absX == 3037) {
						c.getPA().walkTo2(1, 0);
					} else if (c.absX == 3038) {
						c.getPA().walkTo2(-1, 0);
					}
				} else if (c.objectX == 3041 && c.objectY == 3959) {
					if (c.absY == 3960) {
						c.getPA().walkTo2(0, -1);
					} else if (c.absY == 3959) {
						c.getPA().walkTo2(0, 1);
					}
				}
			} else {
				c.sendMessage("I need a lockpick to pick this lock.");
			}
			break;
		default:
			System.out.println("objectClick2: ID = " + objectType + " X = "
					+ obX + " Y = " + obY);
			break;
		}
	}

	public void thirdClickObject(int objectType, int obX, int obY) {
		c.clickObjectType = 0;
		c.sendMessage("Object type: " + objectType);
		switch (objectType) {
		// In here
		default:
			System.out.println("objectClick3: ID = " + objectType + " X = "
					+ obX + " Y = " + obY);
			break;
		}
	}

	public void firstClickNpc(int npcType) {
		c.clickNpcType = 0;
		c.npcClickIndex = 0;
		switch (npcType) {
		// shops
		case 520:
			c.getShops().openShop(1);
			break;
		case 683:
			c.getShops().openShop(2);
			break;

		case 546:
			c.getShops().openShop(3);
			break;

		case 219:
			c.getShops().openShop(4);
			break;

		case 1282:
			c.getShops().openShop(5);
			break;
		case 242:
			c.getShops().openShop(6);
			break;
		// fishing
		case 316:
			c.getFishing().setupFishing(317);
			break;

		case 334:
			c.getFishing().setupFishing(389);
			break;

		case 324:
			c.getFishing().setupFishing(377);
			break;

		case 314:
			c.getFishing().setupFishing(335);
			break;

		case 2625:
			c.orbShop = true;
			c.getShops().openShop(37);
			break;
		case 2161:
			c.getDH().sendDialogues(86, npcType);
			break;
		case 162:
			c.getDH().sendDialogues(1, npcType);
			break;
		case 384:
			c.getDH().sendDialogues(3, npcType);
			break;
		case 599:
			c.getPA().showInterface(3559);
			c.canChangeAppearance = true;
			break;
		case 278:
			if (c.cooksA == 0) {
				c.getDH().sendDialogues(100, npcType);
			} else if (c.cooksA == 1) {
				c.getDH().sendDialogues(108, npcType);
			} else if (c.cooksA == 2) {
				c.getDH().sendDialogues(113, npcType);
			}
			if (c.cooksA == 1 && c.getItems().playerHasItem(1933, 1)
					&& c.getItems().playerHasItem(1927, 1)
					&& c.getItems().playerHasItem(1944, 1)) {
				c.getDH().sendDialogues(109, npcType);
			}
			break;
		case 1918:
			if (c.desertT == 2) {
				c.getDH().sendDialogues(62, -1);
			} else if (c.desertT == 1 && c.lastDtKill != 0) {
				c.getDH().sendDialogues(47, npcType);
			} else if (c.desertT == 0) {
				c.getDH().sendDialogues(41, npcType);
			} else {
				c.lastDtKill = 1977;
				c.getDH().sendDialogues(63, npcType);
			}
			break;

		case 1334:
			c.getDH().sendDialogues(32, npcType);
			break;

		case 706:
			c.getDH().sendDialogues(9, npcType);
			break;
		case 462:
			c.getDH().sendDialogues(20, npcType);
			break;
		case 2258:
			c.getDH().sendDialogues(17, npcType);
			break;
		case 639:
			c.getDH().sendDialogues(24, npcType);
			break;
		case 398:
			c.getDH().sendDialogues(325, npcType);
			break;
		case 1599:
			if (c.slayerTask <= 0) {
				c.getDH().sendDialogues(11, npcType);
			} else {
				c.getDH().sendDialogues(13, npcType);
			}
			break;

		case 1526:
			c.getDH().sendDialogues(55, npcType);
			break;

		// case 212:
		case 589:
			c.getDH().sendDialogues(56, npcType);
			break;

		case 1152:
			c.getDH().sendDialogues(16, npcType);
			break;

		case 905:
			c.getDH().sendDialogues(5, npcType);
			break;
		case 460:
			c.getDH().sendDialogues(3, npcType);
			break;

		case 904:
			c.sendMessage("You have " + c.magePoints + " points.");
			break;

		case 812:
			c.getDH().sendDialogues(998, npcType);
			break;

		/*
		 * case 546: c.getDH().sendDialogues(1004, npcType); break;
		 */
		case 548:
			c.getDH().sendDialogues(1008, npcType);
			break;
		case 641:
			c.getDH().sendDialogues(997, npcType);
			break;
		case 530:
			c.getDH().sendDialogues(996, npcType);
			break;

		default:
			// c.getDH().sendDialogues(144, npcType);
			System.out.println("npClick1: ID = " + npcType);
			break;
		}
	}

	public void secondClickNpc(int npcType) {
		c.clickNpcType = 0;
		c.npcClickIndex = 0;
		switch (npcType) {
		// Fishing
		case 314:
			c.getFishing().setupFishing(349);
			break;
		case 334:
			c.getFishing().setupFishing(383);
			break;
		case 324:
			c.getFishing().setupFishing(359);
			break;
		case 316:
			c.getFishing().setupFishing(349);
			break;
		case 494:
		case 495:
		case 496:
		case 497:
		case 498:
		case 499:
			c.getPA().openUpBank();
			break;
		default:
			// c.getDH().sendDialogues(144, npcType);
			System.out.println("npClick2: ID = " + npcType);
			break;

		}
	}

	public void thirdClickNpc(int npcType) {
		c.clickNpcType = 0;
		c.npcClickIndex = 0;
		switch (npcType) {
		/*
		 * case 1526: c.getShops().openShop(19); break;
		 */

		case 553:
			c.getPA().startTeleport2(3252, 3401, 0);
			break;
		default:
			// c.getDH().sendDialogues(144, npcType);
			System.out.println("npClick3: ID = " + npcType);
			break;

		}
	}

}