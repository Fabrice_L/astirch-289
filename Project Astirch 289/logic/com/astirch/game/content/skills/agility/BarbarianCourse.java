package com.astirch.game.content.skills.agility;

import com.astirch.Config;
import com.astirch.engine.event.*;
import com.astirch.engine.util.Misc;
import com.astirch.game.model.players.Client;
import com.astirch.game.model.players.PathFinder;

public class BarbarianCourse {
	
	/**
	 ** @author Fabrice
	 **
	 **/
	@SuppressWarnings("unused")
	private Client c;
	public BarbarianCourse(Client c) {
		this.c = c;
	}	
	private boolean ropeSwing, logBalance, balancingLedge, crumblingWall;
	public  boolean doingAgility;
	private int experience;
	
	private void agilityWalk(final Client c, final int walkAnimation, final int x, final int y) {
		c.isRunning2 = false;
		c.getPA().sendFrame36(173, 0);
		c.playerWalkIndex = walkAnimation;
		c.getPA().requestUpdates();
		c.getPA().walkTo(x ,y);
		c.postProcessing();
	}
	
	private void resetAgilityWalk(final Client c) {
		c.isRunning2 = true;
		c.getPA().sendFrame36(173, 1);
		c.playerWalkIndex = 0x333;
		c.getPA().requestUpdates();
	}
	
	private int[] agilityObject = {
		2282, 2294, 2284, 2302, 1948
	};
	
	public boolean agilityObstacle(final Client c, final int objectType) {
		for (int i = 0; i < agilityObject.length; i++) {
			if (objectType == agilityObject[i]) {
				return true;
			}
		}
		return false;		
	}
	
	public void agilityCourse(final Client c, final int objectType) {
		switch (objectType) {
		case 2282:
			doingAgility = true;
			CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
				@Override
				public void execute(CycleEventContainer container) {
					PathFinder.getPathFinder().findRoute(c, 2551, 3554, false, 0, 0);
					if (c.absX == 2551 && c.absY == 3554) {
						c.turnPlayerTo(c.objectX, c.objectY);
						container.stop();
					}
				}
				@Override
				public void stop() {
					c.sendMessage("You don't need a rope with your agility skills.");
					agilityWalk(c, 1501, 0, -5);
				}
			}, 1);
			CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
				@Override
				public void execute(CycleEventContainer container) {
					if ((c.absX == 2551 && c.absY == 3551) && (Misc.random(c.playerLevel[c.playerAgility]) < 5) && (c.playerLevel[c.playerAgility] < 60)) {
						c.sendMessage("You lose concentration and you fall down into the pit.");
						c.getPA().movePlayer(2551, 9950, 0);
						c.hitDiff = 1 + Misc.random(4);
						c.hitUpdateRequired = true;
						c.playerLevel[3] -= c.hitDiff;
						c.getPA().refreshSkill(3);
						container.stop();
					}
					if (c.absX == 2551 && c.absY == 3549 && doingAgility == true) {
						c.getPA().addSkillXP(22 * Config.SKILLING_EXPERIENCE, c.playerAgility);
						ropeSwing = true;
						container.stop();
					}
				}
				@Override
				public void stop() {
					resetAgilityWalk(c);
					doingAgility = false;
				}
			}, 1);
			break;
		case 2294:
			doingAgility = true;
			CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
				@Override
				public void execute(CycleEventContainer container) {
					PathFinder.getPathFinder().findRoute(c, 2551, 3546, false, 0, 0);
					if (c.absX == 2551 && c.absY == 3546) {
						c.turnPlayerTo(c.objectX, c.objectY);
						container.stop();
					}				
				}
				@Override
				public void stop() {
					agilityWalk(c, 762, -10, 0);
				}
			}, 1);
			CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
				@Override
				public void execute(CycleEventContainer container) {
					if (c.absX == 2541 && c.absY == 3546)
						container.stop();
				}
				@Override
				public void stop() {
					resetAgilityWalk(c);
					c.getPA().addSkillXP((int) 13.7 * Config.SKILLING_EXPERIENCE, c.playerAgility);
					logBalance = true;
					doingAgility = false;
				}
			}, 1);
			break;
		case 2284:
			doingAgility = true;
			c.startAnimation(828);
			CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
				@Override
				public void execute(CycleEventContainer container) {
						container.stop();
				}
				@Override
				public void stop() {
					c.getPA().movePlayer(2537, c.absY, 1);
					c.getPA().addSkillXP((int) 8.2 * Config.SKILLING_EXPERIENCE, c.playerAgility);
					doingAgility = false;
				}
			}, 2);
			break;
		case 2302:
			doingAgility = true;
			CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
				@Override
				public void execute(CycleEventContainer container) {
					PathFinder.getPathFinder().findRoute(c, 2536, 3547, false, 0, 0);
					if (c.absX == 2536 && c.absY == 3547) {
						c.turnPlayerTo(c.objectX, c.objectY);
						container.stop();
					}				
				}
				@Override
				public void stop() {
					agilityWalk(c, 756, -4, 0);
				}
			}, 1);
			CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
				@Override
				public void execute(CycleEventContainer container) {
					if ((c.absX == 2534 && c.absY == 3547) && (Misc.random(c.playerLevel[c.playerAgility]) < 5) && (c.playerLevel[c.playerAgility] < 60)) {
						c.sendMessage("You lose concentration and you fall forward.");
						c.getPA().movePlayer(2534, 3546, 0);
						c.hitDiff = 1 + Misc.random(4);
						c.hitUpdateRequired = true;
						c.playerLevel[3] -= c.hitDiff;
						c.getPA().refreshSkill(3);
						container.stop();
					}
					if (c.absX == 2532 && c.absY == 3547 && doingAgility == true) {
						c.getPA().addSkillXP(22 * Config.SKILLING_EXPERIENCE, c.playerAgility);
						agilityWalk(c, 756, 0, -1);
						balancingLedge = true;
						return;
					}
					if (c.absX == 2532 && c.absY == 3546 && doingAgility == true && balancingLedge) {
						resetAgilityWalk(c);
						container.stop();
					}
				}
					@Override
				public void stop() {
					doingAgility = false;
				}
			}, 1);
			break;
		case 1948:
			doingAgility = true;
			CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
				@Override
				public void execute(CycleEventContainer container) {
					PathFinder.getPathFinder().findRoute(c, c.objectX -1, c.objectY, false, 0, 0);
					if (c.absX == c.objectX -1 && c.absY == c.objectY) {
						c.turnPlayerTo(c.objectX, c.objectY);
						container.stop();
					}				
				}
				@Override
				public void stop() {
					agilityWalk(c, 839, 2, 0);
				}
			}, 1);
			CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
				@Override
				public void execute(CycleEventContainer container) {
					experience = (int) 13.7;
					if (c.absX == 2537)
					container.stop();
					if (c.absX == 2540) {
						crumblingWall = true;
						container.stop();
					}
					if (c.absX == 2543) {
						if (ropeSwing && logBalance && balancingLedge && crumblingWall) {
							c.sendMessage("You successfully finished the barbarian course.");
							experience += (int) 46.2;							
						}
						ropeSwing = false;
						logBalance = false;
						balancingLedge = false;
						crumblingWall = false;
						container.stop();
					}
				}
				@Override
				public void stop() {
					resetAgilityWalk(c);
					c.getPA().addSkillXP(experience * Config.SKILLING_EXPERIENCE, c.playerAgility);
					doingAgility = false;
				}
			}, 1);
			break;
		}
	}
}