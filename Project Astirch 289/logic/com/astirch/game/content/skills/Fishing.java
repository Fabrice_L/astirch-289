package com.astirch.game.content.skills;

import com.astirch.Config;
import com.astirch.engine.event.CycleEvent;
import com.astirch.engine.event.CycleEventContainer;
import com.astirch.engine.event.CycleEventHandler;
import com.astirch.engine.util.Misc;
import com.astirch.game.model.players.Client;
/**
 * Fishing.java
 *
 * @author Sanity
 *
 **/
 
public class Fishing {
	
	private Client c;
	private int fishType;
	private int exp;
	private int req;
	@SuppressWarnings("unused")
	private int equipmentType;
	private final int SALMON_EXP = 70;
	private final int SWORD_EXP = 100;
	private final int SALMON_ID = 331;
	private final int SWORD_ID = 371;
	public boolean processRunning = false;
	
	private final int[] REQS = {1,20,25,40,35,76,81};
	private final int[] FISH_TYPES = {317,335,349,377,359,383,389};
	private final int[] EXP = {10,50,60,80,90,110,115};
	
	public Fishing(Client c) {
		this.c = c;
	}
	
	public void setupFishing(int fishType) {
		if (processRunning) {
			return;
		}
		if (!c.getItems().playerHasItem(getEquipment(fishType))) {
			c.sendMessage("You need a " + c.getItems().getItemName(getEquipment(fishType)) + " to fish here.");
			resetFishing();
			return;
		}
		int slot = getSlot(fishType);
		if (slot > -1) {
			this.req = REQS[slot];
			this.fishType = FISH_TYPES[slot];
			this.equipmentType = getEquipment(fishType);
			this.exp = EXP[slot];
		}
		if (c.playerLevel[c.playerFishing] >= req) {
				c.fishTimer = 3 + Misc.random(2);
				c.startAnimation(getFishingEmote());
				catchFish();
				c.sendMessage("You start fishing.");
		} else {
			c.sendMessage("You need a fishing level of " + req + " to fish here.");
			resetFishing();
		}
	}
	
	private boolean spaceInInventory(){
		if (c.getItems().freeSlots() > 0){
			return true;
		}
		return false;
	}
	
	public void catchFish() {
		if (processRunning) {
			return;
		}
		c.isSkilling = true;
		processRunning = true;
		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
			@Override
			public void execute(CycleEventContainer container) {
				if (c.isSkilling) {
					if (!spaceInInventory()){
						c.sendMessage("Not enough space in your inventory");
						container.stop();
						return;
					}
					if (c.fishTimer > 0) {
						c.fishTimer--;
					}
					int fishOther = Misc.random(1);
					if (c.fishTimer == 0){
						if (!c.getItems().playerHasItem(getEquipment(fishType))) {
							c.sendMessage("You need a " + c.getItems().getItemName(getEquipment(fishType)) + " to fish here.");
							container.stop();
							return;
						}
						if (!c.getItems().playerHasItem(getEquipment2(fishType))) {
							c.sendMessage("You need " + c.getItems().getItemName(getEquipment2(fishType)) + "s to fish here.");
							container.stop();
							return;
						}						
						if (c.playerLevel[c.playerFishing] >= req) {
							c.sendMessage("You catch a fish.");
							c.fishTimer = 2 + Misc.random(2);
							c.startAnimation(getFishingEmote());
								if (fishOther == 0 && canFishOther(fishType)) {
									c.getItems().addItem(otherFishId(fishType),1);
									c.getItems().deleteItem(getEquipment2(fishType), c.getItems().getItemSlot(getEquipment2(fishType)), 1);
									c.getPA().addSkillXP(otherFishXP(fishType),c.playerFishing);
								} else {
									c.getItems().addItem(fishType,1);
									c.getItems().deleteItem(getEquipment2(fishType), c.getItems().getItemSlot(getEquipment2(fishType)), 1);
									c.getPA().addSkillXP(exp * Config.SKILLING_EXPERIENCE,c.playerFishing);
								}
						} else {
							c.sendMessage("You need a fishing level of " + req + " to fish here.");
							container.stop();
						}
					}
				} else {
					container.stop();
				}
			}
			@Override
			public void stop() {
				resetFishing();
			}
		}, 1);
	}
	
	private int getSlot(int fishType) {
		for (int j = 0; j < REQS.length; j++)
			if (FISH_TYPES[j] == fishType)
				return j;
		return -1;
	}
	
	private int getEquipment(int fish) {
		if (fish == 317) //shrimp
			return 303;
		if (fish == 335) //trout + salmon
			return 309;
		if (fish == 349) //pike
			return 307;
		if (fish == 377) //lobs
			return 301;
		if (fish == 359)//tuna
			return 311;
		if (fish == 383)//sharks
			return 311;
		if (fish == 389)//mantas
			return 303;
		return -1;
	}
	
	private int getEquipment2(int fish) {
		if (fish == 335) //trout + salmon
			return 314;
		if (fish == 349) //pike
			return 313;
		return -1;
	}
	
	private boolean canFishOther(int fishType) {			
		if (fishType == 335 && c.playerLevel[c.playerFishing] >= 30)
			return true;
		if (fishType == 359 && c.playerLevel[c.playerFishing] >= 50)
			return true;
		return false;
	}
	
	private int otherFishId(int fishType) {
		if (fishType == 335)
			return SALMON_ID;
		else if (fishType == 359)
			return SWORD_ID;
		return -1;
	}
	
	private int otherFishXP(int fishType) {
		if (fishType == 335)
			return SALMON_EXP;
		else if (fishType == 359)
			return SWORD_EXP;
		return 0;
	}
	
	private int getFishingEmote() {
		switch(fishType) {
		case 317: //net fishing
		case 289:
			return 621;
		case 335: //fly fishing
			return 623;
		case 349: //fishing rod
			return 622;
		case 377: //lobster pot
			return 619;
		case 359: //harpoon
		case 383:
			return 618;
		}
		return -1;
	}
	
	public void resetFishing() {
		this.exp = 0;
		this.fishType = -1;
		this.equipmentType = -1;
		this.req = 0;
		c.startAnimation(65535);
		processRunning = false;
		c.isSkilling = false;
		c.fishTimer = -1;
	}
}