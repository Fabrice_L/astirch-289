package com.astirch.game.content.skills;

import com.astirch.Config;
import com.astirch.engine.event.CycleEvent;
import com.astirch.engine.event.CycleEventContainer;
import com.astirch.engine.event.CycleEventHandler;
import com.astirch.game.model.players.Client;

public class Crafting {

	Client c;
	
	public Crafting(Client c) {
		this.c = c;
	}
	private boolean processRunning = false;
	public boolean isGemCutting = false;
	public int hideType = 0, makeId = 0, craftType = 0, exp = 0, index = 0;
	public void resetCrafting() {
		hideType = 0;
		makeId = 0;
		c.productionAmount= 0;
		c.craftingLeather = false;
		c.isSkilling = false;
		processRunning = false;
		craftType = 0;
	}
	
	public void handleChisel(int id1, int id2) {
		if (id1 == 1755)
			cutGem(id2);
		else
			cutGem(id1);	
	}
	
	public int[][] gems = {{1623,1607,1,50, 888},{1621,1605,27,68, 889},{1619,1603,34,85, 887},{1617,1601,43,108, 886},{1631,1615,55,138, 885}};	
	public void cutGem(final int id) {
		isGemCutting = true;
		for (int j = 0; j < gems.length; j++) {
			if (gems[j][0] == id) {
				c.startAnimation(gems[j][4]);
			}
		}
		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
			@Override
			public void execute(CycleEventContainer container) {
				for (int j = 0; j < gems.length; j++) {
					if (gems[j][0] == id) {
						if (c.playerLevel[c.playerCrafting] >= gems[j][2]) {
							c.getItems().deleteItem(id, c.getItems().getItemSlot(id),1);
							c.getItems().addItem(gems[j][1], 1);
							c.getPA().addSkillXP(gems[j][3] * Config.SKILLING_EXPERIENCE, c.playerCrafting);
						} else {
							c.sendMessage("You need a higher crafting level to cut this gem.");
							container.stop();
							return;
						}
						container.stop();
						return;
					}
				}
				container.stop();
			}
			@Override
			public void stop() {
				resetCutting(c);
			}
		}, 2);		
	}
	
	public void resetCutting(Client c){
		c.startAnimation(65535);
		isGemCutting = false;
	}
	
	public void handleCraftingClick(int clickId) {
		for (int j = 0; j < buttons.length; j++) {
			if (buttons[j][0] == clickId) {
				craftType = buttons[j][1];
				c.productionAmount = buttons[j][2];
				checkRequirements();
				break;
			}
		}	
	}
	
	public void checkRequirements() {
		for (int j = 0; j < expsAndLevels.length; j++) {
			if (expsAndLevels[j][0] == hideType) {
				if (c.playerLevel[c.playerCrafting] >= expsAndLevels[j][1]) {
					if (c.getItems().playerHasItem(hideType, 1)) {
						c.getPA().closeAllWindows();
						exp = expsAndLevels[j][2];
						index = j;
						craftHides(hideType);
					}
				} else {
					c.sendMessage("You need a crafting level of " + expsAndLevels[j][1] + " to craft this.");
					c.getPA().removeAllWindows();
				}
			}
		}	
	}
	
	public void craftHides(final int id) {
		if (processRunning){
			c.getPA().removeAllWindows();
			return;
		}
		processRunning = true;
		c.isSkilling = true;
		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
			@Override
			public void execute(CycleEventContainer container) {
				if (c.productionAmount > 0 && c.isSkilling){
					if (!c.getItems().playerHasItem(1734)){
						c.sendMessage("You need atleast one thread to craft this");
						container.stop();
						return;
					}
					if (!c.getItems().playerHasItem(id,craftType)) {
						container.stop();
						return;
					}
					c.startAnimation(1249);
					c.productionAmount--;
					c.getItems().deleteItem(id, craftType);
					c.getItems().deleteItem(1734, c.getItems().getItemSlot(1734), 1);
					if (getItemToAdd() <= 0) {
						container.stop();
						return;
					}
					c.getItems().addItem(getItemToAdd(), 1);
					c.sendMessage("You succesfully create " + c.getItems().getItemName(getItemToAdd()));
					c.getPA().addSkillXP(exp * Config.SKILLING_EXPERIENCE, c.playerCrafting);
				} else {
					container.stop();
				}
			}
			@Override
			public void stop() {
				resetCrafting();
			}
		}, 2);	
		c.getPA().closeAllWindows();
	}
	
	public int getItemToAdd() {
		if (craftType == 1) {
			return vambs[index];
		} else if (craftType == 2) {
			return chaps[index];
		} else if (craftType == 3) {
			return bodys[index];
		}	
		return -1;
	}
	
	public int[] vambs = {1065,2487,2489,2491};
	public int[] chaps = {1099,2493,2495,2497};
	public int[] bodys = {1135,2499,2501,2503};
	
	
	public void handleLeather(int item1, int item2) {
		if (item1 == 1733) {
			openLeather(item2);
		} else {
			openLeather(item1);
		}
	}
	
	public int[][] buttons = {{34185,1,1},{34184,1,5},{34183,1,10},{34182,1,27},{34189,2,1},{34188,2,5},{34187,2,10},{34186,2,27},{34193,3,1},{34192,3,5},{34191,3,10},{34190,3,27}};
	
	public int[][] expsAndLevels = {{1745,62,57},{2505,66,70},{2507,73,78},{2509,79,86}};
	
	
	public void openLeather(int item) {
		if (item == 1745) {
         	c.getPA().sendFrame126("What would you like to make?", 8879);
         	c.getPA().sendFrame246(8884, 200, 1099); // middle
     		c.getPA().sendFrame246(8883, 200, 1065); // left picture
     		c.getPA().sendFrame246(8885, 200, 1135); // right pic
     		c.getPA().sendFrame126("Vambs", 8889);
     		c.getPA().sendFrame126("Chaps", 8893);
     		c.getPA().sendFrame126("Body", 8897);
			c.getPA().sendFrame164(8880); //green dhide
			hideType = item;
		} else if (item == 2505) {
         	c.getPA().sendFrame126("What would you like to make?", 8879);
         	c.getPA().sendFrame246(8884, 200, 2493); // middle
     		c.getPA().sendFrame246(8883, 200, 2487); // left picture
     		c.getPA().sendFrame246(8885, 200, 2499); // right pic
     		c.getPA().sendFrame126("Vambs", 8889);
     		c.getPA().sendFrame126("Chaps", 8893);
     		c.getPA().sendFrame126("Body", 8897);
        	c.getPA().sendFrame164(8880); //blue
			hideType = item;			
		} else if (item == 2507) {
         	c.getPA().sendFrame126("What would you like to make?", 8879);
         	c.getPA().sendFrame246(8884, 200, 2495); // middle
     		c.getPA().sendFrame246(8883, 200, 2489); // left picture
     		c.getPA().sendFrame246(8885, 200, 2501); // right pic
     		c.getPA().sendFrame126("Vambs", 8889);
     		c.getPA().sendFrame126("Chaps", 8893);
     		c.getPA().sendFrame126("Body", 8897);
			c.getPA().sendFrame164(8880);
			hideType = item;
		} else if (item == 2509) {
         	c.getPA().sendFrame126("What would you like to make?", 8879);
         	c.getPA().sendFrame246(8884, 200, 2497); // middle
     		c.getPA().sendFrame246(8883, 200, 2491); // left picture
     		c.getPA().sendFrame246(8885, 200, 2503); // right pic
     		c.getPA().sendFrame126("Vambs", 8889);
     		c.getPA().sendFrame126("Chaps", 8893);
     		c.getPA().sendFrame126("Body", 8897);
			c.getPA().sendFrame164(8880);
			hideType = item;			
		}
		c.craftingLeather = true;
	}
	
	public static enum jewelryData {
		
		RINGS(new int[][] {{2357, 1635, 5, 15}, {1607, 1637, 20, 40}, {1605, 1639, 27, 55}, {1603, 1641, 34, 70}, {1601, 1643, 43, 85}, {1615, 1645, 55, 100}}),
		NECKLACE(new int[][] {{2357, 1654, 6, 20}, {1607, 1656, 22, 55}, {1605, 1658, 29, 60}, {1603, 1660, 40, 75}, {1601, 1662, 56, 90}, {1615, 1664, 72, 105}}),
		AMULETS(new int[][] {{2357, 1673, 8, 30}, {1607, 1675, 24, 65}, {1605, 1677, 31, 70}, {1603, 1679, 50, 85}, {1601, 1681, 70, 100}, {1615, 1683, 80, 150}});
		
		public int[][] item;
		
		private jewelryData(final int[][] item) {
			this.item = item;
		}	
	}
	
	public static void jewelryInterface(final Client c) {
		c.getPA().showInterface(4161);
		for (final jewelryData i : jewelryData.values()) {
			if (c.getItems().playerHasItem(1592)) {
				for (int j = 0; j < i.item.length; j++) {
					if (c.getItems().playerHasItem(jewelryData.RINGS.item[j][0])) {
						c.getPA().sendFrame34(jewelryData.RINGS.item[j][1], j, 4233, 1);

						if (c.getItems().playerHasItem(1601)){
							c.getPA().sendFrame34(1643, 4, 4233, 1);
						}
					} else {
						c.getPA().sendFrame34(-1, j, 4233, 1);
					}
					c.getPA().sendFrame126("", 4230);
					c.getPA().sendFrame246(4229, 0, -1);
				}
			} else {
				c.getPA().sendFrame246(4229, 120, 1592);
				for (int j = 0; j < i.item.length; j++) {
					c.getPA().sendFrame34(-1, j, 4233, 1);
				}
				c.getPA().sendFrame126("You need a ring mould to craft rings.", 4230);
			}
			if (c.getItems().playerHasItem(1597)) {
				for (int j = 0; j < i.item.length; j++) {
					if (c.getItems().playerHasItem(jewelryData.NECKLACE.item[j][0])) {
						c.getPA().sendFrame34(jewelryData.NECKLACE.item[j][1], j, 4239, 1);
						if (c.getItems().playerHasItem(1601)){
							c.getPA().sendFrame34(1662, 4, 4239, 1);
						}
					} else {
						c.getPA().sendFrame34(-1, j, 4239, 1);
					}
					c.getPA().sendFrame126("", 4236);
					c.getPA().sendFrame246(4235, 0, -1);
				}
			} else {
				c.getPA().sendFrame246(4235, 120, 1597);
				for (int j = 0; j < i.item.length; j++) {
					c.getPA().sendFrame34(-1, j, 4239, 1);
				}
				c.getPA().sendFrame126("You need a necklace mould to craft necklaces.", 4236);
			}
			if (c.getItems().playerHasItem(1595)) {
				for (int j = 0; j < i.item.length; j++) {
					if (c.getItems().playerHasItem(jewelryData.AMULETS.item[j][0])) {
						c.getPA().sendFrame34(jewelryData.AMULETS.item[j][1], j, 4245, 1);
						if (c.getItems().playerHasItem(1601)){
							c.getPA().sendFrame34(1681, 4, 4245, 1);
						}
					} else {
						c.getPA().sendFrame34(-1, j, 4245, 1);
					}
					c.getPA().sendFrame126("", 4242);
					c.getPA().sendFrame246(4241, 0, -1);
				}
			} else {
				c.getPA().sendFrame246(4235, 120, 1597);
				for (int j = 0; j < i.item.length; j++) {
					c.getPA().sendFrame34(-1, j, 4245, 1);
				}
				c.getPA().sendFrame126("You need an amulet mould to craft amulets.", 4242);
			}
		}
	}
	
	public static void jewelryMaking(final Client c, final String type, final int itemId, final int amount) {
		switch (type) {
		case "RING":
			for (int i = 0; i < jewelryData.RINGS.item.length; i++) {
				if (itemId == jewelryData.RINGS.item[i][1]) {
					mouldJewelry(c, jewelryData.RINGS.item[i][0], itemId, amount, jewelryData.RINGS.item[i][2], jewelryData.RINGS.item[i][3]);
				}
			}
			break;
		case "NECKLACE":
			for (int i = 0; i < jewelryData.NECKLACE.item.length; i++) {
				if (itemId == jewelryData.NECKLACE.item[i][1]) {
					mouldJewelry(c, jewelryData.NECKLACE.item[i][0], itemId, amount, jewelryData.NECKLACE.item[i][2], jewelryData.NECKLACE.item[i][3]);
				}
			}
			break;
		case "AMULET":
			for (int i = 0; i < jewelryData.AMULETS.item.length; i++) {
				if (itemId == jewelryData.AMULETS.item[i][1]) {
					mouldJewelry(c, jewelryData.AMULETS.item[i][0], itemId, amount, jewelryData.AMULETS.item[i][2], jewelryData.AMULETS.item[i][3]);
				}
			}
			break;
		}
	}
	
	private static int time;
	
	private static void mouldJewelry(final Client c, final int required, final int itemId, final int amount, final int level, final int xp) {
		if (c.isSkilling) {
			return;
		}
		if (c.playerLevel[12] < level) {
			c.sendMessage("You need a crafting level of "+ level +" to mould this item.");
			return;
		}
		if (!c.getItems().playerHasItem(2357)) {
			c.sendMessage("You need a gold bar to mould this item.");
			return;
		}
		final String itemRequired = c.getItems().getItemName(required);		
		if (!c.getItems().playerHasItem(required)) {
			c.sendMessage("You need "+ ((itemRequired.startsWith("A") || itemRequired.startsWith("E") || itemRequired.startsWith("O")) ? "an" : "a") +" "+ itemRequired.toLowerCase() +" to mould this item.");
			return;
		}
		time = amount;
		c.getPA().removeAllWindows();
		final String itemName = c.getItems().getItemName(itemId);
		c.startAnimation(899);
		c.isSkilling = true;
		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
			@Override
			public void execute(CycleEventContainer container) {
				if (c.isSkilling) {
					if (time == 0) {
						container.stop();
					}
					if (c.getItems().playerHasItem(2357) && c.getItems().playerHasItem(required)) {
						c.getItems().deleteItem(2357, 1);
						c.getItems().deleteItem(required, 1);
						c.getItems().addItem(itemId, 1);
						c.startAnimation(899);
						c.getPA().addSkillXP(xp, 12);
						time--;
						c.sendMessage("You make "+ ((itemName.startsWith("A") || itemName.startsWith("E") || itemName.startsWith("O")) ? "an" : "a") +" "+ itemName.toLowerCase());
					} else {
						container.stop();
					}
				} else {
					container.stop();
				}
			}
			@Override
			public void stop() {
				
			}
		}, 4);
	}
	
	public static enum amuletData {
		GOLD(1673, 1692),
		SAPPHIRE(1675, 1694),
		EMERALD(1677, 1696),
		RUBY(1679, 1698),
		DIAMOND(1681, 1700),
		DRAGONSTONE(1683, 1702),
		ONYX(6579, 6851);
		
		private int amuletId, product;
		
		private amuletData(final int amuletId, final int product) {
			this.amuletId = amuletId;
			this.product = product;
		}
		
		public int getAmuletId() {
			return amuletId;
		}
		
		public int getProduct() {
			return product;
		}
	}
	
	public static void stringAmulet(final Client c, final int itemUsed, final int usedWith) {
		final int amuletId = (itemUsed == 1759 ? usedWith : itemUsed);
		for (final amuletData a : amuletData.values()) {
			if (amuletId == a.getAmuletId()) {
				c.getItems().deleteItem(1759, 1);
				c.getItems().deleteItem(amuletId, 1);
				c.getItems().addItem(a.getProduct(), 1);
				c.getPA().addSkillXP(4, 12);
				c.sendMessage("You make a " + c.getItems().getItemName(a.getProduct()));
			}
		}
	}

}