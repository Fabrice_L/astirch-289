package com.astirch.game.content.minigames;

import com.astirch.Server;
import com.astirch.engine.event.CycleEvent;
import com.astirch.engine.event.CycleEventContainer;
import com.astirch.engine.event.CycleEventHandler;
import com.astirch.game.model.npcs.NPC;
import com.astirch.game.model.players.Client;
import com.astirch.game.model.players.PlayerHandler;

public class DragonLair {
	
	public static void showDragonLairInterface(Client client) {
		client.getPA().sendFrame126("", 6570);
		client.getPA().sendFrame126("", 6664);
		client.getPA().sendFrame126("Kill Points: " + client.dragonKillPoints, 6572);
		client.getPA().walkableInterface(6673);
	}
    
	public static void incrementDragonKillPoints(NPC npc) {
		int [][] dragonPoints = {{941, 1}, {55, 2}, {53, 3}, {54, 4}};
		Client client = (Client) PlayerHandler.players[npc.killedBy];
		if (npc.inDragonsLair()) {
			for (int i = 0; i < dragonPoints.length; i++) {
				if (npc.npcType == dragonPoints[i][0]) {
					client.dragonKillPoints += dragonPoints[i][1];
					client.sendMessage("You gain " + dragonPoints[i][1] + " kill points for killing a " + Server.npcHandler.getNpcName(npc.npcType).replace("_", " ") + ".");
				}
			}
		}
	}
	
	public static void enterKBDLair(final Client client) {
		if (client.dragonKillPoints >= 15) {
			client.startAnimation(828);
			CycleEventHandler.getSingleton().addEvent(client, new CycleEvent() {
				@Override
				public void execute(CycleEventContainer container) {
					container.stop();
				}
				@Override
				public void stop() {
					client.teleportToX = 2717;
					client.teleportToY = 9801;
					client.heightLevel = 0;
					client.dragonKillPoints -= 15;
					client.sendMessage("You enter the King Black Dragon's territory.");
				}
			}, 1);
		} else {
			client.sendMessage("You need atleast 15 kill points to go here.");
		}
	}
}
