package com.astirch.game.content.skills;

import com.astirch.Config;
import com.astirch.engine.event.CycleEvent;
import com.astirch.engine.event.CycleEventContainer;
import com.astirch.engine.event.CycleEventHandler;
import com.astirch.engine.util.Misc;
import com.astirch.game.model.players.Client;

public class Cooking {
	
	Client c;
	
	public Cooking(Client c) {
		this.c = c;
	}
	private boolean processRunning = false;
	private int objectId = -1;
	private int[][] cookingItems = {{317,315,323,1,30},{335,333,323,15,70},{349,351,343,20,80},{331,329,343,25,90},{359,361,367,30,100},{377,379,381,40,120},{371,373,375,45,140},{383,385,387,80,210},{389,391,393,91,216}};
	
	public void itemOnObject(int id, int objectId) {
		c.turnPlayerTo(c.objectX, c.objectY);
		for (int j = 0; j < cookingItems.length; j++) {
			if (cookingItems[j][0] == id)
				cookFish(cookingItems[j][0],j);
		}
		this.objectId = objectId;
	}
	
	public boolean isRange() {
		switch(objectId){
		case 2728:
			return true;
		default:
			return false;
		}
	}
	
	public void cookFish(final int id, final int slot) {
		if (processRunning) {
			return;
		}
		processRunning = true;
		c.isSkilling = true;
		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
			@Override
			public void execute(CycleEventContainer container) {
				if (c.getItems().playerHasItem(id,1) && c.isSkilling) {
					if (c.playerLevel[c.playerCooking] >= cookingItems[slot][3]) {
						c.startAnimation(isRange() ? 883 : 897);
						if (Misc.random(c.playerLevel[c.playerCooking] + 3 - cookingItems[slot][3]) == 1) {
							c.sendMessage("You accidently burn the fish.");
							c.getItems().deleteItem(id, c.getItems().getItemSlot(id), 1);
							c.getItems().addItem(cookingItems[slot][2], 1);
						} else {
							c.getItems().deleteItem(id, c.getItems().getItemSlot(id), 1);
							c.getItems().addItem(cookingItems[slot][1], 1);
							c.getPA().addSkillXP(cookingItems[slot][4] * Config.SKILLING_EXPERIENCE, c.playerCooking);
							c.sendMessage("You succesfully cook the " + c.getItems().getItemName(cookingItems[slot][1]));
						}
					} else {
						c.sendMessage("You need a cooking level of " + cookingItems[slot][3] + " to cook this fish.");
						container.stop();
					}
				} else {
					container.stop();
				}
			}
			@Override
			public void stop() {
				c.isSkilling = false;
				processRunning = false;
			}
		}, 2);
	}
	
	
}