package com.astirch.game.content.skills;

import com.astirch.Config;
import com.astirch.Server;
import com.astirch.engine.clipping.region.Region;
import com.astirch.engine.event.CycleEvent;
import com.astirch.engine.event.CycleEventContainer;
import com.astirch.engine.event.CycleEventHandler;
import com.astirch.game.model.objects.Object;
import com.astirch.game.model.players.Client;


public class Firemaking {

	private final Client c;
	private final int[] logs = {1511,1521,1519,1517,1515,1513};
	private final int[] level = {1,15,30,45,60,75};
	private final int[] XP = {40, 60, 90, 135, (int) 202.5, (int) 303.8};
	public long lastLight;
	public boolean logsLighted = false;
	private int lightTimer = 2500;

	public Firemaking(final Client c) {
		this.c = c;
	}

	public void checkLog(final int item1, final int item2 ) {
		for (int j = 0; j < logs.length;j++) {
			if (logs[j] == item1 || logs[j] == item2) {
				lightFire(j);
				return;
			}
		}
	}

	public void lightFire(final int arrayslot) {
		final int objectX = c.absX;
		final int objectY = c.absY;
		if (c.playerLevel[c.playerFiremaking] >= level[arrayslot]) {
			if (c.getItems().playerHasItem(590) && c.getItems().playerHasItem(logs[arrayslot])) { 
					if (Server.objectManager.objectExists(objectX, objectY)) {
						c.sendMessage("You cannot light a fire here.");
						return;
					}
					c.getItems().deleteItem(logs[arrayslot], c.getItems().getItemSlot(logs[arrayslot]), 1);
					Server.itemHandler.createGroundItem(c, logs[arrayslot], objectX, objectY, 1, c.playerId);
					if (!logsLighted) {
						c.startAnimation(733);
						c.sendMessage("You attempt to light the logs.");
						logsLighted = true;
						CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
							@Override
							public void execute(CycleEventContainer container) {
								container.stop();
							}
								@Override
							public void stop() {
								if(logsLighted) {
								logsLighted = false;
								c.sendMessage("The logs cath fire..");
								c.getPA().addSkillXP(XP[arrayslot] * Config.SKILLING_EXPERIENCE, c.playerFiremaking);
								new Object(2732, objectX, objectY, c.getHeightLevel(), 0, 10, -1, 45);
								Server.itemHandler.removeGroundItem(c, logs[arrayslot], objectX, objectY, false);
								c.startAnimation(65535);
								c.turnPlayerTo(objectX + 1, objectY);
								if (Region.getClipping(objectX - 1, objectY, c.heightLevel, -1, 0)) {
									c.getPA().walkTo(-1, 0);
								} else  {
									c.getPA().walkTo(1, 0);
								}
								c.postProcessing();
								removeFire(logs[arrayslot], objectX, objectY);
								lastLight = System.currentTimeMillis();
								}
									}
						}, System.currentTimeMillis() - lastLight < lightTimer ? 1 : 7);
					}
			}
		}
	}
	
	private void removeFire(int logType, final int x, final int y) {
		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
			@Override
			public void execute(CycleEventContainer container) {
				container.stop();
			}
			@Override
			public void stop() {
				Server.objectManager.removeObject(x, y);
				Server.itemHandler.createGroundItem(c, 592, x, y, 1, c.playerId);
			}
		}, 45);	
	}
}

