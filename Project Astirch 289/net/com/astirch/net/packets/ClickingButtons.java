package com.astirch.net.packets;

import com.astirch.Config;
import com.astirch.engine.event.CycleEvent;
import com.astirch.engine.event.CycleEventContainer;
import com.astirch.engine.event.CycleEventHandler;
import com.astirch.engine.util.Misc;
import com.astirch.game.model.items.GameItem;
import com.astirch.game.model.players.Client;
import com.astirch.game.model.players.PacketType;
import com.astirch.game.model.players.PlayerHandler;

/**
 * Clicking most buttons
 **/
public class ClickingButtons implements PacketType {

	private int[] prayerActions = {21233, 21234, 21235, 70080, 70082, 21236, 21237, 21238, 21239, 21240, 21241, 70084, 70086, 21242, 21243, 21244, 21245, 21246, 21247, 21248, 70088, 2171, 2172, 2173, 70092, 70094};

	@Override
	public void processPacket(final Client c, int packetType, int packetSize) {
		int actionButtonId = Misc.hexToInt(c.getInStream().buffer, 0, packetSize);
		//int actionButtonId = c.getInStream().readShort();
		if (c.getGnomeAgility().doingAgility || c.getBarbarianAgility().doingAgility) {
			return;
		}
		if (c.isDead)
			return;
		if(c.playerRights == 3)	
			Misc.println(c.playerName+ " - actionbutton: "+actionButtonId);
		for(int ir = 0; ir < prayerActions.length; ir++) {
			int pr = prayerActions[ir];
			if(pr == actionButtonId) {
				c.getCombat().handlePrayerDrain();
			}
		}
		switch (actionButtonId){
		case 28215:
			if (System.currentTimeMillis() - c.lastReq < 60000) {
				c.sendMessage("You have to wait "+ (60000 - (System.currentTimeMillis() - c.lastReq))/1000 + " seconds before asking staff for help again");
				return;
			}
			c.lastReq = System.currentTimeMillis();
			if(c.playerRights == 0){
				for (int i = 0; i < PlayerHandler.players.length; i++) {
					if (PlayerHandler.players[i] != null) {
						Client c2 = (Client)PlayerHandler.players[i];
						if (PlayerHandler.staffCount > 0){
							if (c2.playerRights > 0){
								c2.sendMessage("@red@" + Misc.optimizeText(c.playerName) + " is requesting help!");
								c.sendMessage("Your request has been sent to all online staffmembers.");
							}
						} else {
							c.sendMessage("There's no staff online at this specific time.");
						}
					}
				}
			} else {
				c.sendMessage("This is a player only feature.");
			}
			break;
			
		//Smelting
		case 15163:
			if (c.smeltInterface) {
				c.smeltType = 2357;
				c.smeltAmount = 1;
				c.getSmithing().startSmelting(c.smeltType);
			}
		break;	
		
		case 15162:
			if (c.smeltInterface) {
				c.smeltType = 2357;
				c.smeltAmount = 5;
				c.getSmithing().startSmelting(c.smeltType);
			}
		break;	
		
		case 15161:
			if (c.smeltInterface) {
				c.smeltType = 2357;
				c.smeltAmount = 10;
				c.getSmithing().startSmelting(c.smeltType);
			}
		break;	
		
		case 15160:
			if (c.smeltInterface) {
				c.smeltType = 2357;
				c.smeltAmount = 28;
				c.getSmithing().startSmelting(c.smeltType);
			}
		break;
		
		case 15155:
			if (c.smeltInterface) {
				c.smeltType = 2355;
				c.smeltAmount = 1;
				c.getSmithing().startSmelting(c.smeltType);
			}
		break;
		
		case 15154:
			if (c.smeltInterface) {
				c.smeltType = 2355;
				c.smeltAmount = 5;
				c.getSmithing().startSmelting(c.smeltType);
			}
		break;
		
		case 15153:
			if (c.smeltInterface) {
				c.smeltType = 2355;
				c.smeltAmount = 10;
				c.getSmithing().startSmelting(c.smeltType);
			}
		break;
		
		case 15152:
			if (c.smeltInterface) {
				c.smeltType = 2355;
				c.smeltAmount = 28;
				c.getSmithing().startSmelting(c.smeltType);
			}
		break;
		
		case 15147:
			if (c.smeltInterface) {
				c.smeltType = 2349;
				c.smeltAmount = 1;
				c.getSmithing().startSmelting(c.smeltType);
			}
		break;
		
		case 15146:
			if (c.smeltInterface) {
				c.smeltType = 2349;
				c.smeltAmount = 5;
				c.getSmithing().startSmelting(c.smeltType);
			}
		break;
		
		case 10247:
			if (c.smeltInterface) {
				c.smeltType = 2349;
				c.smeltAmount = 10;
				c.getSmithing().startSmelting(c.smeltType);
			}
		break;
		
		case 9110:
			if (c.smeltInterface) {
				c.smeltType = 2349;
				c.smeltAmount = 28;
				c.getSmithing().startSmelting(c.smeltType);
			}
		break;
		
		case 15151:
			if (c.smeltInterface) {
				c.smeltType = 2351;
				c.smeltAmount = 1;
				c.getSmithing().startSmelting(c.smeltType);
			}
		break;
		
		case 15150:
			if (c.smeltInterface) {
				c.smeltType = 2351;
				c.smeltAmount = 5;
				c.getSmithing().startSmelting(c.smeltType);
			}
		break;
		
		case 15149:
			if (c.smeltInterface) {
				c.smeltType = 2351;
				c.smeltAmount = 10;
				c.getSmithing().startSmelting(c.smeltType);
			}
		break;
		
		case 15148:
			if (c.smeltInterface) {
				c.smeltType = 2351;
				c.smeltAmount = 28;
				c.getSmithing().startSmelting(c.smeltType);
			}
		break;
		
		case 15159:
			if (c.smeltInterface) {
				c.smeltType = 2353;
				c.smeltAmount = 1;
				c.getSmithing().startSmelting(c.smeltType);
			}
		break;
		
		case 15158:
			if (c.smeltInterface) {
				c.smeltType = 2353;
				c.smeltAmount = 5;
				c.getSmithing().startSmelting(c.smeltType);
			}
		break;
		
		case 15157:
			if (c.smeltInterface) {
				c.smeltType = 2353;
				c.smeltAmount = 10;
				c.getSmithing().startSmelting(c.smeltType);
			}
		break;
		
		case 15156:
			if (c.smeltInterface) {
				c.smeltType = 2353;
				c.smeltAmount = 28;
				c.getSmithing().startSmelting(c.smeltType);
			}
		break;
				
		case 29017:
			if (c.smeltInterface) {
				c.smeltType = 2359;
				c.smeltAmount = 1;
				c.getSmithing().startSmelting(c.smeltType);
			}
		break;
		
		case 29016:
			if (c.smeltInterface) {
				c.smeltType = 2359;
				c.smeltAmount = 5;
				c.getSmithing().startSmelting(c.smeltType);
			}
		break;
		
		case 24253:
			if (c.smeltInterface) {
				c.smeltType = 2359;
				c.smeltAmount = 10;
				c.getSmithing().startSmelting(c.smeltType);
			}
		break;
		
		case 16062:
			if (c.smeltInterface) {
				c.smeltType = 2359;
				c.smeltAmount = 28;
				c.getSmithing().startSmelting(c.smeltType);
			}
		break;
		
		case 29022:
			if (c.smeltInterface) {
				c.smeltType = 2361;
				c.smeltAmount = 1;
				c.getSmithing().startSmelting(c.smeltType);
			}
		break;
		
		case 29020:
			if (c.smeltInterface) {
				c.smeltType = 2361;
				c.smeltAmount = 5;
				c.getSmithing().startSmelting(c.smeltType);
			}
		break;
		
		case 29019:
			if (c.smeltInterface) {
				c.smeltType = 2361;
				c.smeltAmount = 10;
				c.getSmithing().startSmelting(c.smeltType);
			}
		break;
		
		case 29018:
			if (c.smeltInterface) {
				c.smeltType = 2361;
				c.smeltAmount = 28;
				c.getSmithing().startSmelting(c.smeltType);
			}
		break;
		
		case 29026:
			if (c.smeltInterface) {
				c.smeltType = 2363;
				c.smeltAmount = 1;
				c.getSmithing().startSmelting(c.smeltType);
			}
		break;
		
		case 29025:
			if (c.smeltInterface) {
				c.smeltType = 2363;
				c.smeltAmount = 5;
				c.getSmithing().startSmelting(c.smeltType);
			}
		break;
		
		case 29024:
			if (c.smeltInterface) {
				c.smeltType = 2363;
				c.smeltAmount = 10;
				c.getSmithing().startSmelting(c.smeltType);
			}
		break;
		
		case 29023:
			if (c.smeltInterface) {
				c.smeltType = 2363;
				c.smeltAmount = 28;
				c.getSmithing().startSmelting(c.smeltType);
			}
		break;
		
		case 34185: case 34184: case 34183: case 34182: case 34189: case 34188: case 34187: case 34186: case 34193: case 34192: case 34191: case 34190:
			if (c.craftingLeather)
				c.getCrafting().handleCraftingClick(actionButtonId);
			if (c.getFletching().fletching)
				c.getFletching().handleFletchingClick(actionButtonId);
		break;
				case 19212:
		c.setSidebarInterface(8, 5065);
	break;
	case 19215:
		c.setSidebarInterface(8, 5715);
	break;

			
			case 150:
					c.autoRet = true;
			break;
			case 151:
					c.autoRet = false;
			break;
			//1st tele option
			case 9190:
				if (c.teleAction == 1) {
					//rock crabs
					c.getPA().spellTeleport(2676, 3715, 0);
				} else if (c.teleAction == 2) {
					//barrows
					c.getPA().spellTeleport(3565, 3314, 0);
				} else if (c.teleAction == 3) {
					//godwars
					c.getPA().spellTeleport(2916, 3612, 0);
				} else if (c.teleAction == 4) {
					//varrock wildy
					c.getPA().spellTeleport(3243, 3513, 0);
				} else if (c.teleAction == 5) {
					c.getPA().spellTeleport(3046,9779,0);
				}
				
				if (c.dialogueAction == 10) {
					c.getPA().spellTeleport(2845, 4832, 0);
					c.dialogueAction = -1;
				} else if (c.dialogueAction == 11) {
					c.getPA().spellTeleport(2786, 4839, 0);
					c.dialogueAction = -1;
				} else if (c.dialogueAction == 12) {
					c.getPA().spellTeleport(2398, 4841, 0);
					c.dialogueAction = -1;
				}
				break;
				//mining - 3046,9779,0
			//smithing - 3079,9502,0

			//2nd tele option
			case 9191:
				if (c.teleAction == 1) {
					//tav dungeon
					c.getPA().spellTeleport(2884, 9798, 0);
				} else if (c.teleAction == 2) {
					//pest control
					c.getPA().spellTeleport(2662, 2650, 0);
				} else if (c.teleAction == 3) {
					//kbd
					c.getPA().spellTeleport(3007, 3849, 0);
				} else if (c.teleAction == 4) {
					//graveyard
					c.getPA().spellTeleport(3164, 3685, 0);
				} else if (c.teleAction == 5) {
					c.getPA().spellTeleport(3079,9502,0);
				}
				if (c.dialogueAction == 10) {
					c.getPA().spellTeleport(2796, 4818, 0);
					c.dialogueAction = -1;
				} else if (c.dialogueAction == 11) {
					c.getPA().spellTeleport(2527, 4833, 0);
					c.dialogueAction = -1;
				} else if (c.dialogueAction == 12) {
					c.getPA().spellTeleport(2464, 4834, 0);
					c.dialogueAction = -1;
				}
				break;
			//3rd tele option	

			case 9192:
				if (c.teleAction == 1) {
					//slayer tower
					c.getPA().spellTeleport(3428, 3537, 0);
				} else if (c.teleAction == 2) {
					//tzhaar
					c.getPA().spellTeleport(2444, 5170, 0);
				} else if (c.teleAction == 3) {
					//dag kings
					c.getPA().spellTeleport(2479, 10147, 0);
				} else if (c.teleAction == 4) {
					//44 portals
					c.getPA().spellTeleport(2975, 3873, 0);
				} else if (c.teleAction == 5) {
					c.getPA().spellTeleport(2813,3436,0);
				}
				if (c.dialogueAction == 10) {
					c.getPA().spellTeleport(2713, 4836, 0);
					c.dialogueAction = -1;
				} else if (c.dialogueAction == 11) {
					c.getPA().spellTeleport(2162, 4833, 0);
					c.dialogueAction = -1;
				} else if (c.dialogueAction == 12) {
					c.getPA().spellTeleport(2207, 4836, 0);
					c.dialogueAction = -1;
				}
				break;
			//4th tele option
			case 9193:
				if (c.teleAction == 1) {
					//brimhaven dungeon
					c.getPA().spellTeleport(2710, 9466, 0);
				} else if (c.teleAction == 2) {
					//duel arena
					c.getPA().spellTeleport(3366, 3266, 0);
				} else if (c.teleAction == 3) {
					//chaos elemental
					c.getPA().spellTeleport(3295, 3921, 0);
				} else if (c.teleAction == 4) {
					//gdz
					c.getPA().spellTeleport(3288, 3886, 0);
				} else if (c.teleAction == 5) {
					c.getPA().spellTeleport(2724,3484,0);
					c.sendMessage("For magic logs, try north of the duel arena.");
				}
				if (c.dialogueAction == 10) {
					c.getPA().spellTeleport(2660, 4839, 0);
					c.dialogueAction = -1;
				}
				break;
			//5th tele option
			case 9194:
				if (c.teleAction == 1) {
					//island
					c.getPA().spellTeleport(2895, 2727, 0);
				} else if (c.teleAction == 2) {
					//last minigame spot
					c.sendMessage("Suggest something for this spot on the forums!");
					c.getPA().closeAllWindows();
				} else if (c.teleAction == 3) {
					//last monster spot
					c.sendMessage("Suggest something for this spot on the forums!");
					c.getPA().closeAllWindows();
				} else if (c.teleAction == 4) {
					//ardy lever
					c.getPA().spellTeleport(2561, 3311, 0);
				} else if (c.teleAction == 5) {
					c.getPA().spellTeleport(2812,3463,0);
				}
				if (c.dialogueAction == 10 || c.dialogueAction == 11) {
					c.dialogueId++;
					c.getDH().sendDialogues(c.dialogueId, 0);
				} else if (c.dialogueAction == 12) {
					c.dialogueId = 17;
					c.getDH().sendDialogues(c.dialogueId, 0);
				}
				break;
			

			case 58253:
			//c.getPA().showInterface(15106);
			c.getItems().writeBonus();
			break;
			
			case 59004:
			c.getPA().removeAllWindows();
			break;
			
			case 9178:
				if (c.usingGlory)
					c.getPA().startTeleport(Config.EDGEVILLE_X, Config.EDGEVILLE_Y, 0, "modern");
				if (c.dialogueAction == 2)
					c.getPA().startTeleport(3428, 3538, 0, "modern");
				if (c.dialogueAction == 3)		
					c.getPA().startTeleport(Config.EDGEVILLE_X, Config.EDGEVILLE_Y, 0, "modern");
				if (c.dialogueAction == 4)
					c.getPA().startTeleport(3565, 3314, 0, "modern");
				if (c.dialogueAction == 20) {
					c.getPA().startTeleport(2897, 3618, 4, "modern");
					c.killCount = 0;
				} else if (c.teleAction == 2) {
					//barrows
					c.getPA().spellTeleport(3565, 3314, 0);
				}
				
				if(c.caOption4a) {
					c.getDH().sendDialogues(102, c.npcType);
					c.caOption4a = false;
				}
				if(c.caOption4c) {
					c.getDH().sendDialogues(118, c.npcType);
					c.caOption4c = false;
				}
			break;
			
			case 9179:
				if (c.usingGlory)
					c.getPA().startTeleport(Config.AL_KHARID_X, Config.AL_KHARID_Y, 0, "modern");
				if (c.dialogueAction == 2)
					c.getPA().startTeleport(2884, 3395, 0, "modern");
				if (c.dialogueAction == 3)
					c.getPA().startTeleport(3243, 3513, 0, "modern");
				if (c.dialogueAction == 4)
					c.getPA().startTeleport(2444, 5170, 0, "modern");
				if (c.dialogueAction == 20) {
					c.getPA().startTeleport(2897, 3618, 12, "modern");
					c.killCount = 0;
				} else if (c.teleAction == 2) {
					//assault
					c.getPA().spellTeleport(2605, 3153, 0);
				}
				if(c.caOption4c) {
					c.getDH().sendDialogues(120, c.npcType);
					c.caOption4c = false;
				}	
												if (c.dialogueAction == 33) {
				if (c.votePoints >= 2) {
				c.votePoints -= 2;
				c.getItems().addItem(995, 2000000);
				c.getPA().removeAllWindows();
				c.sendMessage("You have been given 2 Million GP");
				} else {
				c.sendMessage("You do not have enough for this item!");
				}
				}
				if(c.caPlayerTalk1) {
					c.getDH().sendDialogues(125, c.npcType);
					c.caPlayerTalk1 = false;
				}
			break;
			
			case 9180:
				if (c.usingGlory)
					c.getPA().startTeleport(Config.KARAMJA_X, Config.KARAMJA_Y, 0, "modern");
				if (c.dialogueAction == 2)
					c.getPA().startTeleport(2471,10137, 0, "modern");	
				if (c.dialogueAction == 3)
					c.getPA().startTeleport(3363, 3676, 0, "modern");
				if (c.dialogueAction == 4)
					c.getPA().startTeleport(2659, 2676, 0, "modern");
				if (c.dialogueAction == 20) {
					c.getPA().startTeleport(2897, 3618, 8, "modern");
					c.killCount = 0;
				} else if (c.teleAction == 2) {
					//duel arena
					c.getPA().spellTeleport(3366, 3266, 0);
				}
				if(c.caOption4c) {
					c.getDH().sendDialogues(122, c.npcType);
					c.caOption4c = false;
				}
					if (c.dialogueAction == 33) {
				if (c.votePoints >= 3) {
				c.votePoints -= 3;
				c.getItems().addItem(995, 5000000);
				c.getPA().removeAllWindows();
				c.sendMessage("You have been given 5 Million GP");
				} else {
				c.sendMessage("You do not have enough for this item!");
				}
				}
				if(c.caPlayerTalk1) {
					c.getDH().sendDialogues(127, c.npcType);
					c.caPlayerTalk1 = false;
				}
			break;
			
			case 9181:
				if (c.usingGlory)
					c.getPA().startTeleport(Config.MAGEBANK_X, Config.MAGEBANK_Y, 0, "modern");
				if (c.dialogueAction == 2)
					c.getPA().startTeleport(2669,3714, 0, "modern");
				if (c.dialogueAction == 3)	
					c.getPA().startTeleport(2540, 4716, 0, "modern");
				if (c.dialogueAction == 4) {
					c.getPA().startTeleport(3366, 3266, 0, "modern");
					c.sendMessage("Dueling is at your own risk. Refunds will not be given for items lost due to glitches.");
				} else if (c.teleAction == 2) {
					//tzhaar
					c.getPA().spellTeleport(2444, 5170, 0);
				}
				if (c.dialogueAction == 20) {
					//c.getPA().startTeleport(3366, 3266, 0, "modern");
					//c.killCount = 0;
					c.sendMessage("This will be added shortly");
				}
				if(c.caOption4c) {
					c.getDH().sendDialogues(124, c.npcType);
					c.caOption4c = false;
				}
				if (c.dialogueAction == 33) {
				if (c.votePoints >= 5) {
				c.votePoints -= 5;
				c.getItems().addItem(995, 10000000);
				c.getPA().removeAllWindows();
				c.sendMessage("You have been given 10 Million GP");
				} else {
				c.sendMessage("You do not have enough for this item!");
				}
				}
				if(c.caPlayerTalk1) {
					c.getDH().sendDialogues(130, c.npcType);
					c.caPlayerTalk1 = false;
				}
			break;
			
			case 1093:
			case 1094:
			case 1097:
				if (c.autocastId > 0) {
					c.getPA().resetAutocast();
				} else {
					if (c.playerMagicBook == 1) {
						if (c.playerEquipment[c.playerWeapon] == 4675)
							c.setSidebarInterface(0, 1689);
						else
							c.sendMessage("You can't autocast ancients without an ancient staff.");
					} else if (c.playerMagicBook == 0) {
						if (c.playerEquipment[c.playerWeapon] == 4170) {
							c.setSidebarInterface(0, 12050);
						} else {
							c.setSidebarInterface(0, 1829);
						}	
					}
						
				}		
			break;
			
			case 9157:	
				if (c.dialogueAction == 1) {
					c.getPA().startTeleport(2471, 3437, 0, "modern");
				}
				if (c.dialogueAction == 2) {
					if (c.playerLevel[c.playerAgility] < 35) {
						c.getDH().sendDialogues(5, 384);
						return;
					}
					c.getPA().startTeleport(2552, 3558, 0, "modern");
				}
				if(c.DSOption8) {
					c.getDH().sendDialogues(408, 883);
					c.DSOption8 = false;
				}
				if(c.DSOption2) {
					if(c.getItems().playerHasItem(995, 1)) {
						c.getDH().sendDialogues(335, 882);
						c.DSOption2 = false;
					} else {
						c.getDH().sendDialogues(334, 882);
						c.DSOption2 = false;
					}
				}
				
				if (c.dialogueAction == 32) {
				c.getDH().sendDialogues(88, c.npcType);
				}
								if (c.dialogueAction == 33) {
				if (c.votePoints >= 1) {
				c.votePoints--;
				c.getItems().addItem(995, 1000000);
				c.getPA().removeAllWindows();
				c.sendMessage("You have been given 1 Million GP");
				} else {
				c.sendMessage("You do not have enough for this item!");
				}
				}
				if(c.doricOption2) {
					c.getDH().sendDialogues(310, 284);
					c.doricOption2 = false;
				}
				if(c.rfdOption) {
					c.getDH().sendDialogues(26, -1);
					c.rfdOption = false;
				}
				if(c.horrorOption) {
					c.getDH().sendDialogues(35, -1);
					c.horrorOption = false;
				}
				if(c.dtOption) {
					c.getDH().sendDialogues(44, -1);
					c.dtOption = false;
				}
				if(c.dtOption2) {
					if(c.lastDtKill == 0) {
						c.getDH().sendDialogues(65, -1);
					} else {
						c.getDH().sendDialogues(49, -1);
					}
					c.dtOption2 = false;
				}
				
				if(c.caOption2) {
					c.getDH().sendDialogues(106, c.npcType);
					c.caOption2 = false;
				}
				if(c.caOption2a) {
					c.getDH().sendDialogues(102, c.npcType);
					c.caOption2a = false;
				}
				
				if(c.dialogueAction == 1) {
					c.getDH().sendDialogues(38, -1);
				}
				break;
				
			case 9167:
			if(c.DSOption7) {
				c.getDH().sendDialogues(393, 883);
				c.DSOption7 = false;
			}
			if(c.DSOption6) {
				c.getDH().sendDialogues(387, 882);
				c.DSOption6 = false;
			}
			if(c.DSOption5) {
				c.getDH().sendDialogues(376, 882);
				c.DSOption5 = false;
			}
			if(c.DSOption4) {
				c.getDH().sendDialogues(374, 882);
				c.DSOption4 = false;
			}
			if(c.DSOption3) {
				c.getDH().sendDialogues(351, 882);
				c.DSOption3 = false;
			}
				if(c.DSOption) {
						if(c.getItems().playerHasItem(995, 1)) {
						c.getDH().sendDialogues(335, 882);
						c.DSOption = false;
				} else {
						c.getDH().sendDialogues(334, 882);
						c.DSOption = false;
					}
				}
				if(c.doricOption) {
					c.getDH().sendDialogues(306, 284);
					c.doricOption = false;
				}
			break;
			case 9168:
			if(c.DSOption7) {
				c.getDH().sendDialogues(395, 883);
				c.DSOption7 = false;
			}
			if(c.DSOption6) {
				c.getDH().sendDialogues(388, 882);
				c.DSOption6 = false;
			}
			if(c.DSOption5) {
				c.getDH().sendDialogues(379, 882);
				c.DSOption5 = false;
			}
			if(c.DSOption4) {
				c.getDH().sendDialogues(368, 882);
				c.DSOption4 = false;
			}
			if(c.DSOption3) {
				c.getDH().sendDialogues(372, 882);
				c.DSOption3 = false;
			}
			if(c.DSOption) {
				c.getDH().sendDialogues(330, 882);
				c.DSOption = false;
			}
				if (c.doricOption) {
					c.getDH().sendDialogues(303, 284);
					c.doricOption = false;
				}
				
			break;
			case 9169:
			if(c.DSOption7) {
				c.getDH().sendDialogues(397, 883);
				c.DSOption7 = false;
			}
			if(c.DSOption6) {
				c.getDH().sendDialogues(390, 882);
				c.DSOption6 = false;
			}
			if(c.DSOption5) {
				c.getDH().sendDialogues(380, 882);
				c.DSOption5 = false;
			}
			if(c.DSOption4) {
				c.getDH().sendDialogues(355, 882);
				c.DSOption4 = false;
			}
			if(c.DSOption3) {
				c.getDH().sendDialogues(371, 882);
				c.DSOption3 = false;
			}
			if(c.DSOption) {
				c.getDH().sendDialogues(331, 882);
				c.DSOption = false;
			}
				if (c.doricOption) {
					c.getDH().sendDialogues(299, 284);
				}
			break;
			
			case 9158:  
			if(c.DSOption8) {
				c.getDH().sendDialogues(411, 883);
				c.DSOption8 = false;
			}
			if(c.DSOption2) {
				c.getDH().sendDialogues(330, 882);
				c.DSOption2 = false;
			}
if (c.doricOption2) {
					c.getDH().sendDialogues(309, 284);
					c.doricOption2 = false;
				}			
				if (c.dialogueAction == 8) {
					c.getPA().fixAllBarrows();
				} else {
				c.dialogueAction = 0;
				c.getPA().removeAllWindows();
				}
				if (c.dialogueAction == 32) {
				c.getPA().removeAllWindows();
				}
				if (c.dialogueAction == 27) {
					c.getPA().removeAllWindows();
				}
				if(c.caOption2a) {
					c.getDH().sendDialogues(136, c.npcType);
					c.caOption2a = false;
				}
				break;
				
				/** Specials **/
			case 29188:
				c.specBarId = 7636; // the special attack text - sendframe126(S P E
									// C I A L A T T A C K, c.specBarId);
				c.usingSpecial = !c.usingSpecial;
				c.getItems().updateSpecialBar();
				break;

			case 29163:
				c.specBarId = 7611;
				c.usingSpecial = !c.usingSpecial;
				c.getItems().updateSpecialBar();
				break;

			case 33033:
				c.specBarId = 8505;
				c.usingSpecial = !c.usingSpecial;
				c.getItems().updateSpecialBar();
				break;


			case 29063:
				if (c.getCombat()
						.checkSpecAmount(c.playerEquipment[c.playerWeapon])) {
					c.gfx0(246);
					c.forcedChat("Raarrrrrgggggghhhhhhh!");
					c.startAnimation(1056);
					c.playerLevel[2] = c.getLevelForXP(c.playerXP[2])
							+ (c.getLevelForXP(c.playerXP[2]) * 15 / 100);
					c.getPA().refreshSkill(2);
					c.getItems().updateSpecialBar();
				} else {
					c.sendMessage("You don't have the required special energy to use this attack.");
				}
				break;

			case 48023:
				c.specBarId = 12335;
				c.usingSpecial = !c.usingSpecial;
				c.getItems().updateSpecialBar();
				break;

			case 29138:
				c.specBarId = 7586;
				c.usingSpecial = !c.usingSpecial;
				c.getItems().updateSpecialBar();
				break;

			case 29113:
				c.specBarId = 7561;
				c.usingSpecial = !c.usingSpecial;
				c.getItems().updateSpecialBar();
				break;

			case 29238:
				c.specBarId = 7686;
				c.usingSpecial = !c.usingSpecial;
				c.getItems().updateSpecialBar();
				break;

			
			/**Dueling**/			
			case 26065: // no forfeit
			case 26040:
			c.duelSlot = -1;
			c.getTradeAndDuel().selectRule(0);
			break;
			
			case 26066: // no movement
			case 26048:
			c.duelSlot = -1;
			c.getTradeAndDuel().selectRule(1);
			break;
			
			case 26069: // no range
			case 26042:
			c.duelSlot = -1;
			c.getTradeAndDuel().selectRule(2);
			break;
			
			case 26070: // no melee
			case 26043:
			c.duelSlot = -1;
			c.getTradeAndDuel().selectRule(3);
			break;				
			
			case 26071: // no mage
			case 26041:
			c.duelSlot = -1;
			c.getTradeAndDuel().selectRule(4);
			break;
				
			case 26072: // no drinks
			case 26045:
			c.duelSlot = -1;
			c.getTradeAndDuel().selectRule(5);
			break;
			
			case 26073: // no food
			case 26046:
			c.duelSlot = -1;
			c.getTradeAndDuel().selectRule(6);
			break;
			
			case 26074: // no prayer
			case 26047:	
			c.duelSlot = -1;
			c.getTradeAndDuel().selectRule(7);
			break;
			
			case 26076: // obsticals
			case 26075:
			c.duelSlot = -1;
			c.getTradeAndDuel().selectRule(8);
			break;
			
			case 2158: // fun weapons
			case 2157:
			c.duelSlot = -1;
			c.getTradeAndDuel().selectRule(9);
			break;
			
			case 30136: // sp attack
			case 30137:
			c.duelSlot = -1;
			c.getTradeAndDuel().selectRule(10);
			break;	

			case 53245: //no helm
			c.duelSlot = 0;
			c.getTradeAndDuel().selectRule(11);
			break;
			
			case 53246: // no cape
			c.duelSlot = 1;
			c.getTradeAndDuel().selectRule(12);
			break;
			
			case 53247: // no ammy
			c.duelSlot = 2;
			c.getTradeAndDuel().selectRule(13);
			break;
			
			case 53249: // no weapon.
			c.duelSlot = 3;
			c.getTradeAndDuel().selectRule(14);
			break;
			
			case 53250: // no body
			c.duelSlot = 4;
			c.getTradeAndDuel().selectRule(15);
			break;
			
			case 53251: // no shield
			c.duelSlot = 5;
			c.getTradeAndDuel().selectRule(16);
			break;
			
			case 53252: // no legs
			c.duelSlot = 7;
			c.getTradeAndDuel().selectRule(17);
			break;
			
			case 53255: // no gloves
			c.duelSlot = 9;
			c.getTradeAndDuel().selectRule(18);
			break;
			
			case 53254: // no boots
			c.duelSlot = 10;
			c.getTradeAndDuel().selectRule(19);
			break;
			
			case 53253: // no rings
			c.duelSlot = 12;
			c.getTradeAndDuel().selectRule(20);
			break;
			
			case 53248: // no arrows
			c.duelSlot = 13;
			c.getTradeAndDuel().selectRule(21);
			break;
			
			
			case 26018:	
			Client o = (Client) PlayerHandler.players[c.duelingWith];
			if(o == null) {
				c.getTradeAndDuel().declineDuel();
				return;
			}
			
			if(c.duelRule[2] && c.duelRule[3] && c.duelRule[4]) {
				c.sendMessage("You won't be able to attack the player with the rules you have set.");
				break;
			}
			c.duelStatus = 2;
			if(c.duelStatus == 2) {
				c.getPA().sendFrame126("Waiting for other player...", 6684);
				o.getPA().sendFrame126("Other player has accepted.", 6684);
			}
			if(o.duelStatus == 2) {
				o.getPA().sendFrame126("Waiting for other player...", 6684);
				c.getPA().sendFrame126("Other player has accepted.", 6684);
			}
			
			if(c.duelStatus == 2 && o.duelStatus == 2) {
				c.canOffer = false;
				o.canOffer = false;
				c.duelStatus = 3;
				o.duelStatus = 3;
				c.getTradeAndDuel().confirmDuel();
				o.getTradeAndDuel().confirmDuel();
			}
			break;
			
			case 25120:
			if(c.duelStatus == 5) {
				break;
			}
			final Client o1 = (Client) PlayerHandler.players[c.duelingWith];
			if(o1 == null) {
				c.getTradeAndDuel().declineDuel();
				return;
			}

			c.duelStatus = 4;
			if(o1.duelStatus == 4 && c.duelStatus == 4) {				
				c.getTradeAndDuel().startDuel();
				o1.getTradeAndDuel().startDuel();
				o1.duelCount = 4;
				c.duelCount = 4;
				CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
					@Override
					public void execute(CycleEventContainer container) {
						if(System.currentTimeMillis() - c.duelDelay > 800 && c.duelCount > 0) {
							if(c.duelCount != 1 || o1.duelCount != 1) {
								c.forcedChat(""+(--c.duelCount));
								o1.forcedChat(""+(--o1.duelCount));
								c.duelDelay = System.currentTimeMillis();
								o1.duelDelay = System.currentTimeMillis();
							} else {
								c.damageTaken = new int[Config.MAX_PLAYERS];
								c.forcedChat("FIGHT!");
								o1.forcedChat("FIGHT!");
								c.duelCount = 0;
								o1.duelCount = 0;
							}
						}
						if (c.duelCount == 0 || o1.duelCount == 0) {
							container.stop();
						}
					}
					@Override
					public void stop() {
					}
				}, 1);
				c.duelDelay = System.currentTimeMillis();
				o1.duelDelay = System.currentTimeMillis();
			} else {
				c.getPA().sendFrame126("Waiting for other player...", 6571);
				o1.getPA().sendFrame126("Other player has accepted", 6571);
			}
			break;
	
			
			case 4169: // god spell charge
			c.usingMagic = true;
			if(!c.getCombat().checkMagicReqs(48)) {
				break;
			}
				
			if(System.currentTimeMillis() - c.godSpellDelay < Config.GOD_SPELL_CHARGE) {
				c.sendMessage("You still feel the charge in your body!");
				break;
			}
			c.godSpellDelay	= System.currentTimeMillis();
			c.sendMessage("You feel charged with a magical power!");
			c.gfx100(c.MAGIC_SPELLS[48][3]);
			c.startAnimation(c.MAGIC_SPELLS[48][2]);
			c.usingMagic = false;
	        break;			
			
			case 152:
			case 153:
			c.isRunning2 = !c.isRunning2;
			c.isRunning = !c.isRunning;
			int frame = c.isRunning2 == true ? 1 : 0;
			c.getPA().sendFrame36(173,frame);
			break;
			
			
			case 9154:
			c.logout();
			break;
			
			case 21010:
			c.takeAsNote = true;
			break;

			case 21011:
			c.takeAsNote = false;
			break;
			case 118098:
c.getPA().castVeng();
break; 
			
			//home teleports
			case 4171:
			case 50056:
			c.getPA().startTeleport(Config.START_LOCATION_X, Config.START_LOCATION_Y, 0, "modern");
			break;
			
			case 50235:
			case 4140:
			c.getPA().startTeleport(Config.START_LOCATION_X + Misc.random(2), Config.START_LOCATION_Y + Misc.random(2), 0, "modern");
			break;
			
			case 4143:
			case 50245:
			c.getPA().startTeleport(2662, 3305, 0, "modern");
			break;
			
			case 50253:
			case 4146:
			c.getPA().startTeleport(3087, 3500, 0, "modern");
			//c.teleAction = 3;
			break;
			

			case 51005:
			case 4150:
			c.getPA().startTeleport(2757, 3477, 0, "modern");
			//c.teleAction = 4;
			break;			
			
			case 51013:
			case 6004:
			//c.getPA().startTeleport(Config.ARDOUGNE_X, Config.ARDOUGNE_Y, 0, "modern");
			//c.teleAction = 5;
			break; 
			
			
			case 51023:
			case 6005:
			//c.getPA().startTeleport(Config.WATCHTOWER_X, Config.WATCHTOWER_Y, 0, "modern");
			//c.teleAction = 6;
			break; 
			
			
			case 51031:
			case 29031:
			//c.getPA().startTeleport(Config.TROLLHEIM_X, Config.TROLLHEIM_Y, 0, "modern");
			//c.teleAction = 7;
			break; 		

			case 72038:
			case 51039:
			//c.getPA().startTeleport(Config.TROLLHEIM_X, Config.TROLLHEIM_Y, 0, "modern");
			//c.teleAction = 8;
			break;
			
	                 
			case 9125: //Accurate
			case 6221: // range accurate
			case 22228: //punch (unarmed)
			case 48010: //flick (whip)
			case 21200: //spike (pickaxe)
			case 1080: //bash (staff)
			case 6168: //chop (axe)
			case 6236: //accurate (long bow)
			case 17102: //accurate (darts)
			case 8234: //stab (dagger)
			c.fightMode = 0;
			if (c.autocasting)
				c.getPA().resetAutocast();
			break;
			
			case 9126: //Defensive
			case 48008: //deflect (whip)
			case 22229: //block (unarmed)
			case 21201: //block (pickaxe)
			case 1078: //focus - block (staff)
			case 6169: //block (axe)
			case 33019: //fend (hally)
			case 18078: //block (spear)
			case 8235: //block (dagger)
			c.fightMode = 1;
			if (c.autocasting)
				c.getPA().resetAutocast();
			break;
			
			case 9127: // Controlled
			case 48009: //lash (whip)
			case 33018: //jab (hally)
			case 6234: //longrange (long bow)
			case 6219: //longrange
			case 18077: //lunge (spear)
			case 18080: //swipe (spear)
			case 18079: //pound (spear)
			case 17100: //longrange (darts)
			c.fightMode = 3;
			if (c.autocasting)
				c.getPA().resetAutocast();
			break;
			
			case 9128: //Aggressive
			case 6220: // range rapid
			case 22230: //kick (unarmed)
			case 21203: //impale (pickaxe)
			case 21202: //smash (pickaxe)
			case 1079: //pound (staff)
			case 6171: //hack (axe)
			case 6170: //smash (axe)
			case 33020: //swipe (hally)
			case 6235: //rapid (long bow)
			case 17101: //repid (darts)
			case 8237: //lunge (dagger)
			case 8236: //slash (dagger)
			c.fightMode = 2;
			if (c.autocasting)
				c.getPA().resetAutocast();
			break;	
			
			
			/**Prayers**/
			case 21233: // thick skin
			c.getCombat().activatePrayer(0);
			break;	
			case 21234: // burst of str
			c.getCombat().activatePrayer(1);
			break;	
			case 21235: // charity of thought
			c.getCombat().activatePrayer(2);
			break;	
			case 70080: // range
			c.getCombat().activatePrayer(3);
			break;
			case 70082: // mage
			c.getCombat().activatePrayer(4);
			break;
			case 21236: // rockskin
			c.getCombat().activatePrayer(5);
			break;
			case 21237: // super human
			c.getCombat().activatePrayer(6);
			break;
			case 21238:	// improved reflexes
			c.getCombat().activatePrayer(7);
			break;
			case 21239: //hawk eye
			c.getCombat().activatePrayer(8);
			break;
			case 21240:
			c.getCombat().activatePrayer(9);
			break;
			case 21241: // protect Item
			c.getCombat().activatePrayer(10);
			break;			
			case 70084: // 26 range
			c.getCombat().activatePrayer(11);
			break;
			case 70086: // 27 mage
			c.getCombat().activatePrayer(12);
			break;	
			case 21242: // steel skin
			c.getCombat().activatePrayer(13);
			break;
			case 21243: // ultimate str
			c.getCombat().activatePrayer(14);
			break;
			case 21244: // incredible reflex
			c.getCombat().activatePrayer(15);
			break;	
			case 21245: // protect from magic
			c.getCombat().activatePrayer(16);
			break;					
			case 21246: // protect from range
			c.getCombat().activatePrayer(17);
			break;
			case 21247: // protect from melee
			c.getCombat().activatePrayer(18);
			break;
			case 70088: // 44 range
			c.getCombat().activatePrayer(19);
			break;	
			case 70090: // 45 mystic
			c.getCombat().activatePrayer(20);
			break;				
			case 2171: // retrui
			c.getCombat().activatePrayer(21);
			break;					
			case 2172: // redem
			c.getCombat().activatePrayer(22);
			break;					
			case 2173: // smite
			c.getCombat().activatePrayer(23);
			break;
			case 70092: // chiv
			c.getCombat().activatePrayer(24);
			break;
			case 70094: // piety
			c.getCombat().activatePrayer(25);
			break;
			
			case 13092:
			Client ot = (Client) PlayerHandler.players[c.tradeWith];
			if(ot == null) {
				c.getTradeAndDuel().declineTrade();
				c.sendMessage("Trade declined as the other player has disconnected.");
				break;
			}
			c.getPA().sendFrame126("Waiting for other player...", 3431);
			ot.getPA().sendFrame126("Other player has accepted", 3431);	
			c.goodTrade= true;
			ot.goodTrade= true;
			
			for (GameItem item : c.getTradeAndDuel().offeredItems) {
				if (item.id > 0) {
					if(ot.getItems().freeSlots() < c.getTradeAndDuel().offeredItems.size()) {					
						c.sendMessage(ot.playerName +" only has "+ot.getItems().freeSlots()+" free slots, please remove "+(c.getTradeAndDuel().offeredItems.size() - ot.getItems().freeSlots())+" items.");
						ot.sendMessage(c.playerName +" has to remove "+(c.getTradeAndDuel().offeredItems.size() - ot.getItems().freeSlots())+" items or you could offer them "+(c.getTradeAndDuel().offeredItems.size() - ot.getItems().freeSlots())+" items.");
						c.goodTrade= false;
						ot.goodTrade= false;
						c.getPA().sendFrame126("Not enough inventory space...", 3431);
						ot.getPA().sendFrame126("Not enough inventory space...", 3431);
							break;
					} else {
						c.getPA().sendFrame126("Waiting for other player...", 3431);				
						ot.getPA().sendFrame126("Other player has accepted", 3431);
						c.goodTrade= true;
						ot.goodTrade= true;
						}
					}	
				}	
				if (c.inTrade && !c.tradeConfirmed && ot.goodTrade && c.goodTrade) {
					c.tradeConfirmed = true;
					if(ot.tradeConfirmed) {
						c.getTradeAndDuel().confirmScreen();
						ot.getTradeAndDuel().confirmScreen();
						break;
					}
							  
				}

		
			break;
					
			case 13218:
			c.tradeAccepted = true;
			Client ot1 = (Client) PlayerHandler.players[c.tradeWith];
				if (ot1 == null) {
					c.getTradeAndDuel().declineTrade();
					c.sendMessage("Other player declined trade!");
					break;
				}
				
				if (c.inTrade && c.tradeConfirmed && ot1.tradeConfirmed && !c.tradeConfirmed2) {
					c.tradeConfirmed2 = true;
					if(ot1.tradeConfirmed2) {	
						c.acceptedTrade = true;
						ot1.acceptedTrade = true;
						c.getTradeAndDuel().giveItems();
						c.sendMessage("Accepted trade.");
						ot1.sendMessage("Accepted trade.");
						ot1.getTradeAndDuel().giveItems();
						break;
					}
				ot1.getPA().sendFrame126("Other player has accepted.", 3535);
				c.getPA().sendFrame126("Waiting for other player...", 3535);
				}
				
			break;		
			/* Rules Interface Buttons */
			case 125011: //Click agree
				if(!c.ruleAgreeButton) {
					c.ruleAgreeButton = true;
					c.getPA().sendFrame36(701, 1);
				} else {
					c.ruleAgreeButton = false;
					c.getPA().sendFrame36(701, 0);
				}
				break;
			case 125003://Accept
				if(c.ruleAgreeButton) {
					c.getPA().showInterface(3559);
					c.newPlayer = false;
				} else if(!c.ruleAgreeButton) {
					c.sendMessage("You need to click on you agree before you can continue on.");
				}
				break;
			case 125006://Decline
				c.sendMessage("You have chosen to decline, Client will be disconnected from the server.");
				break;
			/* End Rules Interface Buttons */
			/* Player Options */
			case 74176:
				if(!c.mouseButton) {
					c.mouseButton = true;
					c.getPA().sendFrame36(500, 1);
					c.getPA().sendFrame36(170,1);
				} else if(c.mouseButton) {
					c.mouseButton = false;
					c.getPA().sendFrame36(500, 0);
					c.getPA().sendFrame36(170,0);					
				}
				break;
			case 74184:
				if(!c.splitChat) {
					c.splitChat = true;
					c.getPA().sendFrame36(502, 1);
					c.getPA().sendFrame36(287, 1);
				} else {
					c.splitChat = false;
					c.getPA().sendFrame36(502, 0);
					c.getPA().sendFrame36(287, 0);
				}
				break;
			case 74180:
				if(!c.chatEffects) {
					c.chatEffects = true;
					c.getPA().sendFrame36(501, 1);
					c.getPA().sendFrame36(171, 0);
				} else {
					c.chatEffects = false;
					c.getPA().sendFrame36(501, 0);
					c.getPA().sendFrame36(171, 1);
				}
				break;
			case 74188:
				if(!c.acceptAid) {
					c.acceptAid = true;
					c.getPA().sendFrame36(503, 1);
					c.getPA().sendFrame36(427, 1);
				} else {
					c.acceptAid = false;
					c.getPA().sendFrame36(503, 0);
					c.getPA().sendFrame36(427, 0);
				}
				break;
			case 74192:
				if(!c.isRunning2) {
					c.isRunning2 = true;
					c.getPA().sendFrame36(504, 1);
					c.getPA().sendFrame36(173, 1);
				} else {
					c.isRunning2 = false;
					c.getPA().sendFrame36(504, 0);
					c.getPA().sendFrame36(173, 0);
				}
				break;
			case 74201://brightness1
				c.getPA().sendFrame36(505, 1);
				c.getPA().sendFrame36(506, 0);
				c.getPA().sendFrame36(507, 0);
				c.getPA().sendFrame36(508, 0);
				c.getPA().sendFrame36(166, 1);
				break;
			case 74203://brightness2
				c.getPA().sendFrame36(505, 0);
				c.getPA().sendFrame36(506, 1);
				c.getPA().sendFrame36(507, 0);
				c.getPA().sendFrame36(508, 0);
				c.getPA().sendFrame36(166,2);
				break;

			case 74204://brightness3
				c.getPA().sendFrame36(505, 0);
				c.getPA().sendFrame36(506, 0);
				c.getPA().sendFrame36(507, 1);
				c.getPA().sendFrame36(508, 0);
				c.getPA().sendFrame36(166,3);
				break;

			case 74205://brightness4
				c.getPA().sendFrame36(505, 0);
				c.getPA().sendFrame36(506, 0);
				c.getPA().sendFrame36(507, 0);
				c.getPA().sendFrame36(508, 1);
				c.getPA().sendFrame36(166,4);
				break;
			case 74206://area1
				c.getPA().sendFrame36(509, 1);
				c.getPA().sendFrame36(510, 0);
				c.getPA().sendFrame36(511, 0);
				c.getPA().sendFrame36(512, 0);
				break;
			case 74207://area2
				c.getPA().sendFrame36(509, 0);
				c.getPA().sendFrame36(510, 1);
				c.getPA().sendFrame36(511, 0);
				c.getPA().sendFrame36(512, 0);
				break;
			case 74208://area3
				c.getPA().sendFrame36(509, 0);
				c.getPA().sendFrame36(510, 0);
				c.getPA().sendFrame36(511, 1);
				c.getPA().sendFrame36(512, 0);
				break;
			case 74209://area4
				c.getPA().sendFrame36(509, 0);
				c.getPA().sendFrame36(510, 0);
				c.getPA().sendFrame36(511, 0);
				c.getPA().sendFrame36(512, 1);
				break;
			case 168:
                c.startAnimation(855);
            break;
            case 169:
                c.startAnimation(856);
            break;
            case 162:
                c.startAnimation(857);
            break;
            case 164:
                c.startAnimation(858);
            break;
            case 165:
                c.startAnimation(859);
            break;
            case 161:
                c.startAnimation(860);
            break;
            case 170:
                c.startAnimation(861);
            break;
            case 171:
                c.startAnimation(862);
            break;
            case 163:
                c.startAnimation(863);
            break;
            case 167:
                c.startAnimation(864);
            break;
            case 172:
                c.startAnimation(865);
            break;
            case 166:
                c.startAnimation(866);
            break;
            case 52050:
                c.startAnimation(2105);
            break;
            case 52051:
                c.startAnimation(2106);
            break;
            case 52052:
                c.startAnimation(2107);
            break;
            case 52053:
                c.startAnimation(2108);
            break;
            case 52054:
                c.startAnimation(2109);
            break;
            case 52055:
                c.startAnimation(2110);
            break;
            case 52056:
                c.startAnimation(2111);
            break;
            case 52057:
                c.startAnimation(2112);
            break;
            case 52058:
                c.startAnimation(2113);
            break;
            case 43092:
                c.startAnimation(0x558);
            break;
            case 2155:
                c.startAnimation(0x46B);
            break;
            case 25103:
                c.startAnimation(0x46A);
            break;
            case 25106:
                c.startAnimation(0x469);
            break;
            case 2154:
                c.startAnimation(0x468);
            break;
            case 52071:
                c.startAnimation(0x84F);
            break;
            case 52072:
                c.startAnimation(0x850);
            break;
            case 59062:
                c.startAnimation(2836);
            break;
            case 72032:
                c.startAnimation(3544);
            break;
            case 72033:
                c.startAnimation(3543);
            break;
            case 72254:
                c.startAnimation(3866);
            break;
			/* END OF EMOTES */
			
			case 24017:
				c.getPA().resetAutocast();
				//c.sendFrame246(329, 200, c.playerEquipment[c.playerWeapon]);
				c.getItems().sendWeapon(c.playerEquipment[c.playerWeapon], c.getItems().getItemName(c.playerEquipment[c.playerWeapon]));
				//c.setSidebarInterface(0, 328);
				//c.setSidebarInterface(6, c.playerMagicBook == 0 ? 1151 : c.playerMagicBook == 1 ? 12855 : 1151);
			break;
		}
		if (c.isAutoButton(actionButtonId))
			c.assignAutocast(actionButtonId);
	}

}
