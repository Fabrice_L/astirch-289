package com.astirch.game.content.skills.agility;

import com.astirch.Config;
import com.astirch.engine.event.*;
import com.astirch.game.model.players.Client;
import com.astirch.game.model.players.PathFinder;

public class GnomeCourse {
	
	/**
	 ** @author Fabrice
	 **
	 **/
	@SuppressWarnings("unused")
	private Client c;
	public GnomeCourse(Client c) {
		this.c = c;
	}	
	private boolean logBalance, obstacleNetUp, treeBranchUp, balanceRope, treeBranchDown, obstacleNetOver;
	public  boolean doingAgility;	
	
	private void agilityWalk(final Client c, final int walkAnimation, final int x, final int y) {
		c.isRunning2 = false;
		c.getPA().sendFrame36(173, 0);
		c.playerWalkIndex = walkAnimation;
		c.getPA().requestUpdates();
		c.getPA().walkTo(x ,y);
		c.postProcessing();
	}
	
	private void resetAgilityWalk(final Client c) {
		c.isRunning2 = true;
		c.getPA().sendFrame36(173, 1);
		c.playerWalkIndex = 0x333;
		c.getPA().requestUpdates();
	}
	
	private int[] agilityObject = {
		2295,
		2285, 2286,
		2313,
		2312,
		2314, 2315,
		154, 4058		
	};
	
	public boolean agilityObstacle(final Client c, final int objectType) {
		for (int i = 0; i < agilityObject.length; i++) {
			if (objectType == agilityObject[i]) {
				return true;
			}
		}
		return false;		
	}
	
	public void agilityCourse(final Client c, final int objectType) {
		switch (objectType) {
		case 2295:
			CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
				@Override
				public void execute(CycleEventContainer container) {
					PathFinder.getPathFinder().findRoute(c, 2474, 3436, false, 0, 0);
					if (c.absX == 2474 && c.absY == 3436) {
						c.turnPlayerTo(c.objectX, c.objectY);
						container.stop();
					}
				}
				@Override
				public void stop() {
					doingAgility = true;
					agilityWalk(c, 762, 0, -7);
				}
			}, 1);
			CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
				@Override
				public void execute(CycleEventContainer container) {
					if (c.absX == 2474 && c.absY == 3429)
						container.stop();
				}
				@Override
				public void stop() {
					resetAgilityWalk(c);
					c.getPA().addSkillXP((int) 7.5 * Config.SKILLING_EXPERIENCE, c.playerAgility);
					logBalance = true;
					doingAgility = false;
				}
			}, 1);
			break;
		case 2285:
			doingAgility = true;
			c.startAnimation(828);
			CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
				@Override
				public void execute(CycleEventContainer container) {
						container.stop();
				}
				@Override
				public void stop() {
					c.getPA().movePlayer(c.absX, 3424, 1);
					c.getPA().addSkillXP((int) 7.5 * Config.SKILLING_EXPERIENCE, c.playerAgility);
					doingAgility = false;
					obstacleNetUp = true;				}
			}, 2);
			break;
		case 2313:
			doingAgility = true;
			c.startAnimation(828);
			CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
				@Override
				public void execute(CycleEventContainer container) {
						container.stop();
				}
				@Override
				public void stop() {
					c.getPA().movePlayer(2473, 3420, 2);
					c.getPA().addSkillXP((int) 5 * Config.SKILLING_EXPERIENCE, c.playerAgility);
					doingAgility = false;
					treeBranchUp = true;
				}
			}, 2);
			break;
		case 2312:
			CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
				@Override
				public void execute(CycleEventContainer container) {
					PathFinder.getPathFinder().findRoute(c, 2477, 3420, false, 0, 0);
					if (c.absX == 2477 && c.absY == 3420) {
						c.turnPlayerTo(c.objectX, c.objectY);
						container.stop();
					}				
				}
				@Override
				public void stop() {
					doingAgility = true;
					agilityWalk(c, 762, 6, 0);
				}
			}, 1);
			CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
				@Override
				public void execute(CycleEventContainer container) {
					if (c.absX == 2483 && c.absY == 3420)
						container.stop();
				}
				@Override
				public void stop() {
					resetAgilityWalk(c);
					c.getPA().addSkillXP((int) 7 * Config.SKILLING_EXPERIENCE, c.playerAgility);
					balanceRope = true;
					doingAgility = false;
				}
			}, 1);
			break;
		case 2314:
		case 2315:
			c.startAnimation(828);
			CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
				@Override
				public void execute(CycleEventContainer container) {
						container.stop();
				}
				@Override
				public void stop() {
					c.getPA().movePlayer(c.absX, c.absY, 0);
					c.getPA().addSkillXP((int) 5 * Config.SKILLING_EXPERIENCE, c.playerAgility);
					treeBranchDown = true;
					doingAgility = false;
				}
			}, 2);
			break;
		case 2286:
			doingAgility = true;
			c.startAnimation(828);
			CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
				@Override
				public void execute(CycleEventContainer container) {
					if (c.absY == 3425)
					c.getPA().movePlayer(c.absX, 3427, 0);
					if (c.absY == 3427)
						c.getPA().movePlayer(c.absX, 3425, 0);
					container.stop();
				}
				@Override
				public void stop() {
					c.turnPlayerTo(c.absX, 3426);
					obstacleNetOver = true;
					doingAgility = false;
				}
			}, 1);
			break;
		case 154:
		case 4058:
			doingAgility = true;
			CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
				@Override
				public void execute(CycleEventContainer container) {
					PathFinder.getPathFinder().findRoute(c, c.objectX, 3430, false, 0, 0);
					if (c.absY == 3430 && c.absX == c.objectX) {
						c.turnPlayerTo(c.objectX, c.objectY);
						container.stop();
					}
				}
				@Override
				public void stop() {
					c.startAnimation(749);	
					agilityWalk(c, 844, 0, 7);
					}
			}, 1);
			CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
				@Override
				public void execute(CycleEventContainer container) {
					if (c.absX == c.objectX && c.absY == 3437)
						container.stop();
				}
				@Override
				public void stop() {
					c.startAnimation(748);
					resetAgilityWalk(c);
					if (logBalance && obstacleNetUp && treeBranchUp && balanceRope && treeBranchDown && obstacleNetOver) {
						c.getPA().addSkillXP((int) 47 * Config.SKILLING_EXPERIENCE, c.playerAgility);
						c.sendMessage("You have completed the full gnome agility course.");
					} else {
						c.getPA().addSkillXP((int) 7 * Config.SKILLING_EXPERIENCE, c.playerAgility);
					}
					logBalance = false;
					obstacleNetUp = false;
					treeBranchUp = false;
					balanceRope = false;
					treeBranchDown = false;
					obstacleNetOver = false;
					doingAgility = false;
				}
			}, 1);
			break;
		}
	}
}