package com.astirch;

public class Config {
	public static final boolean SERVER_DEBUG = false;//needs to be false for Real world to work
			
	public static int MESSAGE_DELAY = 6000;
	public static final int ITEM_LIMIT = 16000; // item id limit, different clients have more items like silab which goes past 15000
	public static final int MAXITEM_AMOUNT = Integer.MAX_VALUE;
	public static final int BANK_SIZE = 288;
	public static final int MAX_PLAYERS = 1024;
	
	public static int C0NNECTION_DELAY = 100; // how long one ip can keep connecting
	public static final int IPS_ALLOWED = 2; // how many ips are allowed
		
	public static final boolean WORLD_LIST_FIX = true; // change to true if you want to stop that world--8 thing, but it can cause the screen to freeze on silabsoft client
	
	public static final int[] ITEM_SELLABLE 		=	{3842,3844,3840,8844,8845,8846,8847,8848,8849,8850,10551,6570,7462,7461,7460,7459,7458,7457,7456,7455,7454,8839,8840,8842,11663,11664,11665,10499,
														9748,9754,9751,9769,9757,9760,9763,9802,9808,9784,9799,9805,9781,9796,9793,9775,9772,9778,9787,9811,9766,
														9749,9755,9752,9770,9758,9761,9764,9803,9809,9785,9800,9806,9782,9797,9794,9776,9773,9779,9788,9812,9767,
														9747,9753,9750,9768,9756,9759,9762,9801,9807,9783,9798,9804,9780,9795,9792,9774,9771,9777,9786,9810,9765,995,6570,6585,8839,8840,11663,11664,11665,8845,8850,11732,6914,6889,6731,6733,6735,6737,10551}; // what items can't be sold in any store
	public static final int[] ITEM_TRADEABLE 		= 	{3842,3844,3840,8844,8845,8846,8847,8848,8849,8850,10551,6570,7462,7461,7460,7459,7458,7457,7456,7455,7454,8839,8840,8842,11663,11664,11665,10499,
														9748,9754,9751,9769,9757,9760,9763,9802,9808,9784,9799,9805,9781,9796,9793,9775,9772,9778,9787,9811,9766,
														9749,9755,9752,9770,9758,9761,9764,9803,9809,9785,9800,9806,9782,9797,9794,9776,9773,9779,9788,9812,9767,
														9747,9753,9750,9768,9756,9759,9762,9801,9807,9783,9798,9804,9780,9795,9792,9774,9771,9777,9786,9810,9765,6570,6585,8839,8840,11663,11664,11665,8845,8850,11732,6914,6889,6731,6733,6735,6737,10551}; // what items can't be traded or staked
	public static final int[] UNDROPPABLE_ITEMS 	= 	{6570,6585,6586,8839,8840,11663,11664,11665,8845,8850,11732,11733,6914,6915,6889,6890,6731,6732,6733,6734,6735,6736,6737,6738,10551}; // what items can't be dropped
	
	public static final int[] FUN_WEAPONS	=	{2460,2461,2462,2463,2464,2465,2466,2467,2468,2469,2470,2471,2471,2473,2474,2475,2476,2477}; // fun weapons for dueling
	
	public static final boolean ADMIN_CAN_TRADE = false; //can admins trade?
	public static final boolean ADMIN_CAN_SELL_ITEMS = false; // can admins sell items?
	public static final boolean ADMIN_DROP_ITEMS = false; // can admin drop items?
	
	public static final int START_LOCATION_X = 2591; // start here
	public static final int START_LOCATION_Y = 3861;
	//old = 3222, 3219
	public static final int RESPAWN_X = 2591; // when dead respawn here
	public static final int RESPAWN_Y = 3861;
	public static final int DUELING_RESPAWN_X = 3362; // when dead in duel area spawn here
	public static final int DUELING_RESPAWN_Y = 3263;
	public static final int RANDOM_DUELING_RESPAWN = 5; // random coords
	
	public static final int NO_TELEPORT_WILD_LEVEL = 20; // level you can't tele on and above
	public static final int SKULL_TIMER = 1200; // how long does the skull last? seconds x 2
	public static final int TELEBLOCK_DELAY = 20000; // how long does teleblock last for.
	public static final boolean SINGLE_AND_MULTI_ZONES = true; // multi and single zones?
	public static final boolean COMBAT_LEVEL_DIFFERENCE = true; // wildy levels and combat level differences matters
	
	public static final boolean itemRequirements = true; // attack, def, str, range or magic levels required to wield weapons or wear items?
	
	public static final boolean PRAYER_POINTS_REQUIRED = true; // you need prayer points to use prayer
	public static final boolean PRAYER_LEVEL_REQUIRED = true; // need prayer level to use different prayers
	public static final boolean MAGIC_LEVEL_REQUIRED = true; // need magic level to cast spell
	public static final int GOD_SPELL_CHARGE = 300000; // how long does god spell charge last?
	public static final boolean RUNES_REQUIRED = true; // magic rune required?
	public static final boolean CORRECT_ARROWS = true; // correct arrows for bows?
	public static final boolean CRYSTAL_BOW_DEGRADES = true; // magic rune required?
	
	public static final int SAVE_TIMER = 10; // save every 4 minute
	public static final int NPC_RANDOM_WALK_DISTANCE = 5; // the square created , 5x5 so npc can't move out of that box when randomly walking
	public static final int NPC_FOLLOW_DISTANCE = 10; // how far can the npc follow you from it's spawn point, 													
	public static final int[] UNDEAD_NPCS = {90,91,92,93,94,103,104,73,74,75,76,77}; // undead npcs

	/**
	 * Glory
	 */
	public static final int EDGEVILLE_X = 3087;
	public static final int EDGEVILLE_Y = 3500;
	public static final String EDGEVILLE = "";
	public static final int AL_KHARID_X = 3293;
	public static final int AL_KHARID_Y = 3174;
	public static final String AL_KHARID = "";
	public static final int KARAMJA_X = 3087;
	public static final int KARAMJA_Y = 3500;
	public static final String KARAMJA = "";
	public static final int MAGEBANK_X = 2538;
	public static final int MAGEBANK_Y = 4716;
	public static final String MAGEBANK = "";
 
	public static final int TIMEOUT = 20;
	public static final int CYCLE_TIME = 600;
	public static final int BUFFER_SIZE = 10000;
	
	
	public static final int INCREASE_SPECIAL_AMOUNT = 17500;

	/**
	* Skill Experience Multipliers
	*/	
	public static final int SERVER_EXP_BONUS = 100;
	public static final int SKILLING_EXPERIENCE = 10;
	public static final int COMBAT_EXPERIENCE = 15;
	
	public static final int MELEE_EXP_RATE = 4 * COMBAT_EXPERIENCE; // damage * exp rate
	public static final int RANGE_EXP_RATE = 4 * COMBAT_EXPERIENCE;
	public static final int MAGIC_EXP_RATE = 4 * COMBAT_EXPERIENCE;

}
